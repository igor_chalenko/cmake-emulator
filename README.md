# CMake state machine #

What is it
----------
This package manages an in-memory object hierarchy that closely approximates
the CMake object hierarchy. Its purpose is to:
- load an initial state, possibly exported from CMake;
- update the state using CMake-like commands, but in Python;
- serialize the operation history into a valid CMake code fragment. 

Installation
------------

* Configuration
* Dependencies
* How to run tests
* Deployment instructions

