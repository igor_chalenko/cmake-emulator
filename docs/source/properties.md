# Properties

A property is an association between a string name and a value of some type. 
‘Typed’ and ‘generic’ properties are distinguished. A typed property has one of
the types described in . A generic property accepts all values and performs no 
type analysis in any operations. Yaranga gives a certain level of control over 
the kind of properties it instantiates (todo).

Property definitions
--------------------
CMake documents a number of properties that have pre-defined semantics in
the scopes of a certain kind. These semantics are captured by the 

```{eval-rst}
.. autoclass:: yaranga.model.property.PropertyDef
   :members:
```

Property types and value conversion
-----------------------------------
A property may have one of the types defined 
[here](types.html#data_model_types), except `None` and `Undefined` - those are
value types, not the property types. Property of each type will convert 
incoming `CMake` strings into a value of its type, if possible. Conversion is
performed in two different contexts:

* during the initial import of CMake state into Python;
* during the CMake code generation.

Type conversion is **not** performed anywhere else: during the usual value
updates, in \`if\` conditions, or any other type-dependent logic that is
implemented by Python. This means that, for example, a string `YES` is not a
valid value for write operations on a `bool` property.

### Boolean properties

When importing values from `CMake`, `Yaranga` converts certain strings to one
of `True`, `False`, or `None`.

* The `CMake` constants for `True` are `ON`, `YES`, `TRUE`, `Y`, or any non-zero
  number.
* The `CMake` constants for `False` are `0`, `OFF`, `NO`, `FALSE`, `N`, 
  `IGNORE`. Note that this does not include special `NOTFOUND` constants - see 
  below.
* The `CMake` constants for `None` are `NOTFOUND`, the empty string, or a string
  that ends in the suffix `-NOTFOUND`. The need for special `NOTFOUND` constants
  is eliminated by the existence of `None`; in boolean comparisons, it evaluates
  to `False`, which is what we need.

### Integer properties

The input string will be cast to an integer via the built-in function `int()`.
Cast failure leads to `PropertyCastError` being raised.

### Enum properties

Exact type of EnumProperty's value is specified during initialization stage.
The input string will be cast to that enumeration type. Cast failure leads to
`PropertyCastError` being raised.

### Path properties

The input string is given to `pathlib.Path` constructor.

### List properties

The input string is parsed as a semicolon-separated `CMake` list. If the input
is a single value, it becomes the first and the only element in the newly
created list.