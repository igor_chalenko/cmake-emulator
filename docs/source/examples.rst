Examples
--------

Create a target:

.. code-block:: python

    def configure(cmake):
      target = cmake.add_executable('compound', 'example/compound.cc')
      target.include_directories = f'{cmake.project_source_dir}/include'

This would correspond to the following CMake commands:

.. code-block:: cmake

    add_executable(compound SOURCES 'example/compound.cc')
    target_include_directories(compound, ${PROJECT_SOURCE_DIR}/include)

Create a `Catch2`_ test executable for every source file in a directory:

.. code-block:: python

    def configure(context):
        pass

Create:

.. code-block:: python

    def configure(context):
        pass

This example shows how to load a dependency using `CPM`_:

.. code-block:: python

    def configure(context):
        pass


.. _CPM: https://github.com/cpm-cmake/CPM.cmake
.. _Catch2: https://github.com/catchorg/Catch2