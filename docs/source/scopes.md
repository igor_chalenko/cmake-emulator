# Scopes

Target
------

Binary targets are executables and libraries which are created by calls 
to `add_executable` or `add_library`.

todo import targets

Targets may depend on one another, which means that dependent ones will be 
built after their dependencies.

From the standpoint of state management, a target is a regular scope that
is easy to manage.

State updates
-------------

`CMake` commands store state in properties. Given the `Python` syntax, it's 
only natural to use attribute syntax to access property values. Dictionary 
syntax is also supported. There's no data duplication: both the attributes
and dictionary keys refer to the same values in an internal scope's 
dictionary.
