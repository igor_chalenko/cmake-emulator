Typification and related value conversion
-----------------------------------------

A lot of CMake properties have predefined semantics that dictate the kind of
values those properties may have. CMake doesn't enforce it in any way, but
there's no reason why ``Yaranga`` should not. This can be implemented by
attaching a type to each predefined property. Then, that property can:

 * convert values to and from CMake format during the import/export phases;
 * validate property value updates.

In the author's humble opinion, this makes sense in the Python code. But,
conversion entails side effects, which must be kept in mind. For example:

.. code-block:: cmake

    set_property(TARGET foo PROPERTY EXCLUDE_FROM_ALL YES)
    # ...
    get_property(_exclude_from_all TARGET foo PROPERTY EXCLUDE_FROM_ALL)
    if (_exclude_from_all STREQUAL YES)
      message(STATUS "yes")
    endif()

In ``Yaranga``, this would look like:

.. code-block:: python

    foo.exclude_from_all = "YES" # converted to True
    # ...
    if foo.exclude_from_all == 'YES'
        print('yes')
    endif()

The second snippet will not print ``yes``, although it’s a literal translation
of the first snippet. To counter these side effects, there must be an option to
enable or disable conversion.

Values convertible to None
==========================

The ``CMake`` constants for ``None`` are ``NOTFOUND``, the empty string, or
a string that ends in the suffix ``-NOTFOUND``. The need for special
``NOTFOUND`` constants is eliminated by the existence of None; in boolean
comparisons, it evaluates to False, which is what we need.

Boolean conversion
==================

When importing values from CMake, Yaranga converts certain strings to one of
``True``, ``False``, or ``None``. The ``CMake`` constants for ``True`` are:

  * ``ON``,
  * ``YES``,
  * ``TRUE``,
  * ``Y``,
  * or any non-zero number.

The ``CMake`` constants for ``False`` are:
  * ``0``,
  * ``OFF``,
  * ``NO``,
  * ``FALSE``,
  * ``N``,
  * ``IGNORE``.

Note that this does not include special ``NOTFOUND`` constants - see above.

Integer conversion
==================

The input string will be cast to an integer via the built-in function
``int()``.

Enumeration conversion
======================

The input string will be cast to the target enumeration type.

List conversion
===============

The input string is parsed as a semicolon-separated CMake list. If the input is
a single value, it becomes the first and the only element in the newly created
list.
