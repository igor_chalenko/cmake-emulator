.. Yaranga documentation master file, created by
   sphinx-quickstart on Thu Nov 18 17:03:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Yaranga - CMake emulator
========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   examples

.. toctree::
   :maxdepth: 2
   :caption: CMake emulation

   field-object-model
   field
   model
   converters

.. toctree::
   :maxdepth: 2
   :caption: Synchronization with CMake

.. toctree::
   :maxdepth: 2
   :caption: API
