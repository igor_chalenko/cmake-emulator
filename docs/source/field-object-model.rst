CMake emulation
---------------

``CMake`` execution can be seen as a sequence of object updates
with side effects, such as generated build files, cache, etc. ``Yaranga``
reproduces the object updates - it tracks updates of all variables,
targets, sources files, and other objects that it comes across during
the script execution. All the side effects are out of scope.

Every object is a property store
================================

Every ``CMake`` object has a number of predefined properties. Complete list of
all these properties is obtainable by running

.. code-block:: console

  ${CMAKE_COMMAND} --help-property-list

Since these properties are well-documented and have fixed semantics, it
makes sense to define them as Python attributes in respective classes. For
example, cache variable class could look like this:

.. code-block:: python

    class CacheVariable:
        name: str
        helpstring: str
        value: Value
        type: CacheVariableType
        advanced: bool = False
        strings: Optional[list[str]] = None

However, a ``CMake`` object may also have custom properties that are not known
in advance. This calls for a base class that implements dictionary-like storage
for these custom properties:

.. code-block:: python

    class CMakeObject:
        properties: dict[str, Any]

        def __getitem__(self, property_name: str) -> Any:
            return self.properties[property_name]

This does the job, but also creates a division between the attributes and
properties. We could allow dict-based access to the attributes as well:

.. code-block:: python

        def __getitem__(self, property_name: str) -> Any:
            attr_name = self.attr_from_property(property_name)
            if attr_name:
                return self.get(attr_name)
            else:
                return self.properties.get(property_name, Undefined)

In this second implementation, we first check if the given property name
designates an attribute. If so, the attribute's value is returned. Otherwise,
we query the dictionary of custom properties, same as before.

Similar logic should be implemented in ``__setitem__``, ``__contains__``, and
``__delitem__``.

Property types
==============

A lot of ``CMake`` properties have predefined semantics that dictates the kind
of values those properties may have. ``CMake`` doesn't enforce it in any way,
but there’s no reason why ``Yaranga`` should not. It's possible to translate
``CMake`` conventions for string values into Python's type-specific rules.
Every property may have a type attached to it, which would then define
conversion and validation rules.

.. automodule:: yaranga.types
    :members:

Undefined type
==============

.. automodule:: yaranga.undefined
    :members:
    :undoc-members:

The need for declarative validation
===================================

``CMake`` executes script commands sequentially.  When a command is executed,
certain checks may take place that could decide the command's success. Some
of these checks are easily reproducible. Some are not, either because they
depend on a specific ``CMake`` version, or policies set, or involve some
internal data to which ``Yaranga`` has no access. In order to faithfully
emulate ``CMake`` execution, therefore, it is necessary to:

  - delegate 'complex' checks to ``CMake``, and
  - implement reproducible checks, to avoid over-reliance upon ``CMake``.

Given a large amount of properties with individually defined validation
rules, it makes sense to somehow attach these validations to individual
class attributes (counterparts of ``CMake`` properties), to avoid too much
repeated code. This is true for both points above, the only difference
is the action taken by the validation function.

One common validation type is the read-only check. With an ability
to just declare an attribute as read-only, this task become trivial and easily
testable. Another very common case is the property type conversion. A lot of
``CMake`` properties have a fixed data type, which could be translated
into a respective Python type, with proper conversion implemented in
a similar declarative manner.

Declarative validation: fields
==============================

It's possible to address the concerns above using a well-known approach.
Consider an example class:

.. code-block:: python

   class Model(metaclass=ModelMetaclass):
     attr1: int = Field(default=42, read_only=True)
     attr2: int = 42 # the same as attr = Field(default=42)
     # a mandatory attribute
     attr3: int # the same as attr = Field(alias='ATTR3')

The default value of the attribute ``attr1`` is an instance  of
:class:`yaranga.model.field.Field`. This field object specifies the default value of ``attr1``,
and the read-only flag. The field object may also specify other semantics
demanded by the respective CMake property: nullability, validation rules, etc.
During the class initialization, ``Field`` objects are converted to their
respective :class:`ModelField` instances, which then become a part of
the so-called

Field object model
==================

Field object model extends Python object model in the following ways:

  * it introduces declarative data validation and type conversion;
  * it control data access operations (reading/writing).

As can be seen, the purpose of the module is very similar to that of
`pydantic`_. If you are familiar with it, then much of the functionality here
will be familiar. There are certain differences, however, that are dictated
by a different problem domain.

  1. ``__init__`` method must be handwritten for every class. This makes it
     easy to work with scope classes in IDE, such as `PyCharm`_, without
     extra plugins. Also, see below.
  2. The notion of a required field is different. Every field has a default
     value, either explicit or ``Undefined``. This means that required fields
     are effectively the ones that appear in the ``__init__`` method, and the
     rest will be initialized to a default value or become undefined. This
     behavior is dictated by the fact that the general assumption about
     mandatory fields being available during initialization does not hold.
     Indeed, there are properties that only ``CMake`` can provide a value for.
     If a ``CMake`` scope is created on the Python side, such properties will
     remain undefined until the first read attempt. Upon a read of an undefined
     property, the value can be obtained from ``CMake`` (how this is done is
     not important for the moment), and the property updated even if it was
     marked as ``read-only``.
  3. Validation of a property's new value has access to the property's
     containing object. This makes it easy to compare old and new values,
     for example.

.. _pydantic: https://pypi.org/project/pydantic/
.. _PyCharm: https://www.jetbrains.com/ru-ru/pycharm/