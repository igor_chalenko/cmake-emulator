FOM implementation
------------------

.. automodule:: yaranga.model.modelfield
    :members:

.. automodule:: yaranga.model.typeinfo
    :members:

.. automodule:: yaranga.model.model
    :members:
