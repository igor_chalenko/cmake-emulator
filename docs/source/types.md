# Python types for CMake <a name="data_model_types"></a>

It's possible to translate CMake conventions for string values into Python's
type system. `Yaranga` will use the following value types:

* `bool` is for the strings that are recognized by CMake as boolean value
  representations;
* `int` is for the strings that are convertible to an integer;
* `str` is for single values that may not grow to a list because of the
  property's semantics;
* `list` is for lists of primitive values - CMake doesn't support nested lists;
* `typing.Enum` is for Python enumerations that capture CMake constants, such as
  target types, for example;
* `pathlib.Path` for file (and directory) names;
* `None` is for empty strings and the strings of the form `<var>-NOTFOUND`;
* `Undefined` is a special value that can be returned by a read operation on a
  property that was not defined by the initial import.

The "Undefined" type
--------------------

```{eval-rst}
.. automodule:: yaranga.undefined
   :members:
```

`Yaranga` cannot load `CMake` state in its entirety - `CMake` does not provide
facilities to obtain all properties of a target, for example. These limitations
are detailed elsewhere. todo link. In many scenarios, this is not a problem -
for example, when all targets use only documented properties (that are imported
into `Python`). However, it can be hard to predict if this is the case, given
external dependencies and overall project complexity. Therefore, to avoid any
ambiguity, an attempt to read a property that could be present in the `CMake`
context, but was not imported into `Python`, results in the `Undefined` value
returned. The package could then analyze it and act accordingly. One of the
pre-defined behaviors is to raise an exception, letting the users handle the
case in an `except` block. `Yaranga` provides a mechanism (link) to explicitly
add properties into the export set after they have been reported as described
above.

Type checking
-------------
A lot of CMake properties have predefined semantics that dictates the kind of
values those properties may have. CMake doesn't enforce it in any way, but
there’s no reason why `Yaranga` should not. This can be implemented by attaching
a type to each pre-defined property. Then, that property could:
* convert values from and to `CMake` format during the import/export phases;
* validate property value updates.

To facilitate this, `Yaranga` implements the function decorator
```{eval-rst}
.. autofunction:: yaranga.model.check_types.check_types
```

```{eval-rst}
.. autoexception:: yaranga.model.check_types.TypeMismatchError
   :special-members: __init__
   :members: 
```

To minimize the side effects, type checking can be enabled in a 'strict' mode
and disabled otherwise.
