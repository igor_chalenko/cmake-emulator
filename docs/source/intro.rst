Introduction
------------

`Yaranga` is a Python package that enables running CMake commands from within a 
Python runtime. A high-level description of the process is as follows:

1. `CMake` executes a `CMakeLists.txt` file in some directory.
2. `CMake`-`Python` bridge is included to provide the functions necessary for
   the interaction.   
3. Export functions from `CMake`-`Python` bridge serialize the current project
   state for consumption within the `Python` code.
4. Spawn functions from `CMake`-`Python` bridge initialize `Yaranga`.
5. `Yaranga` reads the exported CMake state and executes user-provided 
   commands in the `CMakeLists.py`, same as with `CMakeLists.txt`.
6. Executed commands are serialized into a `CMake` code fragment that is
   returned to the calling `CMake` process (via `OUTPUT_VARIABLE`).
7. The resulting code fragment is executed via `cmake_language`.

