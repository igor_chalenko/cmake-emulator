import os
from typing import Optional

from yaranga import constants
from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.objects.cache_variable import CacheVariableType
from yaranga.types import Value
from yaranga.undefined import Undefined


class Set(Command):
    name: str = Field(nullable=False)
    value: Optional[Value] = Field(nullable=False)

    def command_name(self):
        return "set"

    def __init__(self, context, name: str, value: Optional[Value]):
        super(Set, self).__init__()

        self.context = context
        self.name = name
        self.value = value
        self._env_set = constants.ENV_PATTERN.match(name)
        self.exec(name, value)

    def exec(self, name: str, value: Optional[Value]):
        if self._env_set:
            name = self._env_set.group(1)
            os.environ[name] = value
        else:
            cache_set = constants.CACHE_PATTERN.match(name)
            if cache_set:
                name = cache_set.group(1)
                self.context.set_cache_variable(name, value)
            else:
                if value is not Undefined:
                    self.context.variables[name] = value
                else:
                    if name in self.context.variables:
                        del self.context.variables[name]

    def __eq__(self, other) -> bool:
        return (
            isinstance(other, Set)
            and self.name == other.name
            and self.value == other.value
        )

    def __str__(self) -> str:
        if self._env_set:
            if self.value is not None:
                return f'set($ENV{{{self._env_set.group(1)}}} "{self.value}")'
            else:
                return f"unset($ENV{{{self.name}}})"
        else:
            if self.value is not None:
                return f'set({self.name} "{self.value}")'
            else:
                return f"unset({self.name})"


class SetCacheVariable(Command):
    name: str = Field(nullable=False)
    value: Value = Field(nullable=False)
    cache: str = Field(nullable=False, default="CACHE")
    typ: CacheVariableType = Field(nullable=False)
    helpstring: str = Field(nullable=False)
    force: bool = Field(nullable=True)

    def __init__(
        self,
        name: str,
        value: Value,
        typ: CacheVariableType,
        helpstring: str,
        force: bool = False,
    ):
        super(SetCacheVariable, self).__init__()

        self.name = name
        self.value = value
        self.typ = typ
        self.helpstring = helpstring
        self.force = force

    def command_name(self):
        return "set"
