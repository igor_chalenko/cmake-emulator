import logging
from typing import Any

from yaranga.commands.command import Command
from yaranga.model.field import Field


class MessageCommand(Command):
    level: Any = Field(nullable=False)
    message: str = Field(nullable=False)

    def __init__(self, message: str, level):
        super(MessageCommand, self).__init__()

        self.message = message
        # transform the level constant
        if level == logging.CRITICAL or level == logging.FATAL:
            self.level = "FATAL_ERROR"
        elif level == logging.ERROR:
            self.level = "SEND_ERROR"
        elif level == logging.WARNING or level == logging.WARN:
            self.level = "WARNING"
        elif level == logging.INFO or level == logging.WARN:
            self.level = "STATUS"
        elif level == logging.DEBUG:
            self.level = "DEBUG"
        else:
            self.level = "STATUS"

    def command_name(self):
        return "message"
