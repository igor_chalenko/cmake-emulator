import abc
from typing import Optional, Any

from yaranga.cmake.cmake_directory import CMakeDirectory
from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.objects.cache_variable import CacheVariable
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.objects.directory import Directory
from yaranga.objects.target import Target
from yaranga.objects.test import Test
from yaranga.objects.updatetype import UpdateType
from yaranga.types import Value


class SetProperty(Command, metaclass=abc.ABCMeta):
    prefix_str: str = Field(nullable=False)
    obj_name: str = Field(nullable=False)
    property: str = Field(default="PROPERTY", nullable=False)
    name: str = Field(nullable=False)
    value: Any = Field(nullable=False)

    def __init__(
        self, obj: Any, name: str, value: Value, update_type: UpdateType, **kwargs
    ):
        super(SetProperty, self).__init__(
            obj_name=self.object_name(),
            name=name,
            value=value,
            prefix_str=self.prefix(),
            update_type=update_type,
            **kwargs,
        )

        self.obj = obj
        self.update_type = update_type

    @abc.abstractmethod
    def prefix(self) -> str:
        raise NotImplementedError()

    def object_name(self) -> Optional[str]:
        return None

    def command_name(self):
        return "set_property"

    def __eq__(self, other):
        return (
            self.obj == other.obj
            and self.name == other.name
            and self.value == other.value
        )

    def exec(self):
        self.obj.set_property(self.name, self.value, self.update_type)


class SetGlobalProperty(SetProperty):
    def __init__(
        self,
        obj: CMakeObject,
        name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        super(SetGlobalProperty, self).__init__(obj, name, value, update_type)

    def prefix(self):
        return f"GLOBAL"


class SetTargetProperty(SetProperty):
    def __init__(
        self,
        target: Target,
        name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        self.target = target

        super(SetTargetProperty, self).__init__(target, name, value, update_type)

    def prefix(self):
        return "TARGET"

    def object_name(self):
        return self.target.name


class SetDirectoryProperty(SetProperty):
    def __init__(
        self,
        directory: Directory,
        name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        self.directory = directory
        super(SetDirectoryProperty, self).__init__(directory, name, value, update_type)

    def prefix(self):
        return "DIRECTORY"

    def object_name(self):
        return self.directory.source_dir


class SetTestProperty(SetProperty):
    def __init__(
        self,
        test: Test,
        name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        self.test = test
        super(SetTestProperty, self).__init__(test, name, value, update_type)

    def prefix(self):
        return "TEST"

    def object_name(self):
        return self.test.name


class SetCacheVariableProperty(SetProperty):
    def __init__(
        self,
        variable: CacheVariable,
        property_name: str,
        property_value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        self.variable = variable
        super(SetCacheVariableProperty, self).__init__(
            variable, property_name, property_value, update_type
        )

    def prefix(self):
        return f"CACHE"

    def object_name(self) -> Optional[str]:
        return self.variable.name


class SetSourceFileProperty(SetProperty):
    directories: list[CMakeDirectory]
    target_directories: list[CMakeDirectory]

    def __init__(
        self,
        source_file: str,
        name: str,
        value: Value,
        update_type: UpdateType,
        current_dir: Directory,
        directories=None,
        target_directories=None,
    ):
        self.current_dir = current_dir
        self.source_file = source_file

        super(SetSourceFileProperty, self).__init__(
            source_file,
            name,
            value,
            update_type,
            directories=directories,
            target_directories=target_directories,
        )

    def prefix(self):
        return "SOURCE"

    def object_name(self) -> Optional[str]:
        return self.source_file

    def exec(self):
        if self.directories:
            for directory in self.directories:
                directory.set_source_property(
                    self.obj, self.name, self.value, self.update_type
                )
        if self.target_directories:
            for target_dir in self.target_directories:
                target_dir.set_source_property(
                    self.obj, self.name, self.value, self.update_type
                )

        if not self.directories and not self.target_directories:
            directory = self.current_dir
            directory.set_source_property(
                self.obj, self.name, self.value, self.update_type
            )
