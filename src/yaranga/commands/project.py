from typing import Optional

from yaranga import constants
from yaranga.cmake.cmake_directory import CMakeDirectory
from yaranga.cmake.version import Version
from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.objects.cache_variable import CacheVariableType
from yaranga.objects.updatetype import UpdateType

VERSION_MAJOR = "{}_VERSION_MAJOR"
VERSION_MINOR = "{}_VERSION_MINOR"
VERSION_PATCH = "{}_VERSION_PATCH"
VERSION_TWEAK = "{}_VERSION_TWEAK"

DESCRIPTION_TEMPLATE = "{}_DESCRIPTION"
HOMEPAGE_URL_TEMPLATE = "{}_HOMEPAGE_URL"
NAME_TEMPLATE = "{}_NAME"
VERSION_TEMPLATE = "{}_VERSION"


class Project(Command):
    name: str = Field(nullable=False)
    version: Version = Field(nullable=True)
    description: str = Field(nullable=True)
    homepage_url: str = Field(nullable=True)
    languages: list[str] = Field(nullable=True)

    SOURCE_DIR_TEMPLATE = "{}_SOURCE_DIR"
    BINARY_DIR_TEMPLATE = "{}_BINARY_DIR"
    TOP_LEVEL = "{}_IS_TOP_LEVEL"
    DESCRIPTION_TEMPLATE = "{}_DESCRIPTION"
    HOMEPAGE_URL_TEMPLATE = "{}_HOMEPAGE_URL"
    NAME_TEMPLATE = "{}_NAME"
    VERSION_TEMPLATE = "{}_VERSION"
    VERSION_MAJOR = "{}_VERSION_MAJOR"
    VERSION_MINOR = "{}_VERSION_MINOR"
    VERSION_PATCH = "{}_VERSION_PATCH"
    VERSION_TWEAK = "{}_VERSION_TWEAK"

    COMPUTED_VALUE = "Value Computed by CMake"

    def __init__(
        self,
        context,
        current_dir: Optional[CMakeDirectory],
        top_level: bool,
        name: str,
        version: Optional[str] = None,
        homepage_url=None,
        description=None,
        languages: list[str] = None,
        is_top_level: bool = False,
    ):
        super(Project, self).__init__(
            name=name,
            version=version,
            homepage_url=homepage_url,
            description=description,
            languages=languages,
        )

        self.context = context
        # self.name = name
        # self.version = Version(version) if version else None
        # self.homepage_url = homepage_url
        # self.description = description
        # self.languages = languages
        self.is_top_level = is_top_level

        version_obj = Version(version) if version else None

        prj = "PROJECT"
        root_prj = "CMAKE_PROJECT"

        context[NAME_TEMPLATE.format(prj)] = name
        self._set_project_variables(
            context, version_obj, homepage_url, description, prj
        )
        context.set_cache_variable(
            "CMAKE_PROJECT_NAME",
            name,
            CacheVariableType.STATIC,
            constants.COMPUTED_VALUE,
        )
        context.set_cache_variable(
            constants.SOURCE_DIR.format(name),
            str(current_dir.source_dir),
            CacheVariableType.STATIC,
            constants.COMPUTED_VALUE,
        )
        context.set_cache_variable(
            constants.BINARY_DIR.format(name),
            str(current_dir.binary_dir),
            CacheVariableType.STATIC,
            constants.COMPUTED_VALUE,
        )
        context.set_cache_variable(
            constants.TOP_LEVEL.format(name),
            "ON" if top_level else "OFF",
            CacheVariableType.STATIC,
            constants.COMPUTED_VALUE,
        )
        if top_level:
            context[constants.TOP_LEVEL.format(prj)] = "ON" if top_level else "OFF"
            context[NAME_TEMPLATE.format(root_prj)] = name
            self._set_project_variables(
                context, version_obj, description, homepage_url, root_prj
            )

        if languages:
            # prepend
            context.set_global_property(
                "ENABLED_LANGUAGES", languages, UpdateType.PREPEND
            )

    @staticmethod
    def _set_project_variables(
        context, version: Version, description: str, homepage_url: str, prefix: str
    ):
        if version:
            version_str = str(version)
            version_major = version.major
            version_minor = version.minor
            version_patch = version.patch
            version_tweak = version.tweak
        else:
            version_str = None
            version_major = None
            version_minor = None
            version_patch = None
            version_tweak = None

        context[DESCRIPTION_TEMPLATE.format(prefix)] = description
        context[HOMEPAGE_URL_TEMPLATE.format(prefix)] = homepage_url
        context[VERSION_TEMPLATE.format(prefix)] = version_str
        context[VERSION_MAJOR.format(prefix)] = version_major
        context[VERSION_MINOR.format(prefix)] = version_minor
        context[VERSION_PATCH.format(prefix)] = version_patch
        context[VERSION_TWEAK.format(prefix)] = version_tweak

    def command_name(self):
        return "project"
