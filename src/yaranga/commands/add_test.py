from typing import Optional

from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.types import FileName
from yaranga.undefined import Undefined


class AddTest(Command):
    name: str = Field(nullable=False)
    command: str = Field(nullable=True)
    configurations: list[str] = Field(nullable=True)
    working_directory: FileName = Field(nullable=True)
    command_expand_lists: bool = Field(nullable=True)

    def __init__(
        self,
        name: str,
        command: str = Undefined,
        configurations: Optional[list[str]] = Undefined,
        working_directory: Optional[FileName] = Undefined,
        command_expand_lists: bool = Undefined,
        **kwargs
    ):
        super(AddTest, self).__init__(
            name=name,
            command=command,
            configurations=configurations,
            working_directory=working_directory,
            command_expand_lists=command_expand_lists,
            **kwargs
        )

    def command_name(self):
        return "add_test"
