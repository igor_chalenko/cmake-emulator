from yaranga.commands.command import Command
from yaranga.model.field import Field


class VariablesDeltaBefore(Command):
    prev: str = Field(default="PREV", nullable=False)

    def __init__(self):
        super(VariablesDeltaBefore, self).__init__()

    def command_name(self):
        return "narta_variables"


class VariablesDelta(Command):
    prev: str = Field(default="PREV", nullable=False)
    current: bool = Field(default="CURRENT", nullable=False)
    callback: str = Field(default="variables_callback", nullable=False)

    def __init__(self, callback: str = None):
        super(VariablesDelta, self).__init__()

    def command_name(self):
        return "narta_variables_delta"
