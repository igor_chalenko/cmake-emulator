from yaranga.commands.command import Command
from yaranga.model.field import Field


class GlobalsDeltaBefore(Command):
    prev: str = Field(default="PREV", nullable=False)

    def __init__(self):
        super(GlobalsDeltaBefore, self).__init__()

    def command_name(self):
        return "narta_globals"


class GlobalsDelta(Command):
    prev: str = Field(default="PREV", nullable=False)
    current: bool = Field(default="CURRENT", nullable=False)
    callback: str = Field(default="globals_callback", nullable=False)

    def __init__(self):
        super(GlobalsDelta, self).__init__()

    def command_name(self):
        return "narta_globals_delta"
