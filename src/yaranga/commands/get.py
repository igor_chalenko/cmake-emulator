import abc
from pathlib import Path
from typing import Any, Optional

from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.types import FileName


class Get(Command):
    name: str = Field(nullable=False)

    def __init__(self, name: str):
        super(Get, self).__init__(name=name)

    def __eq__(self, other):
        return isinstance(other, Get) and self.name == other.name

    def command_name(self):
        return "narta_get_variable"


class GetProperty(Command, abc.ABC):
    obj: Any = Field(nullable=False)
    property_name: str = Field(nullable=False)

    def __init__(self, obj: Optional[str], property_name: str):
        super(GetProperty, self).__init__(property_name=property_name)

        self.obj = obj

    def __eq__(self, other):
        return self.obj == other.obj and self.property_name == other.property_name


class GetGlobalProperty(GetProperty):
    def __init__(self, property_name: str):
        super(GetGlobalProperty, self).__init__(None, property_name)

    def command_name(self):
        return "narta_get_global_property"


class GetDirectory(Command):
    directory: FileName = Field(nullable=False)

    def __init__(self, directory: FileName):
        super(GetDirectory, self).__init__(directory=directory)

    def __eq__(self, other):
        return isinstance(other, GetDirectory) and Path(self.directory) == Path(
            other.directory
        )

    def command_name(self):
        return "narta_get_directory"


class GetDirectoryProperty(GetProperty):
    def __init__(self, obj: FileName, property_name: str):
        super(GetDirectoryProperty, self).__init__(obj, property_name)

    def command_name(self):
        return "narta_get_directory_property"

    def __eq__(self, other):
        return (
            Path(self.obj) == Path(other.obj)
            and self.property_name == other.property_name
        )


class GetTarget(Command):
    directory: FileName = Field(nullable=False)
    target_name: str = Field(nullable=False)

    def __init__(self, directory: FileName, target_name: str):
        super(GetTarget, self).__init__(directory=directory, target_name=target_name)

    def __eq__(self, other):
        return (
            self.directory == other.directory and self.target_name == other.target_name
        )

    def command_name(self):
        return "narta_get_target"


class GetTargetProperty(GetProperty):
    def __init__(self, obj: str, property_name: str):
        super(GetTargetProperty, self).__init__(obj, property_name)

    def command_name(self):
        return "narta_get_target_property"


class GetTest(Command):
    directory: FileName = Field(nullable=False)
    test_name: str = Field(nullable=False)

    def __init__(self, directory: FileName, test_name: str):
        super(GetTest, self).__init__(directory=directory, test_name=test_name)

    def command_name(self):
        return "narta_get_test"

    def __eq__(self, other):
        return (
            Path(self.directory) == Path(other.directory)
            and self.test_name == other.test_name
        )


class GetTestProperty(GetProperty):
    directory: FileName = Field(nullable=False)

    def __init__(self, directory: FileName, obj: str, property_name: str):
        super(GetTestProperty, self).__init__(obj, property_name)
        self.directory = directory

    def command_name(self):
        return "narta_get_test_property"

    def __eq__(self, other):
        return Path(self.directory) == Path(other.directory) and super(
            GetTestProperty, self
        ).__eq__(other)


class GetSourceFile(Command):
    directory: FileName = Field(nullable=False)
    filename: str = Field(nullable=False)

    def __init__(self, directory: FileName, filename: str):
        super(GetSourceFile, self).__init__(directory=directory, filename=filename)

    def command_name(self):
        return "narta_source_file_properties"

    def __eq__(self, other):
        return (
            isinstance(other, GetSourceFile)
            and self.directory == other.directory
            and self.filename == other.filename
        )


class GetSourceFileProperty(GetProperty):
    directory: FileName = Field(nullable=False)

    def __init__(self, directory: FileName, filename: str, property_name: str):
        super(GetSourceFileProperty, self).__init__(filename, property_name)
        self.directory = directory

    def command_name(self):
        return "narta_get_source_file_property"


class GetCacheVariable(Command):
    name: str = Field(nullable=False)

    def __init__(self, name: str):
        super(GetCacheVariable, self).__init__(name=name)

    def command_name(self):
        return "narta_get_cache_variable"

    def __eq__(self, other):
        return isinstance(other, GetCacheVariable) and self.name == other.name


class GetCacheVariableProperty(GetProperty):
    def __init__(self, obj: str, property_name: str):
        super(GetCacheVariableProperty, self).__init__(obj, property_name)

    def command_name(self):
        return "narta_get_cache_variable_property"
