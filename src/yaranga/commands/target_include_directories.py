from pathlib import Path
from typing import Optional, Union

from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.types import FileName


class TargetIncludeDirectories(Command):
    name: str = Field(nullable=False)
    public: Optional[list[str]] = Field(nullable=True)
    private: Optional[list[str]] = Field(nullable=True)
    interface: Optional[list[str]] = Field(nullable=True)

    def __init__(
        self,
        current_dir: Path,
        target_name: str,
        public: Optional[Union[str, list[FileName]]] = None,
        private: Optional[Union[str, list[FileName]]] = None,
        interface: Optional[Union[str, list[FileName]]] = None,
    ):
        if public:
            self.public = public if isinstance(public, list) else [public]
        else:
            self.public = None

        if private:
            self.private = private if isinstance(private, list) else [private]
        else:
            self.private = None

        if interface:
            self.interface = interface if isinstance(interface, list) else [interface]
        else:
            self.interface = interface

        super(TargetIncludeDirectories, self).__init__(
            name=target_name, public=public, private=private, interface=interface
        )

        self.current_dir = current_dir
        self.name = target_name
        self.current_dir: Path = current_dir

    def to_absolute(self, path: Path):
        return Path(self.current_dir, path) if not path.is_absolute() else path

    def command_name(self):
        return "target_include_directories"
