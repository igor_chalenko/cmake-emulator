from abc import ABC

from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.objects.target_type import TargetType
from yaranga.types import FileName
from yaranga.undefined import Ignore


class AddTarget(Command, ABC):
    name: str = Field(nullable=False)
    type: TargetType = Field(nullable=False)
    exclude_from_all: bool = Field(nullable=True)
    sources: list[FileName] = Field(nullable=False)

    def __init__(
        self,
        name: str,
        type_: TargetType,
        exclude_from_all: bool = Ignore,
        sources: list[FileName] = Ignore,
        **kwargs
    ):
        super(AddTarget, self).__init__(
            name=name,
            type=type_,
            exclude_from_all=exclude_from_all,
            sources=sources,
            **kwargs
        )
