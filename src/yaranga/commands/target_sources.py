from typing import Optional

from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.types import ListOrValue, Values


class TargetSources(Command):
    name: str = Field(nullable=False)
    public: Optional[list[str]] = Field(nullable=True)
    private: Optional[list[str]] = Field(nullable=True)
    interface: Optional[list[str]] = Field(nullable=True)

    def __init__(
        self,
        name: str,
        public: Optional[Values[str]] = None,
        private: Optional[Values[str]] = None,
        interface: Optional[Values[str]] = None,
    ):
        super(TargetSources, self).__init__()

        self.name = name
        if public:
            self.public = ListOrValue(public) if isinstance(public, list) else [public]
        else:
            self.public = None

        if private:
            self.private = (
                ListOrValue(private) if isinstance(private, list) else [private]
            )
        else:
            self.private = None

        if interface:
            self.interface = (
                ListOrValue(interface) if isinstance(interface, list) else [interface]
            )
        else:
            self.interface = interface

    def command_name(self):
        return "target_sources"
