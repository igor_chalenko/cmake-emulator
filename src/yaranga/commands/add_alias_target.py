import typing

from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.objects.target import AliasTarget, Target
from yaranga.objects.target_type import TargetType


class AddAliasTarget(Command):
    name: str = Field(nullable=False)
    type: TargetType = Field(const=True, default=TargetType.ALIAS)
    alias: str = Field(nullable=True)

    def __init__(self, name: str, target: Target, **kwargs):
        super(AddAliasTarget, self).__init__(name=name, alias=target.name, **kwargs)

    def command_name(self):
        return "add_library"

    def alias_target(self) -> AliasTarget:
        return typing.cast(AliasTarget, self.alias)
