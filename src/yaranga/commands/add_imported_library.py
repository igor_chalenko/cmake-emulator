from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.objects.target_type import ImportedTargetType
from yaranga.undefined import Undefined


class AddImportedLibrary(Command):
    name: str = Field(nullable=False)
    type: ImportedTargetType = Field(nullable=False)
    imported: bool = Field(nullable=True, default=True)
    imported_global: bool = Field(nullable=True, default=True, alias="GLOBAL")

    def __init__(
        self,
        name: str,
        imported_target_type: ImportedTargetType,
        imported_global: bool = Undefined,
        **kwargs
    ):
        super(AddImportedLibrary, self).__init__(
            name=name,
            type=imported_target_type,
            imported_global=imported_global,
            **kwargs
        )

    def command_name(self):
        return "add_library"
