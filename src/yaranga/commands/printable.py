import abc


class Printable(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def command_name(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def arguments(self):
        raise NotImplementedError()
