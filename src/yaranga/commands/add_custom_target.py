from typing import Union

from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.types import FileName


class AddCustomTarget(Command):
    name: str = Field(nullable=False)
    all: bool = Field(nullable=True)
    command: list[str] = Field(nullable=False)
    depends: list[str] = Field(nullable=True)
    byproducts: list[FileName] = Field(nullable=True)
    working_directory: FileName = Field(nullable=True)
    comment: str = Field(nullable=True)
    job_pool: str = Field(nullable=True)
    verbatim: bool = Field(nullable=True)
    uses_terminal: bool = Field(nullable=True)
    command_expand_lists: bool = Field(nullable=True)
    sources: list[FileName] = Field(nullable=True)

    def __init__(self, *, name: str, command: Union[str, list[str]], **kwargs):
        super(AddCustomTarget, self).__init__(name=name, command=command, **kwargs)

    def command_name(self):
        return "add_custom_target"
