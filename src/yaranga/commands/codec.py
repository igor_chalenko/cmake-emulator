import abc
from enum import Enum
from pathlib import Path
from typing import Any

from yaranga.commands.printable import Printable
from yaranga.objects.target_type import TargetType
from yaranga.undefined import Undefined


class CommandCodec(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def format(self, command: Printable):
        raise NotImplementedError()

    @staticmethod
    def _wrap(_x: str, _wrap: bool):
        _str = str(_x)
        if _x not in (None, Undefined):
            return (
                _str
                if _str.replace("_", "a").isalnum() and not _wrap
                else '"' + _str + '"'
            )
        else:
            return str(_x)

    @staticmethod
    def _format_required(value: Any, sep: str = " ", wrap=True):
        if isinstance(value, TargetType):
            return value.as_cmake()
        else:
            if isinstance(value, list):
                return sep.join(CommandCodec._wrap(x, wrap) for x in value)
            else:
                value = str(value).replace('"', '"').replace("\\", "/")
                return CommandCodec._wrap(value, wrap)

    @staticmethod
    def _format_option(k: str, v: Any, uppercase: bool = True):
        if v:
            k = k.upper() if uppercase else k
            wrapped = CommandCodec._wrap(v, False)
            if isinstance(v, list):
                joined_list = " ".join(CommandCodec._wrap(x, False) for x in v)
                return f"{k} {joined_list}"
            elif isinstance(v, bool) and v:
                return k
            elif isinstance(v, Enum):
                return f"{k} {wrapped}"
            else:
                return f"{k} {wrapped}"
        return ""


class CMakeCommandCodec(CommandCodec):
    def format(self, command: Printable):
        formatted_args = []
        result = f"{command.command_name()}("
        for arg in command.arguments():
            value = arg[1]
            if arg[0]:
                formatted_args.append(self._format_required(value, " ", False))
            if not arg[0]:
                option = self._format_option(arg[1], arg[2], True)
                if option:
                    formatted_args.append(option)
        return result + " ".join(formatted_args) + ")"
