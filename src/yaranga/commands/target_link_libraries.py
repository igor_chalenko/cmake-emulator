from typing import Union, Optional

from yaranga.commands.command import Command
from yaranga.model.field import Field


class TargetLinkLibraries(Command):
    name: str = Field(nullable=False)
    public: Optional[list[str]] = Field(nullable=True)
    private: Optional[list[str]] = Field(nullable=True)
    interface: Optional[list[str]] = Field(nullable=True)

    def __init__(
        self,
        name: str,
        public: Optional[Union[str, list[str]]] = None,
        private: Optional[Union[str, list[str]]] = None,
        interface: Optional[Union[str, list[str]]] = None,
    ):
        super(TargetLinkLibraries, self).__init__()

        self.name = name
        if public:
            self.public = public if isinstance(public, list) else [public]
        else:
            self.public = None

        if private:
            self.private = private if isinstance(private, list) else [private]
        else:
            self.private = None

        if interface:
            self.interface = interface if isinstance(interface, list) else [interface]
        else:
            self.interface = interface

    def command_name(self):
        return "target_link_libraries"
