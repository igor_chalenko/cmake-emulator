from abc import ABC
from typing import Iterable

from yaranga.commands.codec import CMakeCommandCodec
from yaranga.commands.printable import Printable
from yaranga.model.basemodel import BaseModel
from yaranga.model.objectmodel import ObjectModel


from yaranga.undefined import Undefined


class Command(BaseModel, Printable, ABC):
    cmake_codec = CMakeCommandCodec()

    def __str__(self) -> Iterable[str]:
        return Command.cmake_codec.format(self)

    def __repr__(self):
        return str(self)

    def arguments(self):
        res = []
        model: ObjectModel = self.__class__.fields
        for k, v in model.get_mutable_fields().items():
            value = getattr(self, k)
            if value not in (None, Undefined):
                if v.nullable:
                    res.append((False, v.alias, value))
                else:
                    res.append((True, value))
        return res
