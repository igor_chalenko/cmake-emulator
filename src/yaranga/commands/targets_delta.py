from pathlib import Path

from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.types import FileName


class TargetsDeltaBefore(Command):
    directory: FileName = Field(nullable=False)
    prev: str = Field(default="PREV", nullable=False)

    def __init__(self, directory: Path):
        super(TargetsDeltaBefore, self).__init__()

        self.directory = directory

    def command_name(self):
        return "narta_targets"


class TargetsDelta(Command):
    directory: FileName = Field(nullable=False)
    prev: str = Field(default="PREV", nullable=False)
    current: bool = Field(default="CURRENT", nullable=False)
    callback: str = Field(default="directory_callback", nullable=False)
    property_callback: str = Field(
        default="directory_property_callback", nullable=False
    )

    def __init__(self, directory: Path):
        super(TargetsDelta, self).__init__()
        self.directory = directory

    def command_name(self):
        return "narta_targets_delta"


# class TargetProperties(Command):
#     PROPERTY_FUNCTION = 'narta_target_properties'
#
#     def __init__(self, target: Target):
#         self.target = target
#
#     def __str__(self):
#         directory = self.target.source_dir.source_dir.as_posix()
#         return '{}("{}" "{}" CURRENT)'.format(TargetProperties.PROPERTY_FUNCTION,
#                                               directory,
#                                               self.target.name)
#
#
# class TargetPropertiesDelta(Command):
#     DELTA_FUNCTION = 'narta_target_properties_delta'
#     CALLBACK = 'directory_property_callback'
#
#     def __init__(self, target: Target):
#         self.target = target
#
#     def __str__(self):
#         directory = self.target.source_dir.source_dir.as_posix()
#         return '{}("{}" "{}" PREV CURRENT {})'.format(TargetPropertiesDelta.DELTA_FUNCTION,
#                                                       directory,
#                                                       self.target.name,
#                                                       TargetPropertiesDelta.CALLBACK)
