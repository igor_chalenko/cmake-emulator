from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.types import FileName


class ExportCommand(Command):
    path: FileName = Field(nullable=False)

    def __init__(self, path: FileName):
        super(ExportCommand, self).__init__()
        self.path = path

    def command_name(self) -> str:
        return "export"


class ExportDeltaCommand(Command):
    prev_snapshot: str = Field(nullable=False)
    current_snapshot: str = Field(nullable=False)

    def __init__(self, prev_snapshot: str, current_snapshot: str):
        super(ExportDeltaCommand, self).__init__(
            prev_snapshot=prev_snapshot, current_snapshot=current_snapshot
        )

    def command_name(self) -> str:
        return "export_delta"
