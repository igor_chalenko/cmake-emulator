from yaranga.commands.add_target import AddTarget
from yaranga.model.field import Field
from yaranga.objects.target_type import TargetType
from yaranga.types import FileName
from yaranga.undefined import Ignore


class AddExecutable(AddTarget):
    bundle: bool = Field(nullable=True)
    type: TargetType = Field(const=True, default=TargetType.EXECUTABLE)
    win32_executable: bool = Field(nullable=True)

    def __init__(
        self,
        name: str,
        exclude_from_all: bool = Ignore,
        sources: list[FileName] = Ignore,
        **kwargs
    ):
        super(AddExecutable, self).__init__(
            name, TargetType.EXECUTABLE, exclude_from_all, sources, **kwargs
        )

    def command_name(self):
        return "add_executable"
