from typing import Any

from yaranga.commands.command import Command
from yaranga.model.field import Field


class Function(Command):
    args: list[Any] = Field(nullable=False)

    def __init__(self, name: str, *args):
        super(Function, self).__init__()

        self.name = name
        self.args = list(args)

    def required(self):
        return self.args

    def command_name(self):
        return self.name
