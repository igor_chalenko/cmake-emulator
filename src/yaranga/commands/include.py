from yaranga.commands.command import Command
from yaranga.model.field import Field
from yaranga.types import FileName


class IncludeCommand(Command):
    path: FileName = Field(nullable=False)

    def __init__(self, path: FileName):
        super(IncludeCommand, self).__init__()

        self.path = path

    def command_name(self):
        return "include"
