from yaranga.commands.add_target import AddTarget
from yaranga.objects.target import Library


class AddLibrary(AddTarget):
    def __init__(self, **kwargs):
        super(AddLibrary, self).__init__(**kwargs)

    def command_name(self):
        return "add_library"
