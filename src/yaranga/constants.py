"""
Global constants.
"""

__author__ = "Igor Chalenko"
__copyright__ = "Copyright 2021 Igor Chalenko"
__credits__ = ["Igor Chalenko"]
__license__ = "MIT"
__version__ = "0.1.0"
__email__ = "igor.chalenko@gmail.com"
__status__ = "Development"

import re

ENV_PATTERN = re.compile("\\$ENV{(.+)}")
CACHE_PATTERN = re.compile("\\$CACHE{(.+)}")
SOURCE_DIR = "{}_SOURCE_DIR"
BINARY_DIR = "{}_BINARY_DIR"
TOP_LEVEL = "{}_IS_TOP_LEVEL"

COMPUTED_VALUE = "Value Computed by CMake"
FIELDS = "__fields__"
