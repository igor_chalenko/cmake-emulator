import os
import time
import traceback
from pathlib import Path
from threading import Lock
from typing import Type, Sequence

from yaranga.server.router import Router, Listener
from yaranga.server.signals import *
from yaranga.server.transport import ThreadingTransport, SignalTransportConfig
from yaranga.util.sync import synchronized

logger = logging.getLogger(__name__)


class FileTransportConfig(SignalTransportConfig):
    def __init__(self, working_dir: str):
        self.working_dir = working_dir


class FileTransport(ThreadingTransport, Listener):
    def __init__(self, config: FileTransportConfig, router: Router):
        super(FileTransport, self).__init__(config, router)

        self._uid = config.working_dir
        self.working_dir = Path(self._uid)
        self._lock = Lock()

        router.add_listener(self)

        self._continue = True

    @synchronized
    def stop(self):
        self._continue = False
        super(FileTransport, self).stop()

    def _run(self):
        def _listen(_signal_type: Type[Signal]):
            key = _signal_type.mnemonic()
            lock_file_path = self.working_dir / f"{key}.lock"
            payload_file_path = self.working_dir / f"{key}.payload"

            if lock_file_path.exists():
                if payload_file_path.exists():
                    with open(payload_file_path, "rt") as f:
                        payload = f.read()
                    os.remove(payload_file_path)
                else:
                    payload = None
                os.remove(lock_file_path)
                try:
                    signal = signal_type(self._uid, copy.copy(payload))
                    self._router.send(signal)
                except:
                    traceback.print_exc()
                    breakpoint()

        while self._continue:
            for signal_type in (Directory, FlushResponse, Stop):
                _listen(signal_type)
            time.sleep(0.5)

    def on_signal(self, signal: Signal):
        if isinstance(signal, Interrupt):
            lock_file_path = self.working_dir / f"{signal.mnemonic()}.lock"
            if signal.payload():
                payload_file_path = self.working_dir / f"{signal.mnemonic()}.payload"
            else:
                payload_file_path = None
            lock_file_path.touch()
            if payload_file_path:
                with open(payload_file_path, "wt") as f:
                    if isinstance(signal.payload(), Sequence):
                        f.writelines([str(cmd) for cmd in signal.payload()])
                    else:
                        f.write(signal.payload())
