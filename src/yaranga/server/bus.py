from queue import Queue, Empty
from typing import Optional, TypeVar

from yaranga.server.signals import *

T = TypeVar("T")
logger = logging.getLogger(__name__)


class Bus(object):
    def __init__(self, uid: str):
        # this assumes there's no more than one event of each type
        # (the assumption holds for now)
        self.uid = uid
        self._incoming: Queue[Signal] = Queue(maxsize=10)
        self._outgoing: Queue[Signal] = Queue(maxsize=10)

    def route(self, signal: Signal):
        # logger.debug(f"bus {signal.uid}, signal {signal}")
        if isinstance(signal, ExternalSignal):
            self._incoming.put(signal)
        else:
            self._outgoing.put(signal)

    def poll_incoming(self, blocking: bool = True) -> Optional[Signal]:
        try:
            return self._incoming.get(blocking)
        except Empty:
            return None

    def poll_outgoing(self, blocking: bool = True) -> Optional[Signal]:
        try:
            return self._outgoing.get(blocking)
        except Empty:
            return None
