import logging
import threading
from concurrent.futures import Future, ThreadPoolExecutor
from typing import Type

from yaranga.server.context import CMakeContext
from yaranga.server.file import FileTransport, FileTransportConfig
from yaranga.server.http import HttpTransportConfig, HttpTransport
from yaranga.server.router import Router, Listener
from yaranga.server.signals import Signal, Directory, Done, Fault, DirectoryProcessed
from yaranga.server.transport import SignalTransportConfig
from yaranga.util.sync import Lockable, synchronized

logger = logging.getLogger(__name__)


class VirtualMachine(Lockable, Listener):
    def __init__(
        self,
        config: SignalTransportConfig,
        context_class: Type[CMakeContext] = CMakeContext,
    ):
        self._context_class = context_class
        self._stop_event = threading.Event()

        self._tasks: dict[str, Future] = {}

        logger.info("Yaranga VM starting...")
        logger.info(f"Configured transport {config}")

        self.router = Router()
        self._executor = ThreadPoolExecutor()
        if isinstance(config, HttpTransportConfig):
            self.transport = HttpTransport(config, self.router)
        elif isinstance(config, FileTransportConfig):
            self.transport = FileTransport(config, self.router)
        else:
            raise NotImplementedError(f"{config.__class__} is not recognized")
        self.router.add_listener(self)

    @synchronized
    def add_application(self, uid: str):
        def _run_context(_context):
            _context.run()

        if uid in self._tasks:
            raise ValueError(f"The route {uid} is already registered.")
        self.router.add_bus(uid)

        context = self._context_class(uid, self.router)
        self._tasks[uid] = self._executor.submit(_run_context, context)
        return self._tasks[uid]

    @synchronized
    def remove_application(self, uid: str):
        context = self._tasks.get(uid)
        if not context:
            raise ValueError(f"The route {uid} is not registered.")

        context.cancel()
        del self._tasks[uid]

        self.router.remove_bus(uid)

        logger.info(f"removed the route `{uid}`")

    def on_signal(self, signal: Signal):
        if isinstance(signal, Fault):
            self._on_fault(signal)
            return True
        elif isinstance(signal, Directory):
            return self._on_directory(signal)
        elif isinstance(signal, Done):
            self.remove_application(signal.uid)
            return True
        else:
            return False

    def start(self):
        self.transport.start()
        self._stop_event.wait()
        self.transport.stop()

    def stop(self):
        self._stop_event.set()

    def _on_directory(self, signal: Directory):
        if signal.uid not in self._tasks:
            self.add_application(signal.uid)
            return True
        else:
            return False

    def _on_fault(self, fault: Fault):
        logger.error(fault)
        self.router.send(DirectoryProcessed(fault.uid, fault.payload()))
        self.stop()
