import logging
from threading import Thread, Event
from typing import Optional, Callable


logger = logging.getLogger(__name__)


class RunnableState:
    CREATED = "CREATED"
    STARTED = "STARTED"
    STOPPING = "STOPPING"
    STOPPED = "STOPPED"


class Runnable(object):
    _runnable_list: list["Runnable"] = []

    def __init__(self, name: str):
        self.state = RunnableState.CREATED
        self._thread: Optional[Thread] = None
        self._name = name

        Runnable._runnable_list.append(self)

    def start(self, target: Callable):
        self._thread = Thread(
            target=target,
            daemon=True,
            name=self._name,
        )
        self._thread.start()

        self.state = RunnableState.STARTED
        return self._thread

    def stop(self):
        self.state = RunnableState.STOPPING
        if self._thread and self._thread.is_alive():
            self._thread.join()
        self.state = RunnableState.STOPPED

    @classmethod
    def dump(cls):
        for runnable in Runnable._runnable_list:
            if runnable.state != RunnableState.CREATED:
                logger.debug(
                    f"Runnable {runnable._thread.name}, state {runnable.state}"
                )
            else:
                logger.debug(f"Runnable {runnable}, state {runnable.state}")


class Task(object):
    def __init__(self):
        self._stop_event = Event()
        self._stopped_event = Event()

    def stoppable(self):
        pass

    def signal_stop(self):
        self._stopped_event.set()

    def request_stop(self):
        self._stop_event.set()

        self._stopped_event.wait(10)
