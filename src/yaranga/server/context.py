import importlib
import logging
import sys
import time
from pathlib import Path

from yaranga.cmake.context_loader import ContextLoader
from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator, silent
from yaranga.server.abstractrouter import AbstractRouter
from yaranga.server.signals import (
    Stop,
    Directory,
    Flush,
    DirectoryProcessed,
    FlushResponse,
    Done, Fault,
)

logger = logging.getLogger(__name__)


class RestartEventException(Exception):
    pass


class CMakeContext(Translator):
    def __init__(self, uid: str, router: AbstractRouter):
        super(CMakeContext, self).__init__()

        self._uid = uid
        self._router = router

    def stop(self):
        # todo
        pass

    @staticmethod
    def extract_directory(source_dir, binary_dir):
        return Path(source_dir), Path(binary_dir)

    def _clear(self):
        self.sm.mark_as_dirty()
        # restart
        self.sm.mark_as_dirty()
        self.targets.clear()
        self.directories.clear()
        self.cache.clear()
        self.globals.clear()
        self.sm.current_dir = None

    def run(self):
        try:
            while True:
                signal = self._router.select(self._uid, blocking=False)
                if isinstance(signal, Stop):
                    self._router.send(Done(signal.uid))
                    break

                if isinstance(signal, Directory):
                    source_dir, binary_dir = eval(compile("self." + signal.payload(), "<string>", "eval"))
                    if source_dir in self.directories:
                        self._clear()

                    if self.sm.current_dir:
                        prev_dir = self.current_source_dir
                    else:
                        prev_dir = None

                    self.add_directory(source_dir, binary_dir)
                    if prev_dir:
                        rel_path = self.current_source_dir.relative_to(prev_dir)
                    else:
                        rel_path = None

                    try:
                        with Injector(self):
                            self.code_for(self.current_source_dir, rel_path)
                        self.flush(last=True)
                    except RestartEventException:
                        # self._clear()
                        logger.info(f"Restart requested for the task {self._uid}")

                time.sleep(1)
        except Exception as e:
            breakpoint()
            self._router.send(Fault(self._uid, e, {"location": "Translator"}))

    def code_for(self, path: Path, rel_path: Path = None):
        if rel_path:
            module_name_prefix = str(rel_path.as_posix()).replace("/", ".")
            module = importlib.import_module(f"{module_name_prefix}.cmakelists")
        else:
            sys.path.append(str(path))
            module = importlib.import_module("cmakelists")
        module.run(self)

    def flush(self, last=False):
        commands = self.command_tray.commands
        logger.info(f"Flush context, {len(commands)} commands")

        self._router.send(Flush(self._uid, commands))
        # discard the commands
        commands.clear()
        if last:
            self._router.send(DirectoryProcessed(self._uid))
        else:
            while True:
                signal = self._router.select(self._uid, blocking=False)

                if isinstance(signal, FlushResponse):
                    loader = ContextLoader(self.sm)
                    with silent(self):
                        loader.load(signal.payload())
                    break

                if isinstance(signal, Directory):
                    self._router.send(signal)
                    raise RestartEventException()

                time.sleep(1)
