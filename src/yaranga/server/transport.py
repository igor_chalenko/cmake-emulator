import logging

from yaranga.server.router import Router
from yaranga.server.runnable import Runnable

logger = logging.getLogger(__name__)


class SignalTransportConfig(object):
    def __str__(self):
        return str(self.__dict__)


class SignalTransport(object):
    def __init__(self, config: SignalTransportConfig):
        super(SignalTransport, self).__init__()

        self.config = config

    def start(self):
        logger.info(f"Started the transport {self.config}.")

    def wait_for_start(self):
        pass

    def stop(self):
        logger.info(f"Stopped the transport {self.config}.")
        pass

    def __str__(self):
        return f"Signal transport {self.config}"


class ThreadingTransport(SignalTransport):
    def __init__(self, config: SignalTransportConfig, router: Router):
        super(ThreadingTransport, self).__init__(config)

        self._router = router
        self._runnable: Runnable = Runnable(f"{self.__class__.__name__} ({config})")

    def start(self):
        self._runnable.start(self._run)

        super(ThreadingTransport, self).start()

    def stop(self):
        self._runnable.stop()

        super(ThreadingTransport, self).stop()

    def _run(self):
        pass
