import abc
import copy
import logging
import threading
import traceback
from typing import Any

from frozendict import frozendict

logger = logging.getLogger(__name__)


class Signal(object, metaclass=abc.ABCMeta):
    def __init__(self, uid: str, payload: Any = None, params: dict[str, str] = None):
        self.uid = uid
        self._payload = copy.copy(payload)
        self.params: frozendict = frozendict(params) if params else None

    def payload(self) -> Any:
        return self._payload

    @staticmethod
    @abc.abstractmethod
    def mnemonic() -> str:
        raise NotImplementedError()

    def __str__(self):
        res = self.mnemonic()
        if self._payload:
            res += f"({self._payload})"
        return res


class ApplicationSignal(Signal, metaclass=abc.ABCMeta):
    def serialize(self, to):
        if self.payload():
            if isinstance(self.payload(), list):
                to.writelines(bytes(line, "utf-8") for line in self.payload())
            else:
                to.write(bytes(self.payload(), "utf-8"))
        else:
            to.write(bytes(b""))


class ExternalSignal(Signal, metaclass=abc.ABCMeta):
    pass


class Directory(ExternalSignal):
    @staticmethod
    def mnemonic() -> str:
        return "directory"


class FlushResponse(ExternalSignal):
    @staticmethod
    def mnemonic() -> str:
        return "commands"


class Stop(ExternalSignal):
    @staticmethod
    def mnemonic() -> str:
        return "stop"


class DirectoryProcessed(ApplicationSignal):
    @staticmethod
    def mnemonic() -> str:
        return "directory_processed"


class Done(ApplicationSignal):
    def __init__(self, uid: str, payload: Any = None, params: dict[str, str] = None):
        super(Done, self).__init__(uid, payload, params)

    @staticmethod
    def mnemonic() -> str:
        return "done"


class Fault(ExternalSignal):
    def __init__(self, uid: str, e: Exception, params: dict[str, str]):
        super(Fault, self).__init__(uid, traceback.format_exception(e), params)

    def mnemonic(self) -> str:
        return "fault"

    def __str__(self):
        res = f"Fault in {self.params['location']}, thread {threading.current_thread().name}:\n"
        res += "".join(self.payload())
        return res


class Flush(ApplicationSignal):
    @staticmethod
    def mnemonic() -> str:
        return "flush"

    def serialize(self, to):
        if self.payload():
            to.writelines([bytes(str(cmd), "utf-8") for cmd in self.payload()])
        else:
            to.write(bytes(b""))


Interrupt = ApplicationSignal
Message = ExternalSignal

SIGNAL_TYPES = [Directory, DirectoryProcessed, Flush, FlushResponse, Stop]
