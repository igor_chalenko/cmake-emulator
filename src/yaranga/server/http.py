import abc
import io
import logging
import traceback
import urllib.parse
import urllib.request
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Event
from typing import Type, Optional, Any, cast, Tuple

from yaranga.server.router import Router
from yaranga.server.signals import (
    Signal,
    Interrupt,
    ExternalSignal,
    SIGNAL_TYPES,
)
from yaranga.server.transport import SignalTransportConfig, SignalTransport

logger = logging.getLogger(__name__)


class RequestHandlerBase(object, metaclass=abc.ABCMeta):
    def __init__(self):
        self._signal_types = {x.mnemonic(): x for x in SIGNAL_TYPES}

    @abc.abstractmethod
    def uri(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def poll(self, uid: str) -> Signal:
        raise NotImplementedError()

    @abc.abstractmethod
    def send(self, signal: Signal):
        raise NotImplementedError()

    @abc.abstractmethod
    def ok(self, body: bytes):
        raise NotImplementedError()

    @abc.abstractmethod
    def not_found(self):
        raise NotImplementedError()

    def _handle(self, uid: str, payload: Optional[str] = None) -> Optional[Signal]:
        imsi = urllib.parse.urlparse(self.uri())
        params = RequestHandlerBase._to_dict(urllib.parse.parse_qsl(imsi.query))
        logger.debug(f"params = {params}")
        signal_type: Optional[Type[Signal]] = self._signal_types[imsi.path[1:]]
        logger.debug(f"signal type = {signal_type}")

        if not signal_type:
            self.not_found()
            return None

        if issubclass(signal_type, ExternalSignal):
            return self._handle_external_signal(signal_type, uid, params, payload)
        elif issubclass(signal_type, Interrupt):
            return self._handle_interrupt(signal_type, uid)

    def _handle_external_signal(
        self, signal_type: Type[ExternalSignal], uid: str, params: dict, payload=None
    ) -> Signal:
        signal = (
            signal_type(uid, payload, params)
            if payload
            else signal_type(uid, None, params)
        )

        self.send(signal)
        self.ok(b"Ok")
        return signal

    def _handle_interrupt(self, signal_type: Type[Signal], uid: str) -> Signal:
        logger.debug(f"polling for {signal_type}")
        signal: Interrupt = cast(Interrupt, self.poll(uid))
        logger.debug(f"no more polling for {signal_type}")
        buffer = io.BytesIO()
        signal.serialize(buffer)

        val = buffer.getvalue()

        self.ok(val)
        return signal

    @staticmethod
    def _to_dict(params: list[Tuple[str, str]]):
        res = {}
        for key, value in params:
            res[key] = value
        return res


class RequestHandler(BaseHTTPRequestHandler, RequestHandlerBase):
    protocol_version = "HTTP/1.1"
    UID = "Cookie"

    def __init__(self, router: Router, *args):
        # super(RequestHandler, self).__init__(*args)
        self.router = router

        RequestHandlerBase.__init__(self)
        BaseHTTPRequestHandler.__init__(self, *args)

    # noinspection PyPep8Naming
    def do_GET(self):
        logger.debug(f"get request")
        uid = self.headers[RequestHandler.UID]

        try:
            self._handle(uid)
        except Exception as e:
            formatted_exception = traceback.format_exception(e)
            print(formatted_exception)
            breakpoint()
            # self.router.route(Fault(uid, formatted_exception))

    # noinspection PyPep8Naming
    def do_PUT(self):
        uid = self.headers[RequestHandler.UID]

        file_length = int(self.headers["Content-Length"])
        payload: str = self.rfile.read(file_length).decode("utf-8")

        try:
            self._handle(uid, payload)
        except Exception as e:
            formatted_exception = traceback.format_exception(e)
            print(formatted_exception)
            # self.handler.route(Fault(uid, formatted_exception))

    def log_message(self, format_str: str, *args: Any):
        logger.debug(format_str % args)

    def send(self, signal: Signal):
        self.router.send(signal)

    def poll(self, uid: str, blocking: bool = True) -> Signal:
        return self.router.poll_outgoing(uid, blocking)

    def ok(self, body: bytes):
        self.send_response(200)
        self.send_header("Content-Type", "text/plain")
        self.send_header("Content-Length", str(len(body)))
        self.end_headers()

        self.wfile.write(body)

    def not_found(self, body=b"not found"):
        self.send_response(404)
        self.send_header("Content-Type", "text/plain")
        self.send_header("Content-Length", str(len(body)))
        self.end_headers()

        self.wfile.write(body)

    def uri(self) -> str:
        return self.path


class HttpTransportConfig(SignalTransportConfig):
    def __init__(self, host: str, port: int):
        self.host = host
        self.port = port


class HttpServer(HTTPServer):
    def __init__(
        self,
        start_event: Event,
        server_address: tuple[str, int],
        request_handler_class,
        bind_and_activate: bool = True,
    ) -> None:
        super().__init__(server_address, request_handler_class, bind_and_activate)
        self._start_event = start_event

    def service_actions(self):
        if not self._start_event.is_set():
            self._start_event.set()


class HttpTransport(SignalTransport):  # , metaclass=Singleton):
    def __init__(self, config: HttpTransportConfig, router: Router):
        super(HttpTransport, self).__init__(config)

        self._httpd: Optional[HttpServer] = None
        self._config = config
        self._router = router
        self._start: Event = Event()

    def start(self):
        def handler(*args):
            return RequestHandler(self._router, *args)

        server_address = (self._config.host, self._config.port)
        self._httpd = HttpServer(self._start, server_address, handler)
        self.config.port = self._httpd.socket.getsockname()[1]

        super(HttpTransport, self).start()
        self._run()

    def wait_for_start(self):
        return self._start.wait()

    def _run(self):
        self._httpd.serve_forever(poll_interval=0.5)

    def stop(self):
        if self._httpd:
            logger.debug(f"Stopping the http server...")
            self._httpd.shutdown()
            self._httpd.server_close()
            self._httpd = None

        super(HttpTransport, self).stop()

    def _listen_url(self):
        return f"http://{self._config.host}:{self._config.port}"

    def __str__(self):
        return self._listen_url()
