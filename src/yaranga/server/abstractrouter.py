import abc
from typing import Optional

from yaranga.server.signals import Signal


class Sender(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def send(self, *signals: Signal):
        raise NotImplementedError()


class SenderReceiver(Sender, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def receive(self, signal: Signal):
        raise NotImplementedError()


class Selector(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def select(self, uid: str, blocking: bool = True) -> Optional[Signal]:
        raise NotImplementedError()


class AbstractRouter(Selector, Sender, metaclass=abc.ABCMeta):
    pass
