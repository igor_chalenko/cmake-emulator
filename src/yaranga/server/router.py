from typing import TypeVar, Optional

from yaranga.server.abstractrouter import AbstractRouter
from yaranga.server.bus import Bus
from yaranga.server.signals import *
from yaranga.util.sync import Lockable

T = TypeVar("T")
logger = logging.getLogger(__name__)


class Listener(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def on_signal(self, signal: Signal):
        raise NotImplementedError()


class Router(AbstractRouter, Lockable):
    def __init__(self):
        self._busses: dict[str, Bus] = {}
        self._listeners: list[Listener] = []

    def add_listener(self, listener: Listener):
        self._listeners.append(listener)

    def add_bus(self, uid: str):
        bus = Bus(uid)
        self._busses[uid] = bus

        logger.info(f"Added the signal bus {uid}")
        return bus

    def remove_bus(self, uid: str):
        if uid not in self._busses:
            raise ValueError(f"The bus {uid} is not registered.")
        del self._busses[uid]

        logger.info(f"Removed the bus {uid}")

    def send(self, signal: Signal):
        logger.debug(f"Routing the signal {signal}")
        uid = signal.uid

        handled = False
        for listener in self._listeners:
            handled = listener.on_signal(signal)
            if handled:
                break

        if not handled:
            bus = self._busses[uid]
            bus.route(signal)

    def select(self, uid: str, blocking: bool = True) -> Optional[Signal]:
        bus = self._busses[uid]
        return bus.poll_incoming(blocking)

    def poll_outgoing(self, uid: str, blocking: bool = True) -> Optional[Signal]:
        bus = self._busses[uid]
        return bus.poll_outgoing(blocking)
