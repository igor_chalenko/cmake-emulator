"""
Yaranga state consists of two parts, determinable and non-determinable.
This happens for two reasons. First, Yaranga cannot load CMake state in
its entirety - CMake does not provide facilities to obtain all properties
of a target, for example. Second, certain operations, such as include, may
invalidate arbitrary parts of the already loaded state. In general, as
the execution progresses, parts of determinable state may become
non-determinable, and vice versa. Undefined values mark this division
between known to be true and not known to be true: accessing a property
that was never loaded, or became stale, results in the value of
the :class:`UndefinedType` type. Once such an undefined property is read,
``CMake`` is spawned to give the up-to-date value of that property, which
then becomes a part of determinable state.
"""
from typing import Any


class UndefinedType(object):
    """
    A sentinel type that marks properties that must be re-synced with
    ``CMake``. There's normally no need to use this class directly, same as
    ``NoneType``. The singleton instance :data:`Undefined` should be used
    instead.
    """

    _singleton = None

    def __new__(cls):
        if UndefinedType._singleton is None:
            UndefinedType._singleton = super(UndefinedType, cls).__new__(cls)
        return UndefinedType._singleton

    def __repr__(self):
        return "Undefined"

    def __bool__(self):
        return False

    def __getitem__(self, item: Any):
        return Undefined

    def __eq__(self, other):
        if isinstance(other, bool):
            return bool(self) == other
        return super(UndefinedType, self).__eq__(other)


Undefined = UndefinedType()
Ignore = Undefined

"""
Holds the singleton instance of the class :class:`UndefinedType`.
"""
