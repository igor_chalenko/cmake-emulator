from yaranga.commands.get import GetTest
from yaranga.objects.cmakeobject import CMakeObject


class TestDict(CMakeObject):
    def on_dirty_read(self, key: str, context):
        context.add_command(GetTest(context.current_source_dir, key))
        context.flush()
        if key not in self:
            raise ValueError(
                f"get_property given TEST names that do not exist:\n\t{key}"
            )
        return self[key]


class TestDictWrapper:
    def __init__(self, context):
        self.context = context

    def __getitem__(self, item: str):
        return self.context.current_dir.tests_dict[item]
