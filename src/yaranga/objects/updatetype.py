from enum import Enum


class UpdateType(Enum):
    SET = 0
    PREPEND = 1
    APPEND = 2
    APPEND_STRING = 3
