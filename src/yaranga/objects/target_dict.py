from yaranga.commands.get import GetTarget
from yaranga.objects.cmakeobject import CMakeObject


class TargetDict(CMakeObject):
    def on_dirty_read(self, key: str, context):
        context.add_command(GetTarget(context.current_source_dir, key))
        context.flush()
        if key not in self:
            raise ValueError(
                f"get_property could not find TARGET {key}. "
                f"Perhaps it has not yet been created."
            )
        return self[key]
