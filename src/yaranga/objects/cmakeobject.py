from inspect import getmembers
from pprint import pformat
from types import FunctionType
from typing import Any

from yaranga.constants import FIELDS
from yaranga.model.basemodel import BaseModel
from yaranga.model.model import ModelMetaclass
from yaranga.model.objectmodel import ObjectModel
from yaranga.objects.updatetype import UpdateType
from yaranga.types import Value
from yaranga.undefined import Undefined


class CMakeObjectModel(ObjectModel):
    def mark_as_dirty(self, obj):
        super(CMakeObjectModel, self).mark_as_dirty(obj)

        for prop in obj.properties.keys():
            obj.properties[prop] = Undefined


class CMakeObject(BaseModel, metaclass=ModelMetaclass, fom=CMakeObjectModel):
    """
     A scope associates a name with a number of properties. Examples of
     ``CMake`` scopes are :py:class:`Target`, :py:class:`Directory`,
     :py:class:`Test`, and:py:class:`SourceFile`. Scope properties can be set or
     updated using either dictionary or attribute syntax:

    .. code-block: python

     assert target.exclude_from_all == target['EXCLUDE_FROM_ALL']

     Scope properties that are implemented as attributes, are the properties
     that are documented and recognized by ``CMake``. If a scope property is
     not an attribute in the respective scope subclass, it's handled as a string
     property; no conversion or validation is attempted for it, except for
     the ``NOTFOUND`` conversion. Every attribute may also be accessed as
     a key/value pair in a dictionary, these two methods are interchangeable.

     Access requests to the attributes apply field-based logic implemented
     by :class:`BaseModel`. The example below shows the duality between
     attributes and properties of a scope:

     .. literalinclude ../../examples/executable_example.py

    """

    def __init__(self, **properties):
        object_model = getattr(self, FIELDS)
        attrs = {
            k: v
            for k, v in properties.items()
            if k in object_model and v is not Undefined
        }

        super(CMakeObject, self).__init__(**attrs)

        self.properties = {}

        # store input properties into respective attributes/properties
        for k, v in filter(lambda x: x[0] not in attrs, properties.items()):
            self.properties[k] = v

    def merge_property(self, name: str, value: Value):
        if value:
            if not isinstance(value, list):
                value = [value]
            if name in self.properties:
                self[name].extend(value)
            else:
                self[name] = value

    def set_property(self, name: str, value: Value, update_type: UpdateType):
        if value is not Undefined:
            if update_type == UpdateType.SET:
                self._setitem(name, value)
            else:
                current_value = self._getitem(name)
                if current_value not in (None, Undefined):
                    if isinstance(current_value, list):
                        if not isinstance(value, list):
                            value = [value]
                        if update_type == UpdateType.PREPEND:
                            self[name] = value + current_value
                        elif update_type == UpdateType.APPEND:
                            self[name] = current_value + value
                        else:
                            raise ValueError(
                                "set_property may not use APPEND_STRING on lists"
                            )
                    else:
                        if update_type == UpdateType.PREPEND:
                            self._setitem(name, [value, current_value])
                        elif update_type == UpdateType.APPEND:
                            self._setitem(name, [current_value, value])
                        else:
                            self._setitem(name, f"{current_value}{value}")
                else:
                    self._setitem(name, value)
        elif name in self:
            if update_type == UpdateType.SET:
                del self[name]
            else:
                raise ValueError("set_property may not prepend/append `Undefined`")

    def mark_as_dirty(self):
        # should not fire on_update
        self.__class__.fields.mark_as_dirty(self)

    def on_dirty_read(self, key: str, context):
        pass

    def __setattr__(self, name: str, value: Value):
        return self._setattr(name, value)

    def _setattr(self, name: str, value: Value):
        if name not in ["properties"]:
            fields: ObjectModel = getattr(self, FIELDS)
            (current_value, value) = fields.convert(value, self, name)
            object.__setattr__(self, name, value)
            return value is not Undefined
        else:
            object.__setattr__(self, name, value)

    def _getitem(self, alias: str) -> Any:
        name = self.attr_from_property(alias)
        if name:
            res = self.get_or_undefined(name)
        else:
            res = self.properties.get(alias, Undefined)
        return res

    def __getitem__(self, alias: Any) -> Any:
        return self._getitem(str(alias))

    def get(self, alias: str, default=None):
        name = self.attr_from_property(alias)
        if name:
            res = self.__dict__.get(alias, default)
        else:
            res = self.properties.get(alias, default)
        return res

    def keys(self):
        return self.properties.keys()

    def values(self):
        # attr_values = [self.get_or_undefined(prop) for prop in self.get_fields().fields.keys()]
        return self.properties.values()

    def clear(self):
        self.properties.clear()

    def _setitem(self, alias: str, value):
        name = self.attr_from_property(alias)
        if name:
            self._setattr(name, value)
        else:
            # not a known field, save as a custom property
            self.properties[alias] = value

    def __setitem__(self, alias: Any, value):
        self._setitem(str(alias), value)

    def __delitem__(self, alias: str):
        name = self.attr_from_property(alias)
        if name:
            object.__setattr__(self, name, None)
        else:
            del self.properties[alias]

    def __contains__(self, alias: Any):
        name = self.attr_from_property(str(alias))
        if name:
            return getattr(self, name) is not None
        else:
            return str(alias) in self.properties

    def _attributes(self):
        skip_names = {
            name
            for name, value in getmembers(type(self))
            if isinstance(value, FunctionType)
        }
        return {
            name: getattr(self, name)
            for name in dir(self)
            if name[0] != "_" and name not in skip_names and hasattr(self, name)
        }

    def __str__(self):
        return f"{{{self.__class__.__name__}}} " + pformat(self.__dict__)
