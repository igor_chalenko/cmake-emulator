import copy
from enum import Enum
from pathlib import Path
from typing import List, Any, Union, Optional

from yaranga.model.field import Field
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.objects.directory import Directory
from yaranga.objects.target_type import TargetType, ImportedTargetType
from yaranga.objects.updatetype import UpdateType
from yaranga.types import FileName, Values


class LinkVisibility(Enum):
    PUBLIC = 1
    PRIVATE = 2
    INTERFACE = 3


class AndroidArch(Enum):
    ARMV7_A = "armv7-a"
    """
    ARMv7-A (armv7-a)
    """

    ARMV7_A_HARD = "armv7-a-hard"
    """
    ARMv7-A, hard-float ABI (armv7-a)
    """

    ARM64_V8A = "arm64-v8a"
    """
    ARMv8-A, 64bit (arm64-v8a)
    """

    X86 = "x86"
    """
    x86 (x86)
    """

    X86_64 = "x86_64"
    """
    x86_64 (x86_64)
    """


class StlType(Enum):
    NONE = "none"
    """
    No C++ Support
    """

    SYSTEM = "system"
    """
    Minimal C++ without STL
    """

    GABI_STATIC = "gabi++_static"
    """
    GAbi++ Static
    """

    GABI_SHARED = "gabi++_shared"
    """
    GAbi++ Shared
    """

    GNU_STL_STATIC = "gnustl_static"
    """
    GNU libstdc++ Static
    """

    GNU_STL_SHARED = "gnustl_shared"
    """
    GNU libstdc + + Shared
    """

    STLPORT_STATIC = "stlport_static"
    """
    STLport Static
    """

    STLPORT_SHARED = "stlport_shared"
    """
    STLport Shared
    """


class Target(CMakeObject):
    """
    From
    `CMake <https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#introduction>`_
    documentation:

        "A CMake-based build system is organized as a set of high-level logical targets. Each target
        corresponds to an executable or library, or is a custom target containing custom commands.
        Dependencies between the targets are expressed in the buildsystem to determine the build
        order and the rules for regeneration in response to change."

    """

    additional_clean_files: List[str]

    android_ant_additional_options: str
    """
    Set the additional options for Android Ant build system. This is a string value containing all
    command line options for the Ant build. This property is initialized by the value of the
    ``CMAKE_ANDROID_ANT_ADDITIONAL_OPTIONS`` variable if it is set when a target is created.
    """
    android_api: int

    android_api_min: int
    """
    Set the Android MIN API version (e.g. 9). The version number must be a positive decimal integer.
    This property is initialized by the value of the ``CMAKE_ANDROID_API_MIN`` variable if it is set
    when a target is created. Native code builds using this API version.
    """
    android_arch: AndroidArch

    android_assets_directories: list[str]
    """
    Set the Android assets directories to copy into the main assets folder before build. This a string
    property that contains the directory paths separated by semicolon. This property is initialized by
    the value of the ``CMAKE_ANDROID_ASSETS_DIRECTORIES`` variable if it is set when a target is 
    created.
    """

    android_gui: bool
    """
    When Cross Compiling for Android with NVIDIA Nsight Tegra Visual Studio Edition, this property
    specifies whether to build an executable as an application package on Android.
    When this property is set to true the executable when built for Android will be created as
    an application package. This property is initialized by the value of the ``CMAKE_ANDROID_GUI`` 
    variable if it is set when a target is created.

    Add the AndroidManifest.xml source file explicitly to the target add_executable() command
    invocation to specify the root directory of the application package source.
    """

    android_jar_dependencies: list[FileName]
    """
    Set the Android property that specifies JAR dependencies. This is a string value property. 
    This property is initialized by the value of the ``CMAKE_ANDROID_JAR_DEPENDENCIES`` variable 
    if it is set when a target is created.
    """

    android_jar_directories: list[FileName]
    """
    Set the Android property that specifies directories to search for the JAR libraries.
    This a string property that contains the directory paths separated by semicolons. This property
    is initialized by the value of the CMAKE_ANDROID_JAR_DIRECTORIES variable if it is set when 
    a target is created.

    Contents of ``ANDROID_JAR_DIRECTORIES`` may use "generator expressions" with the syntax $<...>. 
    """

    android_java_source_dir: list[Path]
    """
    Set the Android property that defines the Java source code root directories. This a string
    property that contains the directory paths separated by semicolon. This property is initialized 
    by the value of the ``CMAKE_ANDROID_JAVA_SOURCE_DIR`` variable if it is set when a target 
    is created.
    """

    android_native_lib_dependencies: list[FileName]
    """
    Set the Android property that specifies the .so dependencies. This is a string property.
    This property is initialized by the value of the CMAKE_ANDROID_NATIVE_LIB_DEPENDENCIES variable 
    if it is set when a target is created. Contents of ``ANDROID_NATIVE_LIB_DEPENDENCIES`` may use
    "generator expressions" with the syntax $<...>.
    """

    android_native_lib_directories: list[FileName]
    """
    Set the Android property that specifies directories to search for the .so libraries.
    This a string property that contains the directory paths separated by semicolons.
    This property is initialized by the value of the ``CMAKE_ANDROID_NATIVE_LIB_DIRECTORIES`` 
    variable if it is set when a target is created. Contents of ``ANDROID_NATIVE_LIB_DIRECTORIES`` 
    may use "generator expressions" with the syntax $<...>.
    """

    bundle: bool
    """
    If a module library target has this property set to true it will be built as a 
    CFBundle when built on the mac. It will have the directory structure required for 
    a CFBundle and will be suitable to be used for creating Browser Plugins or other
    application resources.
    """

    bundle_extension: str
    """
    The file extension used to name a ``BUNDLE``, a ``FRAMEWORK``, or a ``MACOSX_BUNDLE``
    target on the macOS and iOS. The default value is bundle, framework, or app for 
    the respective target types.
    """

    c_extensions: bool
    """
    This property specifies whether compiler specific extensions should be used. For some 
    compilers, this results in adding a flag such as `-std=gnu11` instead of `-std=c11` to
    the compile line. This property is ``ON`` by default. The basic C standard level is 
    controlled by the ``C_STANDARD`` target property.
    """

    c_standard: str
    """
    The C standard whose features are requested to build this target.
    This property specifies the C standard whose features are requested to build this target.
    For some compilers, this results in adding a flag such as -std=gnu11 to the compile line.
    For compilers that have no notion of a C standard level, such as Microsoft Visual C++
    before VS 16.7, this property has no effect.

    Supported values are:
    
        - 90 C89/C90
        - 99 C99
        - 11 C11
        - 17 C17
        - 23 C23
        
    """

    c_standard_required: bool
    """
    Boolean describing whether the value of ``C_STANDARD`` is a requirement.

    If this property is set to ``ON``, then the value of the ``C_STANDARD`` target property
    is treated as a requirement. If this property is ``OFF`` or unset, the ``C_STANDARD`` 
    target property is treated as optional and may "decay" to a previous standard if 
    the requested is not available. For compilers that have no notion of a C standard 
    level, such as Microsoft Visual C++ before VS 16.7, this property has no effect.
    """

    common_language_runtime: str
    imported_common_language_runtime: str

    imported_configurations: list[str]

    compile_definitions: list[str]
    interface_compile_definitions: list[str]
    """
    The ``COMPILE_DEFINITIONS`` property may be set to a semicolon-separated list 
    of preprocessor definitions using the syntax `VAR` or `VAR=value`. Function-style
    definitions are not supported. `CMake` will automatically escape the value correctly
    for the native build system (note that `CMake` language syntax may require escapes 
    to specify some values).
    """

    compile_features: list[str]
    """
    Compiler features enabled for this target.

    The list of features in this property are a subset of the features listed in 
    the ``CMAKE_C_COMPILE_FEATURES``, ``CMAKE_CUDA_COMPILE_FEATURES``, and 
    ``CMAKE_CXX_COMPILE_FEATURES`` variables.
    """

    compile_options: list[str]
    interface_compile_options: list[str]

    imported_libname: str = Field(read_only=True)

    link_libraries: list[str]
    interface_link_libraries: list[str]

    location: Path = Field(read_only=True)
    imported_location: Path = Field(read_only=True)

    name: str = Field(read_only=True)
    type: Union[ImportedTargetType, TargetType] = Field(read_only=True)
    source_dir: Directory = Field(read_only=True)
    sources: list[FileName]
    interface_sources: list[FileName]

    exclude_from_all: bool
    imported: bool
    imported_global: bool
    imported_implib: Path
    imported_link_dependent_libraries: list[str]

    imported_link_interface_languages: list[str]
    """
    Set this to the list of languages of source files compiled to produce a STATIC IMPORTED library 
    (such as C or CXX). CMake accounts for these languages when computing how to link a target to
    the imported library. For example, when a C executable links to an imported C++ static library 
    `CMake` chooses the C++ linker to satisfy language runtime dependencies of the static library.
    
    This property is ignored for targets that are not STATIC libraries. This property is ignored for 
    non-imported targets.
    """

    link_interface_multiplicity: int
    imported_link_interface_multiplicity: int
    imported_no_soname: bool
    imported_no_system: bool
    imported_objects: list[Path]
    imported_soname: str

    position_independent_code: bool
    interface_position_independent_code: bool
    """
    The POSITION_INDEPENDENT_CODE property determines whether position independent executables 
    or shared libraries will be created. This property is True by default for SHARED and MODULE 
    library targets and False otherwise. This property is initialized by the value of 
    the CMAKE_POSITION_INDEPENDENT_CODE variable if it is set when a target is created.
    """

    @property
    def binary_dir(self):
        """
        This read-only property reports the value of the ``CMAKE_CURRENT_BINARY_DIR`` variable in
        the directory in which the target was defined.
        """
        return self.source_dir.binary_dir

    def set_sources(
        self,
        sources: Union[list[str], str],
        properties: Optional[dict[str, Any]] = None,
    ):
        if not isinstance(sources, list):
            sources = [sources]
        for source in sources:
            if properties is None:
                properties = {"LABELS": []}
            for k, v in properties.items():
                self.source_dir.set_source_property(source, k, v, UpdateType.SET)
        self.sources = copy.deepcopy(sources)

    def __init__(
        self,
        name: str,
        type_: Union[ImportedTargetType, TargetType],
        source_dir: Directory,
        **properties
    ):
        super(Target, self).__init__(
            name=name, type=type_, source_dir=source_dir, **properties
        )


class Executable(Target):
    win32_executable: bool
    include_directories: list[FileName] = Field(default=None)

    def __init__(
        self,
        name: str,
        source_dir: Directory,
        sources: Values[FileName] = None,
        **properties
    ):
        """
        New in version 3.11: The source files can be omitted if they are added
        later using target_sources().
        """
        super(Executable, self).__init__(
            name, TargetType.EXECUTABLE, source_dir, **properties
        )

        if sources:
            self.set_sources(sources)


class ImportedLibrary(Target):
    # this redefines `imported` in Target
    imported: bool = Field(const=True, default=True, read_only=True)

    # this redefines `type` in Target
    type: ImportedTargetType

    # avoid clash with 'global' keyword
    is_global: bool = Field(default=False, alias="GLOBAL")

    def __init__(
        self,
        name: str,
        imported_target_type: ImportedTargetType,
        source_dir: Directory,
        **properties
    ):
        super(ImportedLibrary, self).__init__(
            name=name, type_=imported_target_type, source_dir=source_dir, **properties
        )


class ImportedExecutable(Target):
    # this redefines `imported` in Target
    imported: bool = Field(const=True, default=True, read_only=True)

    # avoid clash with 'global' keyword
    is_global: bool = Field(default=False, alias="GLOBAL")

    def __init__(self, name: str, source_dir: Directory, **properties):
        super(ImportedExecutable, self).__init__(
            name, TargetType.EXECUTABLE, source_dir, **properties
        )


class AliasTarget(Target):
    alias_target: Target

    @property
    def alias_global(self) -> bool:
        if self.alias_target.imported:
            return bool(self.alias_target.imported_global)
        else:
            return False

    @property
    def aliased_target(self):
        return self.alias_target.name

    def __init__(self, name: str, source_dir: Directory, target: Target, **properties):
        super(AliasTarget, self).__init__(
            name, TargetType.ALIAS, source_dir, **properties
        )

        self.alias_target = target


class CustomTarget(Target):
    command: list[str] = Field(nullable=False)
    all: bool = Field(nullable=True)

    def __init__(
        self,
        source_dir: Directory,
        name: str,
        command: Union[str, list[str]],
        all_: Optional[bool] = None,
        working_directory: Optional[FileName] = None,
        **kwargs
    ):
        super(CustomTarget, self).__init__(
            source_dir=source_dir,
            name=name,
            all=all_,
            type_=TargetType.UTILITY,
            command=command,
            working_directory=working_directory,
            **kwargs
        )


class Library(Target):
    framework: bool
    include_directories: list[FileName]
    interface_include_directories: list[FileName]
