from enum import Enum


class TargetType(Enum):
    """
    CMake target type. Enum values must match ``CMake`` constants.
    """

    MODULE_LIBRARY = 1
    STATIC_LIBRARY = 2
    SHARED_LIBRARY = 3
    OBJECT_LIBRARY = 4
    """
    The target is an interface library.
    """
    INTERFACE_LIBRARY = 5
    """
    The target is a binary library.
    """
    EXECUTABLE = 6
    """
    The target is a binary executable.
    """
    ALIAS = 7
    """
    An ALIAS target is a name which may be used interchangeably with a binary
    target name in read-only contexts. A primary use-case for ALIAS targets is
    for example or unit test executables accompanying a library, which may be 
    part of the same build system or built separately based on user 
    configuration.
    """
    IMPORTED = (8,)
    """
    An IMPORTED target represents a pre-existing dependency. Usually such 
    targets are defined by an upstream package and should be treated as 
    immutable. 
    """

    UTILITY = 9

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"TargetType.{self.name}"

    def __eq__(self, other) -> bool:
        return self.name == other

    def as_cmake(self):
        if self == TargetType.OBJECT_LIBRARY:
            return "OBJECT"
        if self == TargetType.INTERFACE_LIBRARY:
            return "INTERFACE"
        if self == TargetType.STATIC_LIBRARY:
            return "STATIC"
        elif self == TargetType.SHARED_LIBRARY:
            return "SHARED"
        elif self == TargetType.MODULE_LIBRARY:
            return "MODULE"
        return self.name


class ImportedTargetType(Enum):
    MODULE = 1
    STATIC = 2
    SHARED = 3
    OBJECT = 4
    INTERFACE = 5
    UNKNOWN = 6

    def __str__(self) -> str:
        return self.name

    def __eq__(self, other) -> bool:
        return self.name == other
