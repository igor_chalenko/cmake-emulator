from pathlib import Path
from typing import Any, List, Optional

from yaranga.model.exceptions import ValidationError, NotWritableAttributeError
from yaranga.model.field import Field
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.objects.source_dict import SourceDict
from yaranga.objects.source_file import SourceFile
from yaranga.objects.test import Test
from yaranga.objects.test_dict import TestDict
from yaranga.objects.updatetype import UpdateType
from yaranga.types import FileName, Value
from yaranga.undefined import Undefined


def validate_path(_: object, v: Path):
    if not v.is_absolute():
        raise ValidationError(f"The path {v} is not absolute")
    return v


class Directory(CMakeObject):
    @property
    def macros(self):
        """
        This read-only property specifies the list of CMake macros currently
        defined. It is intended for debugging purposes. See the ``macro()``
        command.
        """
        return self._macros

    @macros.setter
    def macros(self, value: Any):
        raise NotWritableAttributeError("macros")

    @property
    def buildsystem_targets(self):
        """
        This read-only directory property contains a semicolon-separated list
        of buildsystem targets added in the directory by calls to
        the ``add_library()``, ``add_executable()``, and ``add_custom_target()``
        commands. The list does not include any Imported Targets or Alias Targets,
        but does include Interface Libraries. Each entry in the list is
        the logical name of a target, suitable to pass to the ``get_property()``
        command `TARGET` option.
        """
        return self._buildsystem_targets

    @buildsystem_targets.setter
    def buildsystem_targets(self, value: Any):
        raise NotWritableAttributeError("buildsystem_targets")

    @property
    def cache_variables(self):
        """
        This read-only property specifies the list of CMake cache variables
        currently defined. It is intended for debugging purposes.
        """
        return self._cache_variables

    @cache_variables.setter
    def cache_variables(self, value: Any):
        raise NotWritableAttributeError("cache_variables")

    @property
    def imported_targets(self):
        """
        This read-only directory property contains a semicolon-separated list
        of Imported Targets added in the directory by calls to
        the ``add_library()`` and ``add_executable()`` commands. Each entry in
        the list is the logical name of a target, suitable to pass to
        the ``get_property()`` command `TARGET` option when called in the same
        directory.
        """
        return self._imported_targets

    @imported_targets.setter
    def imported_targets(self, value: Any):
        raise NotWritableAttributeError("imported_targets")

    @property
    def parent_directory(self):
        """
        Source directory that added current subdirectory.

        This read-only property specifies the source directory that added
        the current source directory as a subdirectory of the build.
        In the top-level directory the value is the empty-string.
        """
        return self.parent.source_dir if self.parent else None

    @parent_directory.setter
    def parent_directory(self, path: str):
        raise NotWritableAttributeError("parent_directory")

    @property
    def sources(self):
        if self._sources is not Undefined:
            return [x.location for x in self._sources.values()]
        else:
            return Undefined

    @sources.setter
    def sources(self, _: Any):
        raise NotWritableAttributeError("sources")

    @property
    def sub_dirs(self):
        return self._subdirectories

    @property
    def subdirectories(self):
        """
        This read-only directory property contains a semicolon-separated list
        of subdirectories processed so far by the ``add_subdirectory()`` or
        ``subdirs()`` commands. Each entry is the absolute path to the source
        directory (containing the CMakeLists.txt file). This is suitable
        to pass to the ``get_property()`` command `DIRECTORY` option.
        """
        return [subdir.source_dir for subdir in self._subdirectories]

    @subdirectories.setter
    def subdirectories(self, _: Any):
        raise NotWritableAttributeError("subdirectories")

    @property
    def tests(self):
        """
        This read-only property holds a semicolon-separated list of tests
        defined so far, in the current directory, by ``add_test()``
        command.
        """
        return (
            [x.name for x in self.tests_dict.values()]
            if self.tests_dict is not Undefined
            else Undefined
        )

    @tests.setter
    def tests(self, _: Any):
        raise NotWritableAttributeError("tests")

    @property
    def variables(self):
        """
        This read-only property specifies the list of ``CMake`` variables
        currently defined. It is intended for debugging purposes.
        """
        return self._variables

    @variables.setter
    def variables(self, value):
        raise NotWritableAttributeError("variables")

    additional_clean_files: list[str]
    """
    A ;-list of files or directories that will be removed as a part of 
    the global clean target. It is useful for specifying generated files 
    or directories that are used by multiple targets or by CMake itself, 
    or that are generated in ways which cannot be captured as outputs or 
    byproducts of custom commands.
    """

    binary_dir: Path = Field(read_only=True, validators=[validate_path])
    """
    This read-only directory property reports absolute path to the binary 
    directory corresponding to the source on which it is read.
    """

    clean_no_custom: bool
    """
    Set to ``True`` to tell Makefile Generators not to remove the outputs of
    custom commands for this directory during the make clean operation. This
    is ignored on other generators because it is not possible to implement.
    """

    cmake_configure_depends: list[Path]
    """
    Tell CMake about additional input files to the configuration process. 
    If any named file is modified the build system will re-run `CMake` 
    to re-configure the file and generate the build system again.

    Specify files as a semicolon-separated list of paths. Relative paths
    are interpreted as relative to the current source directory.
    """

    compile_definitions: list[str]
    """
    This property specifies the list of options given so far to 
    the ``add_compile_definitions()`` (or ``add_definitions()``) command.

    The `COMPILE_DEFINITIONS` property may be set to a semicolon-separated
    list of preprocessor definitions using the syntax ``VAR`` or ``VAR=value``.
    Function-style definitions are not supported. `CMake` will automatically
    escape the value correctly for the native build system (note that
    `CMake` language syntax may require escapes to specify some values).
    
    The corresponding ``COMPILE_DEFINITIONS_<CONFIG>`` property may be set 
    to specify per-configuration definitions. Generator expressions should be
    preferred instead of setting the alternative property.
    """

    exclude_from_all: bool
    """
    Set this directory property to a true value on a subdirectory to exclude
    its targets from the "all" target of its ancestors. If excluded, running
    e.g. make in the parent directory will not build targets the subdirectory
    by default. This does not affect the "all" target of the subdirectory
    itself. Running e.g. make inside the subdirectory will still build its 
    targets.
    """

    implicit_depends_include_transform: list[str]
    """
    This property specifies rules to transform macro-like #include lines during
    implicit dependency scanning of C and C++ source files. The list of rules
    must be semicolon-separated with each entry of the form 
    ``A_MACRO(%)=value-with-%`` (the % must be literal). During dependency 
    scanning occurrences of ``A_MACRO(...)`` on ``#include`` lines will be 
    replaced by the value given with the macro argument substituted for ``%``.
    For example, the entry

    .. code-block: cmake

        MYDIR(%)=<dir/%>
        
    will convert lines of the form

    .. code-block: cmake

        #include MYDIR(myheader.h)

    to

    .. code-block: cmake

        #include <mydir/header.h>

    allowing the dependency to be followed.
    """

    include_directories: List[FileName]
    """
    List of preprocessor include file search directories.
    This property specifies the list of directories given so far to 
    the ``include_directories()`` command.

    This property is used to populate the `INCLUDE_DIRECTORIES` target 
    property, which is used by the generators to set the include
    directories for the compiler.

    Contents of `INCLUDE_DIRECTORIES` may use "generator expressions" 
    with the syntax ``$<...>``. 
    """

    include_regular_expression: str
    """
    This property specifies the regular expression used during dependency 
    scanning to match include files that should be followed.
    """

    interprocedural_optimization: bool
    """
    Enable interprocedural optimization for targets in a directory.
    """

    labels: List[str]
    """
    Specify a list of text labels associated with a directory and all of its
    subdirectories. This is equivalent to setting the LABELS target property
    and the LABELS test property on all targets and tests in the current
    directory and subdirectories. Note: Launchers must enabled to propagate 
    labels to targets.
    """

    link_directories: list[Path]
    """
    This property holds a semicolon-separated list of directories and is 
    typically populated using the link_directories() command. It gets its 
    initial value from its parent directory, if it has one.

    The directory property is used to initialize the `LINK_DIRECTORIES` target
    property when a target is created. That target property is used by
    the generators to set the library search directories for the linker.

    Contents of `LINK_DIRECTORIES` may use "generator expressions" with 
    the syntax ``$<...>``.
    """

    link_options: list[str]
    """
    List of options to use for the link step of shared library, module and 
    executable targets as well as the device link step.

    This property holds a semicolon-separated list of options given so far
    to the ``add_link_options()`` command.

    This property is used to initialize the LINK_OPTIONS target property when 
    a target is created, which is used by the generators to set the options
    for the compiler.

    Contents of `LINK_OPTIONS` may use "generator expressions" with 
    the syntax ``$<...>``. 
    """

    listfile_stack: list[str]
    """
    The current stack of listfiles being processed.
    """

    _macros: list[str] = Field(read_only=True, alias="MACROS")

    rule_launch_compile: str
    """
    Specify a launcher for compile rules.
    """

    rule_launch_custom: str
    """
    Specify a launcher for custom rules.
    """

    rule_launch_link: str
    """
    Specify a launcher for link rules.
    """

    source_dir: Path = Field(read_only=True, validators=[validate_path])
    """
    This read-only directory property reports absolute path to the source
    directory on which it is read.
    """

    test_include_files: list[str]
    """
    A list of cmake files that will be included when ctest is run.
    """

    _variables: list[str]

    vs_startup_project: str
    """
    Specify the default startup project in a Visual Studio solution.
    """

    parent: "Directory" = Field(read_only=True)
    _subdirectories: list["Directory"] = []
    _buildsystem_targets: list[str] = []
    _cache_variables: list[str] = []
    _imported_targets: list[str] = []
    tests_dict: TestDict = Field(read_only=True, default=TestDict())
    _sources: SourceDict = Field(read_only=True, default=SourceDict(Path()))

    def __init__(
        self, source_dir: Path, binary_dir: Path, parent: Optional["Directory"] = None
    ):
        super(Directory, self).__init__(
            source_dir=source_dir,
            binary_dir=binary_dir,
            parent=parent,
            _sources=SourceDict(source_dir),
        )

        if self.parent:
            self.compile_definitions = self.parent.compile_definitions
            self.implicit_depends_include_transform = (
                parent.implicit_depends_include_transform
            )
            self.parent._subdirectories.append(self)

    def add_test(self, name: str, **properties):
        res = Test(source_dir=self.source_dir, name=name, **properties)
        self.tests_dict[name] = res
        return res

    def set_source_property(
        self,
        source_file: FileName,
        name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        # since source file names support generator expressions,
        # CMake implements matching file names as strings
        # (at the configuration phase)
        if source_file in self._sources:
            source = self._sources[source_file]
        else:
            source = SourceFile(source_file)
            self._sources[source_file] = source

        if name != "LOCATION":
            source.set_property(name, value, update_type)

    def get_source_file(self, location: FileName):
        return self._sources[location]

    def __str__(self):
        return str(self.source_dir)
