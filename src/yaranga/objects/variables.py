from yaranga.commands.get import GetCacheVariable
from yaranga.objects.cache_variable import EmptyCacheVariable
from yaranga.objects.cmakeobject import CMakeObject


class Variables(CMakeObject):
    def mark_as_dirty(self):
        for var in list(self.keys()):
            if var not in ["CMAKE_SOURCE_DIR", "CMAKE_CURRENT_SOURCE_DIR"]:
                del self[var]


class CacheVariables(CMakeObject):
    def mark_as_dirty(self):
        for var in list(self.keys()):
            if var not in ("CMAKE_COMMAND", "CMAKE_BUILD_TYPE"):
                del self[var]

    def on_dirty_read(self, key: str, context):
        context.add_command(GetCacheVariable(key))
        context.flush()
        if key not in self:
            self[key] = EmptyCacheVariable

        return self[key]
