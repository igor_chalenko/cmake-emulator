from enum import Enum
from typing import Optional

from yaranga.model.field import Field
from yaranga.types import Value
from yaranga.objects.cmakeobject import CMakeObject


class CacheVariableType(Enum):
    """
    Widget type for entry in GUIs.
    Cache entry values are always strings, but CMake GUIs present widgets
    to help users set values. The GUIs use this property as a hint
    to determine the widget type. Valid TYPE values are:

    .. glossary:

       BOOL
         Boolean ON/OFF value.

       PATH
         A path to a directory.

       FILEPATH
         Path to a file.

       STRING
         Generic string value.

       INTERNAL
         Do not present in GUI at all.

       STATIC
         Value managed by CMake, do not change.

       UNINITIALIZED
         Type not yet specified.
    """

    UNINITIALIZED = "UNINITIALIZED"
    BOOL = "BOOL"
    FILEPATH = "FILEPATH"
    PATH = "PATH"
    STATIC = "STATIC"
    STRING = "STRING"
    INTERNAL = "INTERNAL"

    def __str__(self):
        return self.value


class CacheVariable(CMakeObject):
    """
    A single cache entry.
    """

    advanced: bool
    """
    ``True`` if entry should be hidden by default in GUIs.
    This is a boolean value indicating whether the entry is considered 
    interesting only for advanced configuration. The ``mark_as_advanced()`` 
    command modifies this property.
    """

    name: str = Field(read_only=True, nullable=False)

    helpstring: str
    """
    Help associated with entry in GUIs.
    This string summarizes the purpose of an entry to help users set it through
    a CMake GUI.
    """

    value: Value
    """
    Value of a cache entry.
    This property maps to the actual value of a cache entry. Setting this 
    property always sets the value without checking, so use with care.
    """

    type: CacheVariableType
    """
    Widget type for entry in GUIs.
    
    .. seealso: CacheVariableType
    """

    strings: Optional[list[str]]
    """
    Enumerate possible STRING entry values for GUI selection.
    For cache entries with type STRING, this enumerates a set of values. 
    ``CMake`` GUIs may use this to provide a selection widget instead of 
    a generic string entry field. This is for convenience only. ``CMake``
    does not enforce that the value matches one of those listed.
    """

    def __init__(
        self, name: str, value: Value, typ: CacheVariableType, helpstring: str, **kwargs
    ):
        super(CacheVariable, self).__init__(
            name=name, value=value, type=typ, helpstring=helpstring, **kwargs
        )


class CacheVariableStub(CacheVariable):
    def __getitem__(self, _):
        return None

    def __contains__(self, _):
        return True

    def __delitem__(self, _):
        pass


EmptyCacheVariable = CacheVariableStub("", None, CacheVariableType.UNINITIALIZED, "")
