from pathlib import Path
from typing import Optional
from collections import OrderedDict

from yaranga.model.exceptions import ValidationError
from yaranga.model.field import Field
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.types import Value, FileName


def validate_skip_return_code(_: object, new_value: Value) -> None:
    int_value = int(new_value)
    if int_value < 0 or int_value > 255:
        raise ValidationError(f"SKIP_RETURN_CODE must be in the range of (0..255)")


class Test(CMakeObject):
    """
    Test objects are created by ``StateManager.add_test``.
    """

    __test__ = False

    attached_files: list[Path]
    """
    Attach a list of files to a dashboard submission.
    Set this property to a list of files that will be encoded and submitted
    to the dashboard as an addition to the test result.
    """

    attached_files_on_fail: list[Path]
    """
    Attach a list of files to a dashboard submission if the test fails.
    Same as ``ATTACHED_FILES``, but these files will only be included if 
    the test does not pass.
    """

    name: str = Field(read_only=True, nullable=False)
    source_dir: FileName = Field(read_only=True, nullable=False)
    command: str
    command_expand_lists: bool
    configurations: list[str]

    cost: float
    """
    This property describes the cost of a test. When parallel testing is 
    enabled, tests in the test set will be run in descending order of cost.
    Projects can explicitly define the cost of a test by setting this 
    property to a floating point value.
    """

    depends: list[str]
    """
    Specifies that this test should only be run after the specified list of 
    tests. Set this to a list of tests that must finish before this test is 
    run. The results of those tests are not considered, the dependency 
    relationship is purely for order of execution (i.e. it is really just a
    run after relationship). Consider using test fixtures with setup tests 
    if a dependency with successful completion is required (see 
    ``FIXTURES_REQUIRED``).
    
    Examples:
    
    .. code-block: cmake
    
        add_test(NAME baseTest1 ...)
        add_test(NAME baseTest2 ...)
        add_test(NAME dependsTest12 ...)
        
        set_tests_properties(dependsTest12 PROPERTIES DEPENDS "baseTest1;baseTest2")
        # dependsTest12 runs after baseTest1 and baseTest2, even if they fail
    
    """

    disabled: bool
    """
    If set to ``True``, the test will be skipped and its status will be 
    ``Not Run``. A ``DISABLED`` test will not be counted in the total number
    of tests and its completion status will be reported to `CDash` as 
    ``Disabled``.

    A ``DISABLED`` test does not participate in test fixture dependency 
    resolution. If a ``DISABLED`` test has fixture requirements defined in its
    ``FIXTURES_REQUIRED`` property, it will not cause setup or cleanup tests 
    for those fixtures to be added to the test set.

    If a test with the ``FIXTURES_SETUP`` property set is ``DISABLED``, 
    the fixture behavior will be as though that setup test was passing and any
    test case requiring that fixture will still run.
    """

    environment: OrderedDict[str, str]
    """
    Specify environment variables that should be defined for running a test.
    Set to a semicolon-separated list list of environment variables and values 
    of the form ``VAR=value``. Those environment variables will be defined 
    while running the test. The environment changes from this property do not 
    affect other tests.
    """

    environment_modification: list[str]
    """
    Specify environment variables that should be modified for running a test. 
    Note that the operations performed by this property are performed after
    the ``ENVIRONMENT`` property is already applied.

    Set to a semicolon-separated list of environment variables and values of
    the form ``VAR=OP:VALUE``, where ``VAR`` is the case-sensitive name of 
    an environment variable to be modified. Entries are considered in the order
    specified in the property's value. The ``OP`` may be one of:

    .. glossary::    
    
       reset
           Reset to the unmodified value, ignoring all modifications to 
           ``VAR`` prior to this entry. Note that this will reset the variable 
           to the value set by ``ENVIRONMENT``, if it was set, and otherwise to
           its state from the rest of the ``CTest`` execution.

        set (str)
            Replaces the current value of ``VAR`` with ``VALUE``.

        unset
            Unsets the current value of ``VAR``.

        string_append
            Appends VALUE to the current value of ``VAR``.

        string_prepend
            Prepends VALUE to the current value of ``VAR``.

        path_list_append
            Appends VALUE to the current value of ``VAR`` using the host 
            platform's path list separator (``;`` on Windows and ``:`` 
            elsewhere).

        path_list_prepend
            Prepends VALUE to the current value of ``VAR`` using the host
            platform's path list separator (``;`` on Windows and ``:`` 
            elsewhere).

        cmake_list_append
            Appends VALUE to the current value of ``VAR`` using ``;`` as 
            the separator.

        cmake_list_prepend
            Prepends VALUE to the current value of ``VAR`` using ``;`` as
            the separator.

    Unrecognized ``OP`` values will result in the test failing before it is 
    executed. This is so that future operations may be added without changing
    valid behavior of existing tests.

    The environment changes from this property do not affect other tests.
    """

    labels: list[str]
    """
    Specify a list of text labels associated with a test. The labels are 
    reported in both the ctest output summary and in dashboard submissions.
    They can also be used to filter the set of tests to be executed 
    (see the ``ctest -L`` and ``ctest -LE`` options).
    """

    measurement: Optional[str]
    """
    Specify a ``CDASH`` measurement and value to be reported for a test.

    If set to a name then that name will be reported to ``CDASH`` as a named 
    measurement with a value of 1. You may also specify a value by setting 
    ``MEASUREMENT`` to ``measurement=value``.
    """

    pass_regular_expression: list[str]
    """
    The output must match this regular expression for the test to pass. 
    The process exit code is ignored. If set, the test output will be checked
    against the specified regular expressions and at least one of the regular
    expressions has to match, otherwise the test will fail. Example:

    .. code-block: cmake

        set_tests_properties(mytest PROPERTIES
          PASS_REGULAR_EXPRESSION "TestPassed;All ok"
        )

    ``PASS_REGULAR_EXPRESSION`` expects a list of regular expressions.
    """

    processor_affinity: bool
    """
    Set to a true value to ask CTest to launch the test process with CPU 
    affinity for a fixed set of processors. If enabled and supported for
    the current platform, CTest will choose a set of processors to place
    in the CPU affinity mask when launching the test process. The number 
    of processors in the set is determined by the PROCESSORS test property 
    or the number of processors available to CTest, whichever is smaller. 
    The set of processors chosen will be disjoint from the processors assigned 
    to other concurrently running tests that also have 
    the ``PROCESSOR_AFFINITY`` property enabled.
    """

    processors: int
    """
    Set to specify how many process slots this test requires. If not set, 
    the default is 1 processor. Denotes the number of processors that this test
    will require. This is typically used for MPI tests, and should be used in
    conjunction with the ``ctest_test()`` `PARALLEL_LEVEL` option.

    This will also be used to display a weighted test timing result in label 
    and subproject summaries in the command line output of ``ctest(1)``. 
    The wall clock time for the test run will be multiplied by this property
    to give a better idea of how much cpu resource ``CTest`` allocated for 
    the test.
    """

    required_files: list[str]
    """
    List of files required to run the test. The filenames are relative to 
    the test WORKING_DIRECTORY unless an absolute path is specified.
    If set to a list of files, the test will not be run unless all of the files
    exist.
    """

    resource_groups: list[str]
    """
    Specify resources required by a test, grouped in a way that is meaningful 
    to the test. See resource allocation for more information on how this 
    property integrates into the CTest resource allocation feature.
    The `RESOURCE_GROUPS` property is a semicolon-separated list of group 
    descriptions. Each entry consists of an optional number of groups using 
    the description followed by a series of resource requirements for those 
    groups. These requirements (and the number of groups) are separated by 
    commas. The resource requirements consist of the name of a resource type,
    followed by a colon, followed by an unsigned integer specifying the number
    of slots required on one resource of the given type.
    """

    resource_lock: list[str]
    """
    Specify a list of resources that are locked by this test.
    If multiple tests specify the same resource lock, they are guaranteed not 
    to run concurrently. See also ``FIXTURES_REQUIRED`` if the resource 
    requires any setup or cleanup steps.

    Both the ``RESOURCE_GROUPS`` and ``RESOURCE_LOCK`` properties serve similar 
    purposes, but they are distinct and orthogonal. Resources specified by 
    ``RESOURCE_GROUPS`` do not affect ``RESOURCE_LOCK``, and vice versa. 
    Whereas ``RESOURCE_LOCK`` is a simpler property that is used for locking 
    one global resource, ``RESOURCE_GROUPS`` is a more advanced property that 
    allows multiple tests to simultaneously use multiple resources of the same 
    type, specifying their requirements in a fine-grained manner.
    """

    run_serial: bool
    """
    Do not run this test in parallel with any other test.
    Use this option in conjunction with the ``ctest_test`` `PARALLEL_LEVEL` 
    option to specify that this test should not be run in parallel with any 
    other tests.
    """

    skip_regular_expression: list[str]
    """
    If the output matches this regular expression the test will be marked as 
    skipped. If set, if the output matches one of specified regular 
    expressions, the test will be marked as skipped. 
    
    Example:

    .. code-block: cmake
    
        set_property(TEST mytest 
                     PROPERTY
                     SKIP_REGULAR_EXPRESSION "[^a-z]Skip" "SKIP" "Skipped")
                     
    `SKIP_REGULAR_EXPRESSION` expects a list of regular expressions.
    """

    skip_return_code: int = Field(validators=[validate_skip_return_code])
    """
    Return code to mark a test as skipped.
    Sometimes only a test itself can determine if all requirements for the test
    are met. If such a situation should not be considered a hard failure
    a return code of the process can be specified that will mark the test as 
    ``Not Run`` if it is encountered. Valid values are in the range of ``0`` to
    ``255``, inclusive.
    """

    timeout: int
    """
    How many seconds to allow for this test.

    This property if set will limit a test to not take more than the specified 
    number of seconds to run. If it exceeds that the test process will be 
    killed and ctest will move to the next test. This setting takes precedence
    over `CTEST_TEST_TIMEOUT`.
    """

    timeout_after_match: int
    """
    Allow a test seconds to complete after regex is encountered in its output.
    When the test outputs a line that matches regex its start time is reset to
    the current time and its timeout duration is changed to seconds. Prior to
    this, the timeout duration is determined by the `TIMEOUT` property or 
    the `CTEST_TEST_TIMEOUT` variable if either of these are set. Because 
    the test's start time is reset, its execution time will not include any
    time that was spent waiting for the matching output.

    `TIMEOUT_AFTER_MATCH` is useful for avoiding spurious timeouts when your 
    test must wait for some system resource to become available before it can
    execute. Set `TIMEOUT` to a longer duration that accounts for resource 
    acquisition and use `TIMEOUT_AFTER_MATCH` to control how long the actual
    test is allowed to run.

    If the required resource can be controlled by `CTest` you should use 
    `RESOURCE_LOCK` instead of `TIMEOUT_AFTER_MATCH`. This property should be
    used when only the test itself can determine when its required resources
    are available.
    """

    will_fail: bool
    """
    If set to ``True``, this will invert the pass/fail flag of the test.
    This property can be used for tests that are expected to fail and return 
    a non zero return code.
    """

    working_directory: Optional[Path]
    """
    The directory from which the test executable will be called.
    If this is not set, the test will be run with the working directory set to
    the binary directory associated with where the test was created (i.e. 
    the `CMAKE_CURRENT_BINARY_DIR` for where ``add_test()`` was called).
    """

    def __init__(self, source_dir: FileName, name: str, **kwargs):
        super(Test, self).__init__(name=name, source_dir=source_dir, **kwargs)

    def __eq__(self, other):
        return self.name == other.name and str(self.source_dir) == str(other.source_dir)
