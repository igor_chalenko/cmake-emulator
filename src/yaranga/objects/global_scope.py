from enum import Enum

from yaranga.model.field import Field
from yaranga.objects.cmakeobject import CMakeObject


class CMakeRole(Enum):
    PROJECT = "PROJECT"
    """
    Running in project mode(processing a CMakeLists.txt file).
    """

    SCRIPT = "SCRIPT"
    """
    Running in -P script mode.
    """

    FIND_PACKAGE = "FIND_PACKAGE"
    """
    Running in --find - package mode.
    """

    CTEST = "CTEST"
    """
    Running in CTest script mode.
    """

    CPACK = "CPACK"
    """
    Running in CPack.
    """


class GlobalScope(CMakeObject):
    """
    The class definition does not include obsolete ``CMake`` properties.
    """

    allow_duplicate_custom_targets: bool
    """
    Allow duplicate custom targets to be created.
    """

    autogen_source_group: str
    """
    Name of the source_group() for ``AUTOMOC``, ``AUTORCC`` and ``AUTOUIC``
    generated files.
    """

    autogen_targets_folder: str
    """
    Name of `FOLDER` for ``*_autogen`` targets that are added automatically 
    by CMake for targets for which `AUTOMOC` is enabled. If not set, 
    ``CMake`` uses the FOLDER property of the parent target as a default 
    value for this property. See also the documentation for the `FOLDER`
    target property and the `AUTOMOC` target property.
    """

    automoc_source_group: str
    """
    Name of the source_group() for `AUTOMOC` generated files.
    """

    autorcc_source_group: str
    """
    Name of the source_group() for `AUTORCC` generated files.
    """

    autouic_source_group: str
    """
    Name of the source_group() for `AUTOUIC` generated files.
    """

    cmake_c_known_features: list[str]
    """
    List of C features known to this version of ``CMake``.
    """

    cmake_cxx_known_features: list[str]
    """
    List of C++ features known to this version of ``CMake``.
    """

    cmake_cuda_known_features: list[str]
    """
    List of CUDA features known to this version of CMake.
    """

    cmake_role: CMakeRole
    """
    Tells what mode the current running script is in.
    """

    debug_configurations: list[str]
    """
    The value must be a semi-colon separated list of configuration names. 
    This property must be set at the top level of the project and before 
    the first ``target_link_libraries()`` commands invocation. If any entry in
    the list does not match a valid configuration for the project the behavior 
    is undefined.
    """

    disabled_features: list[str]
    """
    List of features which are disabled during the ``CMake`` run.
    """

    eclipse_extra_cproject_contents: str
    """
    Additional contents to be inserted into the generated `Eclipse` cproject 
    file. The cproject file defines the CDT specific information. Some third 
    party IDEs are based on Eclipse with the addition of other information
    specific to that IDE. Through this property, it is possible to add this
    additional contents to the generated project. It is expected to contain 
    valid XML.
    """

    eclipse_extra_natures: list[str]
    """
    List of natures to add to the generated Eclipse project file.
    Eclipse projects specify language plugins by using natures. This property 
    should be set to the unique identifier for a nature (which looks like
    a Java package name).
    """

    enabled_features: list[str]
    """
    List of features which are enabled during the ``CMake`` run.
    """

    enabled_languages: list[str]
    """
    Read-only property that contains the list of currently enabled 
    languages.
    """

    find_library_use_lib32_paths: bool
    """
    ``FIND_LIBRARY_USE_LIB32_PATHS`` is a boolean specifying whether 
    the ``find_library()`` command should automatically search the ``lib32`` 
    variant of directories called lib in the search path when building 
    32-bit binaries.
    """

    find_library_use_lib64_paths: bool
    """
    ``FIND_LIBRARY_USE_LIB64_PATHS`` is a boolean specifying whether 
    the ``find_library()`` command should automatically search the ``lib64`` 
    variant of directories called lib in the search path when building 
    64-bit binaries.
    """

    generator_is_multi_config: bool
    """
    Read-only property that is ``true`` on multi-configuration generators.
    """

    global_depends_debug_mode: bool
    """
    Enable global target dependency graph debug mode.
    """

    global_depends_no_cycles: bool
    """
    Disallow global target dependency graph cycles.
    """

    in_try_compile: bool
    """
    Read-only property that is true during a try-compile configuration.
    """

    job_pools: dict[str, int]
    """
    Ninja only: List of available pools.
    A pool is a named integer property and defines the maximum number of 
    concurrent jobs which can be started by a rule assigned to the pool. 
    The `JOB_POOLS` property is a semicolon-separated list of pairs using
    the syntax ``NAME=integer`` (without a space after the equality sign).

    For instance:

    .. code-block: cmake

        set_property(GLOBAL PROPERTY JOB_POOLS two_jobs=2 ten_jobs=10)
    
    """

    packages_found: list[str] = Field(read_only=True)
    """
    List of packages which were found during the CMake run. Whether a package 
    has been found is determined using the ``<NAME>_FOUND`` variables.
    """

    predefined_targets_folder: str
    """
    Name of FOLDER for targets that are added automatically by CMake.
    If not set, CMake uses "CMakePredefinedTargets" as a default value for this 
    property. Targets such as ``INSTALL``, ``PACKAGE`` and ``RUN_TESTS`` will 
    be organized into this FOLDER. See also the documentation for 
    the ``FOLDER`` target property.
    """

    report_undefined_properties: str
    """
    If set, report any undefined properties to this file.
    If this property is set to a filename then when ``CMake`` runs it will 
    report any properties or variables that were accessed but not defined 
    into the filename specified in this property.
    """

    rule_launch_compile: str
    """
    Specify a launcher for compile rules.
    """

    rule_launch_custom: str
    """
    Specify a launcher for custom rules.
    """

    rule_launch_link: str
    """
    Specify a launcher for link rules.
    """

    rule_messages: bool
    """
    Specify whether to report a message for each make rule.
    """

    target_archives_may_be_shared_libs: bool = Field(read_only=True)
    """
    Set if shared libraries may be named like archives.
    """

    target_messages: bool
    """
    Specify whether to report the completion of each target.
    This property specifies whether Makefile Generators should add a progress
    message describing that each target has been completed. If the property is
    not set the default is ``ON``. Set the property to ``OFF`` to disable
    target completion messages.
    """

    target_supports_shared_libs: bool = Field(read_only=True)
    """
    TARGET_SUPPORTS_SHARED_LIBS is a boolean specifying whether the target 
    platform supports shared libraries. Basically all current general general 
    purpose OS do so, the exception are usually embedded systems with no or
    special OSs.
    """

    use_folders: bool
    """
    Use the `FOLDER` target property to organize targets into folders.
    If not set, ``CMake`` treats this property as OFF by default. ``CMake`` 
    generators that are capable of organizing into a hierarchy of folders 
    use the values of the ``FOLDER`` target property to name those folders. 
    """

    xcode_emit_effective_platform_name: bool
    """
    Control emission of ``EFFECTIVE_PLATFORM_NAME`` by the Xcode generator.
    It is required for building the same target with multiple SDKs. A common 
    use case is the parallel use of ``iphoneos`` and ``iphonesimulator`` SDKs.
    Three different states possible that control when the Xcode generator emits
    the `EFFECTIVE_PLATFORM_NAME` variable:

    * If set to ON it will always be emitted

    * If set to OFF it will never be emitted

    * If unset (the default) it will only be emitted when the project was configured 
      for an embedded Xcode SDK like ``iOS``, ``tvOS``, ``watchOS`` or any of 
      the simulators.
      
    """
