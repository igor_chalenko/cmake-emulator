from pathlib import Path

from yaranga.commands.get import GetDirectory
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.types import FileName


class DirectoryDict(CMakeObject):
    def __len__(self):
        return len(self.properties.values())

    def on_dirty_read(self, key: FileName, context):
        key = Path(key)
        context.add_command(GetDirectory(key))
        context.flush()
        if key not in self:
            raise ValueError(
                f"get_property DIRECTORY scope provided but "
                f"requested directory {key} was notfound.  This could be "
                f"because the directory argument was invalid or, it is "
                "valid but has not been processed yet."
            )
        return self[key]
