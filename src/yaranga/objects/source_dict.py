from pathlib import Path

from yaranga.commands.get import GetSourceFile
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.objects.source_file import EmptySourceFile


class SourceDict(CMakeObject):
    directory: Path

    def __init__(self, directory: Path):
        super(SourceDict, self).__init__(directory=directory)

    def on_dirty_read(self, key: str, context):
        context.add_command(GetSourceFile(self.directory, key))
        context.flush()
        if key not in self:
            self[key] = EmptySourceFile
        return self[key]

    def __len__(self):
        return len(self.properties.values())
