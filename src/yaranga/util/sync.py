import logging
from threading import Lock


logger = logging.getLogger(__name__)


class Lockable(object):
    LOCK_NAME = "_lock"

    def __new__(cls, *args, **kwargs):
        obj = super(Lockable, cls).__new__(cls)
        setattr(obj, Lockable.LOCK_NAME, Lock())
        return obj


def synchronized(method):
    def synced_method(self: Lockable, *args, **kws):
        lock: Lock = getattr(self, Lockable.LOCK_NAME)
        with lock:
            logger.debug(f"lock {str(method)}...")
            return method(self, *args, **kws)

    res = synced_method
    return res
