from os import PathLike

import yaml


def yaml_dict(config_file_name: PathLike):
    with open(config_file_name, "r") as stream:
        return yaml.safe_load(stream)
