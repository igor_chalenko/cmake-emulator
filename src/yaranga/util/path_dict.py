from pathlib import Path

from yaranga.types import FileName


class PathDict(dict):
    def __init__(self, working_dir: Path, strict=False):
        super().__init__()
        self.working_dir = working_dir
        self.strict = strict

    def __setitem__(self, key: FileName, value):
        if isinstance(key, str):
            key = Path(key)
        if not key.is_absolute():
            key = Path(self.working_dir, key)
        key = key.resolve(strict=self.strict)
        super(PathDict, self).__setitem__(key, value)

    def __getitem__(self, key: FileName):
        if isinstance(key, str):
            key = Path(key)
        if not key.is_absolute():
            key = Path(self.working_dir, key)
        key = key.resolve(strict=self.strict)
        return super(PathDict, self).__getitem__(key)
