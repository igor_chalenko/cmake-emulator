from typing import Optional, Any

from yaranga.constants import FIELDS
from yaranga.model.model import ModelMetaclass
from yaranga.model.modelfield import ModelField
from yaranga.model.objectmodel import ObjectModel
from yaranga.undefined import Undefined


class BaseModel(metaclass=ModelMetaclass):
    """
    Parent for all the classes that use the field object model.
    Adds generic initialization from ``kwargs`` and a few convenience
    methods for working with fields and attributes.

    .. seealso ModelMetaclass

    .. note:
    Because string values are transformed from their ``CMake`` form,
    the Python implementation may break while the ``CMake``-based one
    works fine. For example, an empty string may be converted to the boolean
    value of ``False``, Later the Python code may attempt a comparison such as
    this:

    ```Python
    some_value: str = ''
    ...
    if str(obj.bool_property) == some_value:
      ...
    ```

    The same code works in ``CMake``, but doesn't work in Python, because
    the original string value is lost. And without ``str``, a ``TypeError``
    will be raised (an attempt is made to compare a boolean and a string).
    """

    def __init__(self, **kwargs):
        """
        Initializes the attributes specified by ``kwargs``,
        and sets default values to all the remaining attributes,
        as specified by the respective field annotations.

        :param kwargs: input values; keywords must match the self's
        class attributes
        """
        object_model: ObjectModel = getattr(self, FIELDS)
        # store input kwargs into respective attributes/properties
        # and set default values for the remaining attributes
        object_model.initialize(self, **kwargs)

    def get_or_undefined(self, prop_name: str) -> Any:
        """
        Shorthand for ``self.__dict__.get(prop_name, Undefined)``.
        """
        return self.__dict__.get(prop_name, Undefined)

    def attr_from_property(self, property_name: str) -> str:
        """
        Searches for the field with the given alias.
        :param property_name: the alias of a field that must be found
        :return: the field with the given alias or ``None`` if one doesn't
        exist
        """
        field = self.get_field(property_name, by_alias=True)
        return field.name if field else None

    def field_alias_from_name(self, name: str):
        """
        Searches for the alias of a field with the given name.
        :param name: the nane of a field that must be found
        :return: the alias of the found field or ``name`` if the alias
        is empty. Note the asymmetry with :meth:`field_name_from_alias`.
        """
        field = self.get_field(name, by_alias=False)
        return field.alias or name

    @classmethod
    def get_field(cls, name: str, by_alias: bool = False) -> Optional[ModelField]:
        if by_alias:
            return cls.fields.get_by_alias(name)
        else:
            return cls.fields.get(name, None)

    @classmethod
    def get_fields(cls) -> ObjectModel:
        return cls.fields
