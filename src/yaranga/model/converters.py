import collections
import logging
import re
from enum import EnumMeta
from pathlib import Path
from typing import Type, List, Union, Any, Optional, Dict, Callable

from yaranga.model.typeinfo import TypeInfo

NOT_FOUND = re.compile("(.+)_NOTFOUND")


def _is_not_found(value: str) -> bool:
    not_found = re.match(NOT_FOUND, value)
    return not_found or "NOTFOUND" == value


def _is_convertible_to_bool(value: Any) -> bool:
    return isinstance(value, bool) or "__bool__" in value.__class__.__dict__


def _to_boolean(value: object, _):
    """
    * The `CMake` constants for ``True`` are ``ON``, ``YES``, ``TRUE``, ``Y``,
      or any non-zero number;
    * The `CMake` constants for ``False`` are ``0``, ``OFF``, ``NO``,
      ``FALSE``, ``N``, ``IGNORE``. Note that this does not include special
      ``NOTFOUND`` constants - see below;
    * The `CMake` constants for ``None`` are ``NOTFOUND``, the empty string,
      or a string that ends in the suffix ``-NOTFOUND``. The need for
      special ``NOTFOUND`` constants is eliminated by the existence of `None`;
      in boolean comparisons, it evaluates to ``False``, which is what we
      need.

    :param value: a string value obtained from CMake
    :return: a boolean value that matches the evaluation of the `CMake`
    expression `if (<value>)`
    """
    if _is_convertible_to_bool(value):
        return bool(value)

    str_value = str(value)
    value_lower = str_value.lower()

    if _is_not_found(str_value):
        return None
    elif value_lower in ["on", "yes", "true", "y"]:
        return True
    elif value_lower in ["off", "no", "false", "n", "ignore"]:
        return False

    try:
        int_value = int(str_value)
        return int_value > 0
    except ValueError:
        # after the above checks casting to bool is the same as in CMake
        return bool(value)


def _to_int(value: object, _):
    """
    The input object is first converted to string, if needed, and then
    cast to an integer via the built-in function ``int()``.

    :param value: the input value
    :return: converted input value
    """
    if isinstance(value, int):
        return value
    str_value = str(value)
    if _is_not_found(str_value):
        return None
    return int(str_value)


def _to_float(value: object, _):
    """
    The input object is first converted to string, if needed, and then
    cast to a float via the built-in function ``float()``.

    :param value: the input value
    :return: converted input value
    """
    if isinstance(value, float):
        return value
    str_value = str(value)
    if _is_not_found(str_value):
        return None
    return float(str_value)


def _to_string(value: object, _):
    """
    If the input value is ``None`` or convertible to it, returns ``None``.
    Otherwise, returns ``str(value)``.

    :param value: the input value
    :return: converted input value
    """
    if value is not None:
        str_value = str(value)
        if not _is_not_found(str_value):
            return str_value
    return None


def _to_path(value: object, _):
    """
    If the input value is ``None`` or convertible to it, returns ``None``.
    Otherwise, returns ``Path(value)``.

    :param value: the input value
    :return: converted input value
    """
    if value is not None:
        str_value = str(value)
        if not _is_not_found(str_value):
            return Path(str_value)
    return None


def _to_enum(value: object, target_type: EnumMeta):
    """
    If the input value is ``None`` or convertible to it, returns ``None``.
    Otherwise, returns ``str(value)``.

    :param value: the input value
    :return: converted input value
    """
    if value is not None and not _is_not_found(str(value)):
        if isinstance(value, target_type):
            return value

        return target_type[str(value)]
    return None


def _convert_element(value, target_type: Type) -> Optional[type]:
    """
    Converts an input value to the type as specified by the given type.
    Because an input value may be converted to ``None``,
    the exact return type is ``Optional[target_type]``.

    :param value: an input value
    :param target_type: the target type
    :return: converted value

    Example:

    .. literalinclude ../../../examples/model/convert_element.py

    """

    for _type, _callable in converters.items():
        # the reason we don't use issubclass(target_type, Enum) is
        # that it fails when given `ForwardRef`, `Union`, and others
        if target_type is _type or isinstance(target_type, _type):
            return _callable(value, target_type)

    return value


def _convert_list(value, field_type: Type) -> list[Optional[type]]:
    """
    Converts an input value to the list where each element has a type specified
    by the given type. Because some values in the input list may be converted
    to ``None``, the exact return type is ``list[Optional[target_type]]``.

    :param value: an input value; it's expected to be a list or have the format
    of CMake list objects (a sequence of semicolon-separated values)
    :param field_type: the type of the resulting list's elements
    :return: converted value

    Example:

    .. literalinclude ../../../examples/model/convert_list.py

    """

    if value is None:
        return []

    if not isinstance(value, List):
        if isinstance(value, str):
            value = value.split(";") if value else []
        else:
            value = [value]
    res: list = []
    for element in value:
        res.append(_convert_element(element, field_type))
    return res


def _convert_dict(value, field_types: list[TypeInfo]) -> dict[str, Any]:
    """
    Converts an input value to the dict where each element has a type specified
    by ``field_types``. Because some values in the input list may be converted
    to ``None``, the exact return type is ``dict[str, Optional[target_type]]``.

    :param value: an input value; it's expected to be a dict or have the format
    of ``VAR1=VALUE1 VAR2=VALUE2 ...``:

    .. code-block:

        two_jobs=2 ten_jobs=10

    :param field_types: the type of the resulting dictionary values
    :return: converted value

    Example:

    .. literalinclude ../../../examples/model/convert_list.py

    """
    res = {}
    if not isinstance(value, (Dict, dict)):
        if value:
            tokens = str(value).split(";")
            for token in tokens:
                values = token.strip().split("=")
                res[values[0]] = _convert_element(
                    values[1].strip(), field_types[1].type
                )
    else:
        res = value
    return res


converters: dict[type, Callable] = {
    bool: _to_boolean,
    int: _to_int,
    float: _to_float,
    str: _to_string,
    Path: _to_path,
    EnumMeta: _to_enum,
}


def convert(value: Union[object, list], type_info: TypeInfo):
    """
    Converts the input value to an object whose type is identified by the given
    type information object. Delegates the actual work to :meth:`_convert_list`
    in the case of the list type info, or :meth:`_convert_element` for
    single-element type info.

    :param value: an input value
    :param type_info: type information object; for the conversion to take place
     either ``type_info.type`` or `type_info.subtypes[0]`` must be one of
     ``str``, ``int``, ``float``, ``bool``, a ``typing.Enum`` subclass, or
     ``Path``
    :return: converted input value, ``type_info`` represents a supported type;
     original input value otherwise

    .. seealso TypeInfo
    """

    root_type = type_info.type

    if root_type is list or root_type is List:
        return _convert_list(value, type_info.subtypes[0].type)
    elif root_type is dict or root_type is collections.OrderedDict:
        return _convert_dict(value, type_info.subtypes)
    else:
        return _convert_element(value, root_type)


def register(_type: type, _callable: Callable):
    if _type in converters:
        logging.info(f"override the existing converter for {_type}")
    converters[_type] = _callable
