import collections
from typing import Optional, Type, get_origin, get_args, List, Dict, OrderedDict

from yaranga.model.reflection import is_optional, is_union


class TypeInfo(object):
    """
    Stores parsed information about a given, possibly generic, type. In
    the case of a non-generic type, the attribute ``type`` holds that type,
    and the value of ``subtypes`` is ``None``. In the case of a generic type,
    ``type`` stores the ``origin`` type, and ``subtypes`` store the generic's
    arguments, with a few exceptions:

      * If the origin type is ``typing.Optional``, it is discarded, and
        the first argument becomes the origin. We don't need optionality
        information, because every field is implicitly optional anyway.
      * If the origin type is not ``typing.Optional``, ``typing.Union``,
        ``typing.List`` nor ``list``, the input type is stored as is, unless
        it's a generic type, in which case ``TypeError`` is raised.

    Objects of this class drive data conversion within the field object model.

    """

    type: Type
    """
    One of the value types, a list, or ``typing.Union``  
    """
    subtypes: Optional[list["TypeInfo"]]
    """
    Generic type's arguments or ``None`` if ``type`` is not generic.  
    """

    def __init__(self, typ: Type):
        """
        Parses a given type into ``type`` and ``subtypes``.

        :param typ: input type, taken from an attribute's
        type annotation (and then stored in the corresponding
        instance of :class:`ModelField`)
        """

        def _create_type_info(field_type: Type):
            type_args = get_args(field_type)
            origin_type = get_origin(field_type)

            if is_optional(field_type):
                return _create_type_info(type_args[0])
            elif is_union(origin_type):
                return origin_type, [TypeInfo(x) for x in type_args]
            elif origin_type is list or origin_type is List:
                # we don't support nested lists, since CMake doesn't support them
                if type_args and get_origin(type_args[0]) is list:
                    raise TypeError("nested lists are not supported")
                ti = TypeInfo(type_args[0])
                return origin_type, [ti]
            elif (
                origin_type is dict
                or origin_type is Dict
                or origin_type is collections.OrderedDict
            ):
                # we don't support nested lists, since CMake doesn't support them
                if type_args and get_origin(type_args[0]) is dict:
                    raise TypeError("nested dicts are not supported")
                ti1 = TypeInfo(type_args[0])
                ti2 = TypeInfo(type_args[1])
                return origin_type, [ti1, ti2]
            else:
                return field_type, None

        self.type, self.subtypes = _create_type_info(typ)

    def __str__(self):
        """
        Creates string representation of the stored type and its subtypes.
        """
        res = getattr(self.type, "__name__", str(self.type))
        if self.subtypes:
            res += "["
            res += ", ".join(str(x) for x in self.subtypes)
            res += "]"
        return res
