import copy
import logging
import sys
from types import GenericAlias
from typing import (
    ForwardRef,
    Generator,
    get_origin,
    get_args,
    Optional,
    Any,
    Tuple,
    Type,
)

from frozendict import frozendict

from yaranga.constants import FIELDS
from yaranga.model.exceptions import NotWritableAttributeError
from yaranga.model.field import Field
from yaranga.model.modelfield import ModelField
from yaranga.model.reflection import is_class_var, make_forward_ref
from yaranga.undefined import Undefined

SKIP_CLASS = ("yaranga.model.model", "BaseModel")

logger = logging.getLogger(__name__)


class ObjectModel(object):
    """
    Field object model turns Python class attributes into `fields`.
    In order for this to be possible, the attribute must have a type
    annotation; everything else is optional. Since the attributes of ancestor
    classes are available to the descendants, there's a field counterpart for
    every inherited attribute as well. The newly added field semantics include:

      * declarative data validation and conversion;
      * data access control (reading/writing).

    As can be seen, the purpose of the module is very similar to that of
    `pydantic`_. If you are familiar with it, then much of the functionality
    here will make sense instantly. There are certain differences, however,
    that are dictated by a different problem domain.

      1. The ``__init__`` method must be handwritten for every class. This
         makes it easy to work with scope classes in IDE, such as `PyCharm`_,
         without extra plugins. Also, see below.
      2. The notion of a required field is different. Every field has a default
         value, either explicit or implicit one (``Undefined``). This means
         that required fields are effectively the ones that appear in the
         ``__init__`` method, and the rest will be initialized to a default
         value (and some may become undefined as a result). This behavior is
         dictated by the fact that the general assumption about mandatory
         fields being available during initialization does not hold.
         Indeed, there are properties that only ``CMake`` can provide a value
         for. If a ``CMake`` object is created on the Python side, such
         properties will remain undefined until the first read attempt. Upon
         a read of an undefined property, the value can be obtained from
         ``CMake`` (how this is done is not important for the moment), and
         the property updated even if it was marked as ``read-only``.
      3. Validation of a property's new value has access to the property's
         containing object. This makes it easy to compare old and new values,
         for one.

    Consider an example:

    .. code-block: python

    class Model(metaclass=ModelMetaclass):
      meaning: int = 42
      meaningful: bool = Field(nullable=False)

    In the example above, the class ``Model`` has two fields. The first one
    is created implicitly: ``Field(default=42)``. It means that if
    the attribute ``meaning`` is not given to the ``__init__`` method, it
    will be set to 42. The second one is explicit. It means that the value
    of ``meaningful`` cannot be set to ``None``. For a complete description
    of field semantics, refer to :class:`Field`.

    ``ObjectModel`` is created as an attribute in :class:`ModelMetaclass`,
    and therefore can be seen as an extension of Python class capabilities.

    .. _pydantic: https://pypi.org/project/pydantic/
    .. _PyCharm: https://www.jetbrains.com/ru-ru/pycharm/

    .. seealso Field
    """

    def __init__(self, namespace: dict, cls: type):
        """
        Initializes the field object model object from the field
        dictionary, obtained from the class object ``cls``.

        """

        def _collect_fields_from_bases():
            res = {}
            _bases: Tuple[Type] = cls.__bases__
            for base in _bases:
                if hasattr(base, FIELDS):
                    _base_fields: ObjectModel = getattr(base, FIELDS)
                    count = len(_base_fields._fields)
                    if count:
                        logger.debug(
                            f"adding {count} field(s) from the base class {base.__name__}"
                        )
                        res.update(_base_fields._fields)
            return res

        self.cls = cls

        logger.debug("".join(["=" for _ in range(0, 80)]))
        logger.debug(f"build field object model for {cls}...")

        fields = {field.name: field for field in FieldFactory(namespace)()}
        if not fields:
            logger.debug(f"... no fields collected for {cls}")

        for field in fields.values():
            # need to set all fields, otherwise the current default will
            # remain, possibly affecting validation later
            setattr(cls, field.name, copy.deepcopy(field.default))

        # add all the fields from the base classes - they must have been
        # already collected because of the class load order
        base_fields = _collect_fields_from_bases()

        self._fields = self._merge(base_fields, fields)
        self._aliases = frozendict({x.alias: x for x in self._fields.values()})

    def mark_as_dirty(self, obj):
        """
        Sets all attributes and properties of ``obj`` to ``Undefined``.
        """
        for field in filter(lambda x: not x.read_only, self._fields.values()):
            # should not fire on_update with Undefined
            object.__setattr__(obj, field.name, field.default)

    def initialize(self, obj, **kwargs):
        for k, v in kwargs.items():
            setattr(obj, k, v)
        for field in filter(lambda x: x.name not in kwargs, self._fields.values()):
            setattr(obj, field.name, copy.deepcopy(field.default))

    def convert(self, value: object, obj: Any, prop_name: str):
        """
        Performs conversion of the input value, and subsequent validation of
        the conversion result.
        """
        class_name = obj.__class__.__name__
        current_value = obj.get_or_undefined(prop_name)
        if prop_name in self._fields:
            field = self._fields[prop_name]
            if field.is_writable(current_value):
                converted = field.convert(field.maybe_set_default(value))
                if converted != value:
                    logger.debug(
                        f"converted {value} ({value.__class__})"
                        f" to {converted} ({converted.__class__.__name__})"
                    )
                field.validate(obj, converted)
                return current_value, converted
            else:
                raise NotWritableAttributeError(prop_name, class_name)
        else:
            return current_value, value

    def get(self, name: str, default=None):
        return self._fields.get(name, default)

    @property
    def fields(self) -> dict[str, Field]:
        return self._fields

    def get_mutable_fields(self) -> dict[str, Field]:
        return {k: v for k, v in self._fields.items() if not v.const}

    def __getitem__(self, item: str) -> ModelField:
        return self._fields[item]

    def __contains__(self, item: str) -> bool:
        return item in self._fields

    def get_by_alias(self, alias: str) -> ModelField:
        return self._aliases.get(alias, None)

    def _merge(self, base_fields, fields):
        redefined = set.intersection(set(base_fields.keys()), set(fields.keys()))
        if redefined:
            logger.debug(f"the following fields are redefined:")
            for key in redefined:
                logger.debug(f"... from bases: {base_fields[key]}")
                logger.debug(f"... from {self.cls}: {fields[key]}")
            logger.debug(f"... ignoring base definition(s)")

        result = copy.copy(fields)
        for name, field in base_fields.items():
            if name not in result:
                result[name] = field

        return frozendict(result)


class FieldFactory:
    def __init__(self, namespace: dict):
        self.namespace = namespace
        self.module_name: Optional[str] = namespace.get("__module__", None)
        self.class_name = namespace.get("__qualname__", None)

    def __call__(self, *args, **kwargs):
        if (self.module_name, self.class_name) != SKIP_CLASS:
            logger.debug(f"collecting fields for the model class {self.class_name}")
            for name, ann_type in self._resolve_annotations():
                value = self.namespace.get(name, Undefined)
                if not is_class_var(ann_type):
                    field = ModelField(name, ann_type, value)
                    logger.debug(f"... {field}")
                    yield field

    def _resolve_annotations(self) -> Generator:
        """
        Resolves forward references.
        """

        def _is_public_field(_name: str) -> bool:
            return not _name.startswith("__")

        def _eval_type(t, global_ns, local_ns, recursive_guard=frozenset()):
            """
            Evaluate all forward references in the given type t.
            For use of global_ns and local_ns see the docstring for get_type_hints().
            recursive_guard is used to prevent infinite recursion
            with recursive ForwardRef.
            """
            if isinstance(t, ForwardRef):
                # noinspection PyProtectedMember, PyArgumentList
                return t._evaluate(global_ns, local_ns, recursive_guard)
            # if isinstance(t, (_GenericAlias, GenericAlias)):
            if isinstance(t, GenericAlias):
                args = get_args(t)
                origin = get_origin(t)
                ev_args = tuple(
                    _eval_type(a, global_ns, local_ns, recursive_guard) for a in args
                )
                if ev_args == args:
                    return t
                if isinstance(t, GenericAlias):
                    return GenericAlias(origin, ev_args)
                else:
                    return t.copy_with(ev_args)
            return t

        raw_annotations = self.namespace.get("__annotations__", {})
        module = sys.modules[self.module_name]
        base_globals = module.__dict__

        for name in filter(_is_public_field, raw_annotations):
            value = raw_annotations[name]
            if isinstance(value, str):
                value = make_forward_ref(value)
            try:
                value = _eval_type(value, base_globals, None)
            except NameError:
                pass
            yield name, value
