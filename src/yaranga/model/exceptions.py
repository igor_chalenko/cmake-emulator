class NotWritableAttributeError(RuntimeError):
    """
    Raised upon update of a read-only attribute.
    """

    def __str__(self):
        return f"{self.args[1]}.{self.args[0]} is read-only, will not update"


class ValidationError(RuntimeError):
    """
    Raised by field validation functions on validation failure.
    """

    pass
