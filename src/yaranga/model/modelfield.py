import copy
from typing import Optional, Any, Type, Callable, Union

from yaranga.model.converters import convert
from yaranga.model.exceptions import ValidationError
from yaranga.model.field import Field
from yaranga.model.typeinfo import TypeInfo
from yaranga.undefined import Undefined


class ModelField(object):
    """
    Run-time field information. Created from ``Field`` instances by ``ModelMetaclass``.
    Compared to ``Field``, also stores field name, type, and sets the converter to
    default in case the source field didn't provide one.
    Not a part of the public API.

    .. seealso ModelMetaclass
    """

    name: str
    type: TypeInfo
    default: Any = Undefined
    read_only: bool = False
    nullable: bool = True
    const: bool = False
    alias: str = None
    sources: bool = False
    converter: Callable = None
    validators: list[Callable] = None

    def __init__(self, field_name: str, field_type: Type, value: Field):
        def _create():
            default_alias = field_name.upper()
            if isinstance(value, Field):
                args = {
                    "const": value.const,
                    "sources": value.sources,
                    "default": value.default,
                    "read_only": value.read_only,
                    "nullable": value.nullable,
                    "alias": value.alias,
                    "converter": value.converter or convert,
                    "validators": value.validators,
                }
                if value.alias:
                    default_alias = value.alias
            else:
                args = {
                    "converter": convert,
                    "read_only": False,
                    "const": False,
                    "nullable": True,
                }
                if value is not Undefined:
                    args["default"] = value

            args["typ"] = TypeInfo(field_type)
            args["name"] = field_name
            args["alias"] = default_alias

            return args

        def _fill(
            *,
            name: str,
            typ: Union[Type, TypeInfo],
            default: Optional[Any] = Undefined,
            read_only: Optional[bool] = None,
            nullable: bool = True,
            const: bool = False,
            sources: bool = False,
            alias: Optional[str] = None,
            converter: Optional[Callable] = None,
            validators: Optional[list[Callable]] = None,
        ):
            """
            Fills the attributes.
            :param name: field name, the same as the attribute name
            :param typ: field type information, derived from the attribute's possibly generic type
            :param default: attribute's default value
            :param read_only: indicates that this field cannot be updated after initialization
            :param alias: field's alternative nane
            :param converter: value converter
            :param validators: value validators
            """

            self.name = name
            self.type = typ if isinstance(typ, TypeInfo) else TypeInfo(typ)
            self.default = default
            self.const = const
            self.read_only = read_only
            self.nullable = nullable
            self.sources = sources
            self.alias = alias
            self.converter = converter
            self.validators = validators

        _fill(**_create())

    def is_writable(self, current_value: object) -> bool:
        """
        A field is writable if it's not read-only or its value is ``Undefined``.
        The value of ``Undefined`` indicates that the field's value needs to be re-read,
        or it was not yet initialized by the respective ``__init__`` method.
        :param current_value: current field's value
        :return: a boolean indicating whether this field is writable
        """
        if (self.const or self.read_only) and current_value is not Undefined:
            return False
        return True

    def maybe_set_default(self, new_value: object):
        """
        Substitutes the default value for the empty input.
        :param new_value:
        :return: the default value if it's set; ``new_value`` otherwise
        """
        if new_value is None and self.default is not Undefined:
            new_value = copy.deepcopy(self.default)
        return new_value

    def validate(self, validated_object: object, new_value: object) -> None:
        """
        Invokes all the configured validators.

        :param validated_object: a model object
        :param new_value: a new field value
        """
        if new_value is None and not self.nullable:
            raise ValidationError(
                f"The non-optional field {self.name} cannot be set to `None`"
            )

        if self.validators and new_value is not Undefined:
            for validator in self.validators:
                validator(validated_object, new_value)

    def convert(self, new_value: object):
        """
        Invokes the configured converter. ``Undefined`` is returned unchanged.
        :param new_value: the input value
        :return: the converted input value
        """
        if new_value is Undefined or not self.converter:
            return new_value
        return self.converter(new_value, self.type)

    def __str__(self):
        return (
            f"{self.name} (type={self.type}, read_only={self.read_only}, "
            f'alias="{self.alias}", const={self.const}, default={self.default})'
        )
