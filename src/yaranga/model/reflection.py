import sys
from typing import (
    Optional,
    ForwardRef,
    get_origin,
    get_args,
    Dict,
    Type,
    Any,
    Union,
    ClassVar,
)

TypeDict = Dict[str, Type[Any]]

if sys.version_info >= (3, 10) or sys.version_info < (3, 0):
    from types import NoneType
else:
    NoneType = type(None)


def is_class_var(ann_type: Type[Any]) -> bool:
    """
    Checks whether the given type is ``typing.ClassVar``. Supports generic types.
    :param ann_type: the input type
    :return: check result
    """
    return _is_class_var(ann_type) or _is_class_var(get_origin(ann_type))


# todo UnionType from 3.10
def is_union(input_type: Type[Any]) -> bool:
    return input_type is Union or get_origin(input_type) is Union


def is_optional(input_type) -> bool:
    """
    Checks whether the given type is ``typing.Optional``. Supports generic types.
    :param input_type: the input type
    :return: check result
    """
    origin_type = get_origin(input_type)
    type_args = get_args(input_type)
    return is_union(origin_type) and len(type_args) == 2 and type_args[1] == NoneType


def _is_class_var(v: Optional[Type[Any]]) -> bool:
    if v is None:
        return False

    return v.__class__ == ClassVar.__class__ and getattr(v, "_name", None) == "ClassVar"


def make_forward_ref(value: Any) -> ForwardRef:
    if (3, 10) > sys.version_info >= (3, 9, 8) or sys.version_info >= (3, 10, 1):
        # this is for newer Python versions
        # noinspection PyArgumentList
        return ForwardRef(value, is_argument=False, is_class=True)
    elif sys.version_info >= (3, 7):
        return ForwardRef(value, is_argument=False)
    else:
        return ForwardRef(value)
