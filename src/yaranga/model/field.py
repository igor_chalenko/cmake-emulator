"""
Field extends the semantics of Python attribute. It contains read-only
information about an attribute's validation, conversion, and access rules.
Most of the time, a field describes a documented ``CMake`` property. Since
``CMake`` uses upper-case convention for the property names, and Python
uses lower-case, every field has a name and an alias; the name is
the respective attribute's name, and the alias is the respective ``CMake``
property's name. All field's attributes are optional. The following are
supported:

.. glossary::

   default
      A default value. Can be specified by either providing
      the field's attribute ``default``, or assigning the value to
      the attribute directly:

      .. code-block:: python

         class Model:
           # 1. Default value in the field
           meaning_1: int = Field(default = 42)
           # 2. Default value without field
           meaning_2: int = 42

      Both methods are equivalent. If the default value is not provided
      explicitly, it is set to ``Undefined``. Therefore, ``Undefined``
      replaces ``None`` as a value of uninitialized variables. Also,
      it means that initializing a variable means setting it to any value
      other than ``Undefined``.

   read_only
      Read-only flag. Indicates that this field cannot be updated after
      initialization (so it can be set only if the current value is
      ``Undefined``). As a part of invalidation process, a field can be reset
      to ``Undefined`` later, read-only flag doesn't prevent this.

   nullable
      Nullability flag. Indicates that the field cannot be set to ``None``.
      Thus, the default value of a non-nullable cannot be ``None``.

   const
      Const-ness flag. Indicates that the field will be set to the default
      value during initialization. Thus, a constant field must always have
      a default value.

   alias
      An alias string. Most of the time, a field's alias is its name
      upper-cased.

   validators
      Zero or more validation functions.

   converter
      The Conversion function. By default, every field of the boolean,
      integer, enumeration, path, or string type, has a converter for that
      type.

During the class initialization, ``Field`` objects are
converted to their respective :class:`ModelField` instances, which
are stored in the field object model.

.. seealso:: :class:`yaranga.model.model.ModelMetaclass`,
             :class:`yaranga.model.model.ModelField`

"""

from typing import Callable, Optional, Any
from yaranga.undefined import Undefined
from pprint import pformat


class Field(object):
    """
    A field's compile-time representation.
    """

    def __init__(
        self,
        *,
        default: Any = Undefined,
        read_only: bool = False,
        nullable: bool = True,
        const: bool = False,
        alias: Optional[str] = None,
        sources: bool = False,
        validators: Optional[list[Callable]] = None,
        converter: Optional[Callable] = None
    ):
        self.default = default
        self.read_only = read_only
        self.nullable = nullable
        self.const = const
        self.alias = alias
        self.sources = sources
        self.validators = validators
        self.converter = converter

        if self.const and self.default is Undefined:
            raise AttributeError("const fields must have a default value")
        if not self.nullable and self.default is None:
            raise AttributeError(
                "non-nullable fields must not have a default value of `None`"
            )

    def __str__(self):
        return pformat(self)
