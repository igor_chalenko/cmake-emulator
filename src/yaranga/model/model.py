import abc
import logging

from yaranga.constants import FIELDS
from yaranga.model.objectmodel import ObjectModel


class ModelMetaclass(abc.ABCMeta):
    """
    A metaclass for the scope classes. Augments attribute
    semantics via field annotations.
    """

    def __new__(mcs, name, bases, namespace, **kwargs):
        cls = abc.ABCMeta.__new__(mcs, name, bases, namespace)

        field_object_model_class: type = ModelMetaclass._get_fom_class(cls, **kwargs)
        setattr(cls, FIELDS, field_object_model_class(namespace, cls))

        return cls

    @property
    def fields(cls) -> ObjectModel:
        return getattr(cls, FIELDS)

    def _get_fom_class(cls, **kwargs):
        field_object_model_class: type = kwargs.get("fom", ObjectModel)
        existing_fom = getattr(cls, FIELDS, None)
        if field_object_model_class == ObjectModel and existing_fom:
            field_object_model_class = type(existing_fom)
        return field_object_model_class
