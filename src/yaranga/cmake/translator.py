import logging
from pathlib import Path
from typing import Union, Optional

from yaranga.cmake.command_tray import StateUpdateTray
from yaranga.cmake.statemanager import StateManager, Policy
from yaranga.commands.command import Command
from yaranga.commands.function import Function
from yaranga.commands.get import GetTarget, GetCacheVariable, GetDirectory
from yaranga.commands.include import IncludeCommand
from yaranga.commands.setproperty import (
    SetTargetProperty,
    SetTestProperty,
    SetGlobalProperty,
    SetDirectoryProperty,
    SetSourceFileProperty,
)
from yaranga.commands.target_include_directories import TargetIncludeDirectories
from yaranga.commands.target_link_libraries import TargetLinkLibraries
from yaranga.commands.target_sources import TargetSources
from yaranga.objects.cache_variable import CacheVariableType, CacheVariable
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.objects.directory import Directory
from yaranga.objects.directory_dict import DirectoryDict
from yaranga.objects.global_scope import GlobalScope
from yaranga.objects.source_dict import SourceDict
from yaranga.objects.source_file import SourceFile
from yaranga.objects.target import (
    Target,
    Executable,
    Library,
    AliasTarget,
    ImportedLibrary,
)
from yaranga.objects.target_dict import TargetDict
from yaranga.objects.target_type import TargetType, ImportedTargetType
from yaranga.objects.test import Test
from yaranga.objects.test_dict import TestDict
from yaranga.objects.updatetype import UpdateType
from yaranga.objects.variables import CacheVariables
from yaranga.types import FileName, Value, Values, ListOrValue
from yaranga.undefined import Undefined, Ignore


class NoIntercept(object):
    def __init__(self, translator):
        self.translator = translator

    def __enter__(self):
        self.translator._on_update = False

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.translator._on_update = True


class Translator(object):
    def __init__(
        self,
        source_dir: Optional[FileName] = None,
        binary_dir: Optional[FileName] = None,
    ):
        self.sm = StateManager()
        self.command_tray = StateUpdateTray()
        self._on_update = True
        self.cmp0076: Policy = Policy.NEW
        if source_dir and binary_dir:
            self.add_directory(source_dir, binary_dir)

    @property
    def current_source_dir(self) -> Path:
        return self.sm.current_dir.source_dir if self.sm.current_dir else None

    def set_current_directory(self, path: FileName):
        self.sm.set_current_directory(path)

    def get_global_property(self, name: str) -> Value:
        return self.sm.get_global_property(name)

    def set_global_property(
        self,
        name: str,
        value: Value = Undefined,
        update_type: UpdateType = UpdateType.SET,
    ):
        cmd = self.sm.set_global_property(name, value, update_type)
        self.add_command(cmd)

    def get_target_property(self, target_name: str, property_name: str):
        return self.sm.targets[target_name][property_name]

    def set_target_property(
        self,
        targets: Values[str],
        name: str,
        value: Value = None,
        update_type: UpdateType = UpdateType.SET,
    ):
        targets = ListOrValue(targets)
        if self.command_tray.record:
            need_flush = False
            for target in filter(lambda x: x not in self.sm.targets, targets):
                self.add_command(GetTarget(self.current_source_dir, target))
                need_flush = True

            if need_flush:
                self.command_tray.stop()
                self.flush()
                self.command_tray.start()

        for target in targets:
            cmd = self.sm.set_target_property(target, name, value, update_type)
            self.add_command(cmd)

    def get_directory_property(self, path: FileName, name: str) -> Value:
        return self.sm.directories[path][name]

    def set_directory_property(
        self,
        paths: Values[FileName],
        name: str,
        value: Value = Undefined,
        update_type: UpdateType = UpdateType.SET,
    ):
        paths = ListOrValue(paths)
        need_flush = False
        for path in paths:
            prepared_path = self.sm._prepare_path(path)
            if prepared_path not in self.sm.directories:
                self.add_command(GetDirectory(prepared_path))
                need_flush = True

        if need_flush:
            self.flush()

        for path in paths:
            cmd = self.sm.set_directory_property(path, name, value, update_type)
            self.add_command(cmd)

    def get_source_file_property(
        self,
        file: FileName,
        property_name: str,
        directory: Optional[FileName] = None,
        target_directory: Optional[str] = None,
    ):
        if directory:
            path = Path(directory)
        elif target_directory:
            target = self.sm.targets[target_directory]
            path = target.source_dir
        else:
            path = self.current_source_dir

        d: Directory = self.sm.directories[path]
        return d.get_source_file(file)[property_name]

    def set_source_property(
        self,
        filenames: Values[FileName],
        name: str,
        value: Value = None,
        update_type: UpdateType = UpdateType.SET,
        directories: Values[FileName] = None,
        target_directories: Values[FileName] = None,
    ):
        filenames = ListOrValue(filenames)
        for filename in filenames:
            cmd = self.sm.set_source_property(
                filename, name, value, update_type, directories, target_directories
            )
            self.add_command(cmd)

    def get_test_property(self, test_name: str, property_name: str):
        return self.sm.test[test_name][property_name]

    def set_test_property(
        self,
        test_names: Values[str],
        property_name: str,
        value: Value = Undefined,
        update_type: UpdateType = UpdateType.SET,
    ):
        for test_name in ListOrValue(test_names):
            cmd = self.sm.set_test_property(
                test_name, property_name, value, update_type
            )
            self.add_command(cmd)

    def get_cache_variable_property(self, variable: str, name: str):
        return self.sm.cache_variables[variable][name]

    def set_cache_variable_property(
        self,
        variables: Values[str],
        name: str,
        value: Value = Undefined,
        update_type: UpdateType = UpdateType.SET,
    ):
        variables = ListOrValue(variables)
        need_flush = False
        for variable in variables:
            if variable not in self.sm.cache_variables:
                self.add_command(GetCacheVariable(variable))
                need_flush = True

        if need_flush:
            self.flush()

        for variable in variables:
            # if we don't have some variables by this time, this will fail
            cmd = self.sm.set_cache_variable_property(
                variable, name, value, update_type
            )
            self.add_command(cmd)

    def add_executable(
        self,
        name: str,
        win32: Optional[bool] = Undefined,
        macosx_bundle: Optional[bool] = Undefined,
        exclude_from_all: Optional[bool] = Undefined,
        sources: Union[list[FileName], str] = Undefined,
    ) -> Executable:
        return self.sm.add_executable(
            name, win32, macosx_bundle, exclude_from_all, sources
        )

    def add_library(
        self,
        name: str,
        target_type: TargetType,
        exclude_from_all: bool = Ignore,
        sources: Union[list[FileName], str] = Ignore,
        **properties,
    ) -> Library:
        return self.sm.add_library(
            name, target_type, exclude_from_all, sources, **properties
        )

    def add_static_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name, TargetType.STATIC_LIBRARY, exclude_from_all, sources, **properties
        )

    def add_shared_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name, TargetType.SHARED_LIBRARY, exclude_from_all, sources, **properties
        )

    def add_module_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name, TargetType.MODULE_LIBRARY, exclude_from_all, sources, **properties
        )

    def add_object_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name,
            TargetType.OBJECT_LIBRARY,
            exclude_from_all=exclude_from_all,
            sources=sources,
            **properties,
        )

    def add_interface_library(
        self,
        name: str,
        exclude_from_all: Optional[bool] = Ignore,
        sources: list[str] = Ignore,
        **properties,
    ) -> Library:
        return self.add_library(
            name,
            TargetType.INTERFACE_LIBRARY,
            exclude_from_all=exclude_from_all,
            sources=sources,
            **properties,
        )

    def add_imported_library(
        self, name: str, type_: ImportedTargetType, imported_global: bool = False
    ) -> ImportedLibrary:
        return self.sm.add_imported_library(name, type_, imported_global)

    def add_alias_target(self, name: str, target: Union[Target, str]) -> AliasTarget:
        return self.sm.add_alias_target(name, target)

    def add_custom_target(
        self,
        name: str,
        command: Union[str, list[str]],
        working_directory: Optional[FileName] = None,
        all_=Undefined,
        **properties,
    ):
        return self.sm.add_custom_target(
            name, command, working_directory, all_, **properties
        )
        # self.add_command(command)

        # return command

    def add_test(self, test_name: str, **properties):
        return self.sm.add_test(test_name, **properties)

    def set(self, name: str, value: Optional[Value]):
        self[name] = value

    def set_cache_variable(
        self,
        name: str,
        value: Optional[Value],
        typ: Union[CacheVariableType, str] = CacheVariableType.STATIC,
        helpstring: str = "",
        force: bool = False,
    ) -> Optional[CacheVariable]:
        if isinstance(typ, str):
            typ = CacheVariableType[typ]
        return self.sm.set_cache_variable(name, value, typ, helpstring, force)

    def project(
        self,
        name: str,
        version: Optional[str] = None,
        homepage_url=None,
        description=None,
        languages: Optional[list[str]] = None,
    ):
        command = self.sm.project(name, version, homepage_url, description, languages)
        logging.debug(self.add_command(command))

    def target_include_directories(
        self,
        target_name: str,
        public: Optional[Values[str]] = None,
        private: Optional[Values[str]] = None,
        interface: Optional[Values[str]] = None,
    ) -> TargetIncludeDirectories:
        with NoIntercept(self):
            command = self.sm.target_include_directories(
                target_name, public, private, interface
            )
            self.add_command(command)
            return command

    def target_link_libraries(
        self,
        target_name: str,
        public: Optional[Values[str]] = None,
        private: Optional[Values[str]] = None,
        interface: Optional[Values[str]] = None,
    ) -> TargetLinkLibraries:
        with NoIntercept(self):
            command = self.sm.target_link_libraries(
                target_name, public, private, interface
            )
            self.add_command(command)
            return command

    def target_sources(
        self,
        target_name: str,
        public: Optional[Values[str]] = None,
        private: Optional[Values[str]] = None,
        interface: Optional[Values[str]] = None,
    ) -> TargetSources:
        with NoIntercept(self):
            command = self.sm.target_sources(
                target_name, public, private, interface, self.cmp0076
            )
            self.add_command(command)
            return command

    def function(self, name: str, *args):
        return self.add_command(Function(name, *args))

    def include(self, path: FileName):
        self.sm.mark_as_dirty()
        self.add_command(IncludeCommand(path))

    def __setitem__(self, name, value):
        self.add_command(self.sm.set(name, value))

    def __getitem__(self, name):
        return self.sm[name]

    def __delitem__(self, name: str):
        self.sm.__delitem__(name)

    def __contains__(self, item):
        return self.sm.__contains__(item)

    def add_directory(
        self,
        source_dir: FileName,
        binary_dir: FileName,
        parent_dir: Optional[Union[Directory, FileName]] = None,
    ):
        return self.sm.add_directory(source_dir, binary_dir, parent_dir)

    def get_cache_value(self, name: str):
        return self.sm.cache_variables[name]["VALUE"]

    def get_source_file(self, name: FileName) -> Optional[SourceFile]:
        return self.sm.current_dir.get_source_file(name)

    def flush(self, last=False):
        pass

    def on_update(self, obj: CMakeObject, name: str, value: Optional[Value]):
        if not self._on_update:
            return

        field = obj.__class__.get_field(name, by_alias=True)
        if field and field.read_only:
            return

        if isinstance(obj, Target):
            self.add_command(SetTargetProperty(obj, name, value))
        elif isinstance(obj, Test):
            self.add_command(SetTestProperty(obj, name, value))
        elif isinstance(obj, Directory):
            self.add_command(SetDirectoryProperty(obj, name, value))
        elif isinstance(obj, SourceFile):
            self.add_command(
                SetSourceFileProperty(
                    obj.__dict__["location"],
                    name,
                    value,
                    UpdateType.SET,
                    self.sm.current_dir,
                )
            )
        elif isinstance(obj, GlobalScope):
            self.add_command(SetGlobalProperty(obj, name, value))

    def on_dirty_read(self, obj: CMakeObject, name: str):
        if isinstance(
            obj, (CacheVariables, TestDict, TargetDict, DirectoryDict, SourceDict)
        ):
            obj.on_dirty_read(name, self)
        else:
            self.add_command(self.sm.on_dirty_read(obj, name))
            self.flush()

    def add_command(self, command: Command):
        # logging.debug(f'{command.as_python_string()} -> {command}')
        self._on_command(command)
        return command

    def ignore_commands(self):
        self.command_tray.stop()

    def record_commands(self):
        self.command_tray.start()

    def _on_command(self, command: Command):
        self.command_tray.on_command(command)

    @property
    def tests(self):
        return self.sm.test

    @property
    def cache(self):
        return self.sm.cache_variables

    @property
    def globals(self):
        return self.sm.globals

    @property
    def targets(self):
        return self.sm.targets

    @property
    def directories(self):
        return self.sm.directories


class Silencer:
    def __init__(self, translator: Translator):
        self.translator = translator

    def __enter__(self):
        self.translator.ignore_commands()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.translator.record_commands()


def silent(translator: Translator):
    return Silencer(translator)
