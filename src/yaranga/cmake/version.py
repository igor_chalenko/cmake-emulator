__author__ = "Igor Chalenko"
__copyright__ = "Copyright 2021 Igor Chalenko"
__credits__ = ["Igor Chalenko"]
__license__ = "MIT"
__version__ = "0.1.0"
__email__ = "igor.chalenko@gmail.com"
__status__ = "Development"

import re


class Version:
    """
    CMake project version.  Parses a given string into single-number version
    attributes of a `CMake` version: ``major``, ``minor``, ``patch``, and
    `tweak`` version numbers.
    """

    pattern = re.compile("([^.]+)\\.?([^.]+)?\\.?([^.]+)?\\.?([^.]+)?")

    def __init__(self, version_string: str):
        """
        Initializes a version object with the given string.
        :param version_string a version string in the CMake format
        :raises ValueError if the version string is invalid
        """
        match = Version.pattern.match(version_string)
        if match:
            cnt = len(list(filter(lambda x: x is not None, match.groups())))
            self.major = match.group(1)
            self.minor = match.group(2) if cnt > 1 else None
            self.patch = match.group(3) if cnt > 2 else None
            self.tweak = match.group(4) if cnt > 3 else None
        else:
            raise ValueError(f'VERSION "{version_string}" format invalid.')

    def __str__(self):
        """
        Reconstructs the version string that was given to __init__ method.
        """
        return ".".join(filter(None, [self.major, self.minor, self.patch, self.tweak]))
