from enum import Enum
from pathlib import Path
from typing import Union, Optional

from yaranga.cmake.cmake_directory import CMakeDirectory
from yaranga.commands.get import (
    GetDirectoryProperty,
    GetGlobalProperty,
    GetCacheVariableProperty,
    GetTargetProperty,
    GetTestProperty,
    GetSourceFileProperty,
    Get,
)
from yaranga.commands.project import Project
from yaranga.commands.set import Set
from yaranga.commands.setproperty import (
    SetDirectoryProperty,
    SetTargetProperty,
    SetGlobalProperty,
    SetSourceFileProperty,
    SetTestProperty,
    SetCacheVariableProperty,
)
from yaranga.commands.target_include_directories import TargetIncludeDirectories
from yaranga.commands.target_link_libraries import TargetLinkLibraries
from yaranga.commands.target_sources import TargetSources
from yaranga.objects.cache_variable import CacheVariable, CacheVariableType
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.objects.directory import Directory
from yaranga.objects.directory_dict import DirectoryDict
from yaranga.objects.global_scope import GlobalScope
from yaranga.objects.source_file import SourceFile
from yaranga.objects.target import (
    Target,
    Executable,
    AliasTarget,
    Library,
    ImportedLibrary,
)
from yaranga.objects.target_dict import TargetDict
from yaranga.objects.target_type import TargetType, ImportedTargetType
from yaranga.objects.test import Test
from yaranga.objects.test_dict import TestDictWrapper
from yaranga.objects.updatetype import UpdateType
from yaranga.objects.variables import Variables, CacheVariables
from yaranga.types import FileName, Value, Values, ListOrValue
from yaranga.undefined import Undefined, Ignore


class Policy(Enum):
    OLD = 1
    NEW = 2


class StateManager(object):
    def __init__(self):
        self.working_dir = None
        self.globals = GlobalScope()
        self.variables = Variables()
        self.cache_variables = CacheVariables()
        self.current_dir: Optional[CMakeDirectory] = None

        # targets belong to directories, but still have a unique name within a project
        self.targets: TargetDict = TargetDict()
        self.directories: DirectoryDict = DirectoryDict()

        # test dictionary imbued with CMakeObject's power
        self.test = TestDictWrapper(self)

    def set_current_directory(self, path: FileName):
        self.current_dir = self.directory(path)

    def directory(self, path):
        prepared_path = self._prepare_path(path)
        return self.directories[prepared_path]

    def set_global_property(
        self, name: str, value: Value, update_type: UpdateType = UpdateType.SET
    ):
        cmd = SetGlobalProperty(self.globals, name, value, update_type)
        cmd.exec()
        return cmd

    def set_target_property(
        self,
        target: str,
        name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        cmd = SetTargetProperty(self.targets[target], name, value, update_type)
        cmd.exec()
        return cmd

    def set_directory_property(
        self,
        path: Path,
        name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        prepared_path = self._prepare_path(path)
        cmd = SetDirectoryProperty(
            self.directories[prepared_path], name, value, update_type
        )
        cmd.exec()
        return cmd

    def set_source_property(
        self,
        source_file: str,
        name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
        directories: Union[list[FileName], FileName] = None,
        target_directories: Union[list[FileName], FileName] = None,
    ):
        dirs = []
        if directories:
            if not isinstance(directories, list):
                directories = [directories]
            for path in directories:
                dir_path = self.directory(path)
                dirs.append(dir_path)

        target_dirs = []
        if target_directories:
            if not isinstance(target_directories, list):
                target_directories = [target_directories]
            for target_name in target_directories:
                target = self.targets[target_name]
                target_dirs.append(target.source_dir)

        cmd = SetSourceFileProperty(
            source_file, name, value, update_type, self.current_dir, dirs, target_dirs
        )
        cmd.exec()
        return cmd

    def set_test_property(
        self,
        test_name: str,
        property_name: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        test = self.test[test_name]
        if test is not Undefined:
            cmd = SetTestProperty(test, property_name, value, update_type)
            cmd.exec()
            return cmd
        else:
            raise ValueError(
                f"set_property given TEST names that do not exist:\n\t{test_name}"
            )

    def set_cache_variable_property(
        self,
        name: str,
        prop: str,
        value: Value,
        update_type: UpdateType = UpdateType.SET,
    ):
        # will raise if `name` was not yet created
        existing = self.cache_variables[name]
        cmd = SetCacheVariableProperty(existing, prop, value, update_type)
        cmd.exec()
        return cmd

    def add_executable(
        self,
        name: str,
        win32: bool = Undefined,
        macosx_bundle: bool = Undefined,
        exclude_from_all: bool = Undefined,
        sources: Union[list[FileName], str] = Undefined,
        **kwargs,
    ) -> Executable:
        executable = self.current_dir.add_executable(
            name=name,
            win32=win32,
            bundle=macosx_bundle,
            exclude_from_all=exclude_from_all,
            sources=sources,
            **kwargs,
        )

        self.targets[name] = executable
        return executable

    def add_library(
        self,
        name: str,
        target_type: Optional[TargetType],
        exclude_from_all: bool = Ignore,
        sources: Union[list[FileName], str] = Ignore,
        **properties,
    ) -> Library:
        build_shared_libs = self.variables.get("BUILD_SHARED_LIBS", False)
        if not target_type:
            target_type = (
                TargetType.SHARED_LIBRARY
                if build_shared_libs
                else TargetType.STATIC_LIBRARY
            )

        library = self.current_dir.add_normal_library(
            name, target_type, exclude_from_all, sources, **properties
        )

        self.targets[name] = library
        return library

    def add_interface_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name,
            TargetType.INTERFACE_LIBRARY,
            exclude_from_all,
            sources,
            **properties,
        )

    def add_static_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name, TargetType.STATIC_LIBRARY, exclude_from_all, sources, **properties
        )

    def add_shared_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name, TargetType.SHARED_LIBRARY, exclude_from_all, sources, **properties
        )

    def add_module_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name, TargetType.MODULE_LIBRARY, exclude_from_all, sources, **properties
        )

    def add_object_library(
        self,
        name: str,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties,
    ) -> Library:
        return self.add_library(
            name,
            TargetType.OBJECT_LIBRARY,
            exclude_from_all=exclude_from_all,
            sources=sources,
            **properties,
        )

    def add_imported_library(
        self,
        name: str,
        type_: ImportedTargetType,
        imported_global: bool = False,
    ) -> ImportedLibrary:
        library = self.current_dir.add_imported_library(name, type_, imported_global)
        self.targets[name] = library
        return library

    def add_alias_target(self, name: str, target: Union[Target, str]) -> AliasTarget:
        if not isinstance(target, Target):
            target = self.targets[target]
        alias_target = self.current_dir.add_alias_target(name, target)
        self.targets[name] = alias_target
        return alias_target

    def set(self, name: str, value: Optional[Value]):
        return Set(self, name, value)

    def set_cache_variable(
        self,
        name: str,
        value: Optional[Value],
        value_type: CacheVariableType = CacheVariableType.STATIC,
        helpstring: str = "",
        force: bool = False,
    ):
        if value is not None:
            if name in self.cache_variables:
                existing = self.cache_variables[name]
                if force:
                    existing.value = value
                    existing.type = value_type
                    existing.helpstring = helpstring
            else:
                existing = CacheVariable(
                    name=name, value=value, typ=value_type, helpstring=helpstring
                )
                self.cache_variables[name] = existing
            return existing
        else:
            del self.cache_variables[name]

        return None

    def add_directory(
        self,
        source: Path,
        binary: Path,
        parent: Optional[Union[Directory, FileName]] = None,
    ):
        source = self._prepare_path(source)
        binary = self._prepare_path(binary)
        if parent:
            if not isinstance(parent, Directory):
                parent = self._prepare_path(parent)
                parent_directory = self.directories[parent]
            else:
                parent_directory = parent
        else:
            parent_directory = None

        directory = CMakeDirectory(
            source_dir=source, binary_dir=binary, parent=parent_directory
        )

        self.directories[source] = directory

        if self.current_dir:
            for source in self.current_dir.sources:
                directory.set_source_property(source, "LOCATION", source)

        self.current_dir = directory
        return directory

    def add_custom_target(
        self,
        name: str,
        command: Union[str, list[str]],
        working_directory: Optional[FileName],
        all_: bool,
        **properties,
    ):
        target = self.current_dir.add_custom_target(
            name, command, working_directory=working_directory, all_=all_, **properties
        )
        self.targets[name] = target

        return target

    def add_test(self, test_name: str, **properties):
        return self.current_dir.add_test(test_name, **properties)

    def project(
        self,
        name: str,
        version: Optional[str] = None,
        homepage_url=None,
        description=None,
        languages: Optional[list[str]] = None,
    ):
        if not self.current_dir:
            raise AssertionError("current directory is not set")

        root_source_dir = self._prepare_path(self["CMAKE_SOURCE_DIR"])
        top_level = root_source_dir == self.current_dir.source_dir

        return Project(
            self,
            self.current_dir,
            top_level,
            name,
            version,
            homepage_url,
            description,
            languages,
        )

    def target_include_directories(
        self,
        target_name: str,
        public: Optional[Values[str]] = None,
        private: Optional[Values[str]] = None,
        interface: Optional[Values[str]] = None,
    ) -> TargetIncludeDirectories:
        target = self.targets[target_name]
        if not isinstance(target, AliasTarget):
            command = TargetIncludeDirectories(
                self.current_dir.source_dir, target_name, public, private, interface
            )
            if public:
                target.set_property("INCLUDE_DIRECTORIES", public, UpdateType.APPEND)
                target.set_property(
                    "INTERFACE_INCLUDE_DIRECTORIES", public, UpdateType.APPEND
                )
            if private:
                target.set_property("INCLUDE_DIRECTORIES", private, UpdateType.APPEND)
            if interface:
                target.set_property(
                    "INTERFACE_INCLUDE_DIRECTORIES", interface, UpdateType.APPEND
                )
            return command
        else:
            raise ValueError(
                f"{target_name} is an alias target and may not have include directories"
            )

    def target_link_libraries(
        self,
        target_name: str,
        public: Optional[Values[str]],
        private: Optional[Values[str]],
        interface: Optional[Values[str]],
    ) -> TargetLinkLibraries:
        target = self.targets[target_name]
        if not isinstance(target, AliasTarget):
            command = TargetLinkLibraries(target.name, public, private, interface)
            if public:
                target.set_property("LINK_LIBRARIES", public, UpdateType.APPEND)
                target.set_property(
                    "INTERFACE_LINK_LIBRARIES", public, UpdateType.APPEND
                )
            if private:
                target.set_property("LINK_LIBRARIES", private, UpdateType.APPEND)
                target.set_property(
                    "INTERFACE_LINK_LIBRARIES",
                    [f"$<LINK_ONLY:{x}>" for x in private],
                    UpdateType.APPEND,
                )
            if interface:
                target.set_property(
                    "INTERFACE_LINK_LIBRARIES", interface, UpdateType.APPEND
                )
            return command
        else:
            raise ValueError(
                f" target_link_libraries can not be used on an ALIAS target {target_name}"
            )

    def _maybe_absolute(self, x: FileName):
        if isinstance(x, str):
            if x.startswith("$<"):
                return x
            else:
                x = Path(x)
        if not x.is_absolute():
            x = Path(self.current_dir.source_dir) / x
        return x

    def _maybe_convert(self, sources: Optional[Values[FileName]]):
        return (
            [self._maybe_absolute(x) for x in ListOrValue(sources)] if sources else None
        )

    def target_sources(
        self,
        target_name: str,
        public: Optional[Values[FileName]],
        private: Optional[Values[FileName]],
        interface: Optional[Values[FileName]],
        cmp0076: Policy = Policy.NEW,
    ) -> TargetSources:
        target: Target = self.targets[target_name]
        if not isinstance(target, AliasTarget):
            interface_amend_needed = True
            if (
                cmp0076 == Policy.NEW
                and target.source_dir.source_dir != self.current_dir.source_dir
            ):
                public = self._maybe_convert(public)
                private = self._maybe_convert(private)
                interface = self._maybe_convert(interface)
                interface_amend_needed = False

            command = TargetSources(target.name, public, private, interface)
            if public:
                target.set_property("SOURCES", public, UpdateType.APPEND)
                target.set_property("INTERFACE_SOURCES", public, UpdateType.APPEND)
            if private:
                target.set_property("SOURCES", private, UpdateType.APPEND)
            if interface and interface_amend_needed:
                interface = self._maybe_convert(interface)
                target.set_property("INTERFACE_SOURCES", interface, UpdateType.APPEND)
            return command
        else:
            raise ValueError(
                f" target_sources can not be used on an ALIAS target {target_name}"
            )

    def mark_as_dirty(self):
        self.globals.mark_as_dirty()
        self.cache_variables.mark_as_dirty()
        self.variables.mark_as_dirty()
        for target in self.targets.values():
            target.mark_as_dirty()
        for directory in self.directories.values():
            directory.mark_as_dirty()

    def __setitem__(self, key, value):
        self.set(key, value)

    def __getitem__(self, item):
        return self.variables[item]

    def __delitem__(self, name: str):
        self.set(name, Undefined)

    def __contains__(self, item) -> bool:
        return item in self.variables

    def _prepare_path(self, value: FileName):
        if isinstance(value, str):
            value = Path(value)
        if not value.is_absolute():
            if self.current_dir:
                value = self.current_dir.source_dir / value
            else:
                raise ValueError(
                    f"{value} may not be relative until current directory is set"
                )
        return value

    def on_dirty_read(self, obj: CMakeObject, name: str):
        if isinstance(obj, GlobalScope):
            return GetGlobalProperty(name)
        elif isinstance(obj, CacheVariable):
            return GetCacheVariableProperty(obj.name, name)
        elif isinstance(obj, Directory):
            return GetDirectoryProperty(str(obj.source_dir), name)
        elif isinstance(obj, Target):
            return GetTargetProperty(obj.name, name)
        elif isinstance(obj, Test):
            return GetTestProperty(obj.source_dir, obj.name, name)
        elif isinstance(obj, SourceFile):
            return GetSourceFileProperty(
                self.current_dir.source_dir, str(obj.location), name
            )
        elif isinstance(obj, Variables):
            return Get(name)

        raise ValueError(f"The object {type(obj)} is not recognized")

    def get_global_property(self, name: str) -> Value:
        return self.globals[name]
