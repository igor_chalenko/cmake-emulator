import yaml

from yaranga.cmake.statemanager import StateManager


class ContextLoader:
    def __init__(self, sm: StateManager):
        self.sm = sm

    def load(self, resource):
        # context loader should always start with the clean command list;
        commands = yaml.safe_load(resource)
        self.loads(commands)

    def loads(self, commands: str):
        if commands:
            state = ""
            for command in commands:
                cmd = command.replace("\\", "\\\\")
                state += f"self.sm.{cmd}\n"
            eval(compile(state, "<string>", "exec"))
