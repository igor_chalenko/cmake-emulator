import abc

from yaranga.commands.command import Command


class CommandTray(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def start(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def stop(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def on_command(self, command: Command):
        raise NotImplementedError()

    @abc.abstractmethod
    def get_commands(self) -> list[Command]:
        raise NotImplementedError()


class NoOpCommandTray(CommandTray):
    def start(self):
        pass

    def stop(self):
        pass

    def on_command(self, command: Command):
        pass

    def get_commands(self) -> list[Command]:
        return []


class StateUpdateTray(CommandTray):
    def __init__(self):
        self.commands: list[Command] = []
        self.record = True

    def start(self):
        self.record = True

    def stop(self):
        self.record = False

    def on_command(self, command: Command):
        if self.record:
            self.commands.append(command)

    def get_commands(self) -> list[Command]:
        return self.commands
