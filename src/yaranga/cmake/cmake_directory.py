from yaranga.objects.directory import Directory
from yaranga.objects.target import (
    Library,
    Executable,
    AliasTarget,
    Target,
    ImportedLibrary,
    CustomTarget,
)
from yaranga.objects.target_type import TargetType, ImportedTargetType
from yaranga.types import FileName
from yaranga.undefined import Ignore


class CMakeDirectory(Directory):
    def add_normal_library(
        self,
        name: str,
        target_type: TargetType,
        exclude_from_all: bool = Ignore,
        sources: list[FileName] = Ignore,
        **properties
    ):
        library = Library(
            source_dir=self,
            name=name,
            type_=target_type,
            exclude_from_all=exclude_from_all,
            sources=sources,
            **properties
        )

        self._buildsystem_targets.append(name)
        return library

    def add_imported_library(
        self,
        name: str,
        type_: ImportedTargetType,
        imported_global: bool = False,
    ) -> ImportedLibrary:
        library = ImportedLibrary(
            name=name,
            imported_target_type=type_,
            source_dir=self,
            imported_global=imported_global,
        )
        self._imported_targets.append(library.name)
        return library

    def add_alias_target(self, name, target: Target) -> AliasTarget:
        target = AliasTarget(name=name, target=target, source_dir=self)
        self._imported_targets.append(target.name)
        return target

    def add_executable(
        self,
        name: str,
        win32: bool = False,
        macosx_bundle: bool = False,
        exclude_from_all: bool = False,
        sources: list[str] = None,
        **properties
    ) -> Executable:
        if not sources:
            sources = []
        if not isinstance(sources, list):
            sources = [sources]
        executable = Executable(
            source_dir=self,
            name=name,
            sources=sources,
            win32_executable=win32,
            macosx_bundle=macosx_bundle,
            exclude_from_all=exclude_from_all,
            **properties
        )
        self._buildsystem_targets.append(name)
        return executable

    def add_custom_target(self, name, command, working_directory, all_: bool, **kwargs):
        target = CustomTarget(
            source_dir=self,
            name=name,
            command=command,
            working_directory=working_directory,
            all_=all_,
            **kwargs
        )
        self._buildsystem_targets.append(name)
        return target
