from typing import Optional, Any

from yaranga.commands.set import SetCacheVariable
from yaranga.constants import FIELDS
from yaranga.objects.cache_variable import CacheVariable
from yaranga.objects.cmakeobject import CMakeObject
from yaranga.objects.target import (
    Library,
    Executable,
    ImportedLibrary,
    AliasTarget,
    CustomTarget,
)
from yaranga.commands.add_alias_target import AddAliasTarget
from yaranga.commands.add_custom_target import AddCustomTarget
from yaranga.commands.add_executable import AddExecutable
from yaranga.commands.add_imported_library import AddImportedLibrary
from yaranga.commands.add_library import AddLibrary
from yaranga.commands.add_test import AddTest
from yaranga.objects.test import Test
from yaranga.types import Value
from yaranga.undefined import Undefined, Ignore


class Injector(object):
    """
    This class injects attribute update hooks via custom dunder methods
    implementation. The purpose is to enable state synchronization via
    ``CMake``.
    """

    IGNORABLE_FIELDS = {"__dict__", "__class__", "properties", FIELDS}

    def __init__(self, translator):
        self.translator = translator
        self._methods_backup = None

    @staticmethod
    def _backup_dunder_methods():
        return {
            "__setattr__": CMakeObject.__setattr__,
            "__setitem__": CMakeObject.__setitem__,
            "__getitem__": CMakeObject.__getitem__,
            "__delitem__": CMakeObject.__delitem__,
            "__contains__": CMakeObject.__contains__,
            "_init_executable": Executable.__init__,
            "_init_library": Library.__init__,
            "_init_test": Test.__init__,
            "_init_cache_variable": CacheVariable.__init__,
            "_init_imported_library": ImportedLibrary.__init__,
            "_init_alias_target": AliasTarget.__init__,
            "_init_custom_target": CustomTarget.__init__,
        }

    def __enter__(self):
        class NoIntercepting(object):
            def __init__(self, translator):
                self.translator = translator

            def __enter__(self):
                self.translator._on_update = False

            def __exit__(self, exc_type, exc_val, exc_tb):
                self.translator._on_update = True

        def _init(fn, self_object, *args, **kwargs):
            self._methods_backup[fn](self_object, *args, **kwargs)

        def _init_executable(self_object: Executable, *args, **kwargs):
            with NoIntercepting(self.translator):
                self.translator.add_command(AddExecutable(*args, **kwargs))
            _init("_init_executable", self_object, *args, **kwargs)

        def _init_cache_variable(self_object: CacheVariable, *args, **kwargs):
            with NoIntercepting(self.translator):
                self.translator.add_command(SetCacheVariable(*args, **kwargs))
            _init("_init_cache_variable", self_object, **kwargs)

        def _init_library(self_object: Library, **kwargs):
            with NoIntercepting(self.translator):
                self.translator.add_command(AddLibrary(**kwargs))
            _init("_init_library", self_object, **kwargs)

        def _init_test(self_object: Test, **kwargs):
            with NoIntercepting(self.translator):
                self.translator.add_command(AddTest(**kwargs))
            _init("_init_test", self_object, **kwargs)

        def _init_imported_library(self_object: ImportedLibrary, *args, **kwargs):
            with NoIntercepting(self.translator):
                self.translator.add_command(AddImportedLibrary(*args, **kwargs))
            _init("_init_imported_library", self_object, *args, **kwargs)

        def _init_alias_target(self_object: AliasTarget, **kwargs):
            with NoIntercepting(self.translator):
                self.translator.add_command(AddAliasTarget(**kwargs))
            _init("_init_alias_target", self_object, **kwargs)

        def _init_custom_target(self_object: CustomTarget, **kwargs):
            with NoIntercepting(self.translator):
                self.translator.add_command(AddCustomTarget(**kwargs))
            _init("_init_custom_target", self_object, **kwargs)

        def _getattribute(self_object, field_name: str) -> Optional[Value]:
            res = object.__getattribute__(self_object, field_name)
            if field_name not in Injector.IGNORABLE_FIELDS:
                maybe_prop = getattr(type(self_object), field_name)
                if not isinstance(maybe_prop, property) and res is Undefined:
                    # convert the input field name to its alias, then our
                    # calls to `on_dirty_read` are consistent with those
                    # calls in _getitem
                    alias = self_object.field_alias_from_name(field_name)
                    self.translator.on_dirty_read(self_object, alias)
                    res = object.__getattribute__(self_object, field_name)
            return res

        def _getitem(self_object: CMakeObject, key: str) -> Any:
            res = self._methods_backup["__getitem__"](self_object, key)
            if res is Undefined:
                self.translator.on_dirty_read(self_object, key)
                res = self._methods_backup["__getitem__"](self_object, key)
            return res

        def _setitem(self_object, key, value):
            self._methods_backup["__setitem__"](self_object, key, value)
            if value is not Undefined:
                self.translator.on_update(self_object, key, value)

        def _delitem(self_object, key):
            self._methods_backup["__delitem__"](self_object, key)
            self.translator.on_update(self_object, key, None)

        def _setattr(self_object, name: str, value: Value):
            res = self._methods_backup["__setattr__"](self_object, name, value)
            if name not in Injector.IGNORABLE_FIELDS and res and value is not Ignore:
                alias: str = self_object.field_alias_from_name(name)
                self.translator.on_update(self_object, alias, value)

        self._methods_backup = self._backup_dunder_methods()
        init = "__init__"

        setattr(Executable, init, _init_executable)
        setattr(Library, init, _init_library)
        setattr(Test, init, _init_test)
        setattr(ImportedLibrary, init, _init_imported_library)
        setattr(AliasTarget, init, _init_alias_target)
        setattr(CustomTarget, init, _init_custom_target)
        setattr(CacheVariable, init, _init_cache_variable)

        setattr(CMakeObject, "__getattribute__", _getattribute)
        setattr(CMakeObject, "__getitem__", _getitem)
        setattr(CMakeObject, "__setitem__", _setitem)
        setattr(CMakeObject, "__setattr__", _setattr)
        setattr(CMakeObject, "__delitem__", _delitem)

    def __exit__(self, exc_type, exc_val, exc_tb):
        init = "__init__"

        setattr(Executable, init, self._methods_backup["_init_executable"])
        setattr(Library, init, self._methods_backup["_init_library"])
        setattr(Test, init, self._methods_backup["_init_test"])
        setattr(ImportedLibrary, init, self._methods_backup["_init_imported_library"])
        setattr(AliasTarget, init, self._methods_backup["_init_alias_target"])
        setattr(CustomTarget, init, self._methods_backup["_init_custom_target"])
        setattr(CacheVariable, init, self._methods_backup["_init_cache_variable"])

        delattr(CMakeObject, "__getattribute__")
        setattr(CMakeObject, "__getitem__", self._methods_backup["__getitem__"])
        setattr(CMakeObject, "__setitem__", self._methods_backup["__setitem__"])
        setattr(CMakeObject, "__setattr__", self._methods_backup["__setattr__"])
        setattr(CMakeObject, "__delitem__", self._methods_backup["__delitem__"])
