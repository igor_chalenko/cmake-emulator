"""

This module defines project-wide types that a CMake property may have.

  * :class:`ListElement` is a union of the types that list elements may have.
    Since ``CMake`` doesn't support nested lists, the definition is not
    recursive.
  * :class:`Value` is a union of the types that a ``CMake`` property value
    may have. These types roughly correspond to the types supported (at least
    on semantic level) by ``CMake``. Some properties may also have other types,
    such as ``Any``, but ``Value`` should be used whenever possible.
  * :class:`FileName` is either a string or a path-like object.

"""

__author__ = "Igor Chalenko"
__copyright__ = "Copyright 2021 Igor Chalenko"
__credits__ = ["Igor Chalenko"]
__license__ = "MIT"
__version__ = "0.1.0"
__email__ = "igor.chalenko@gmail.com"
__status__ = "Development"

from enum import Enum
from pathlib import Path
from typing import Union, TypeVar, List

from yaranga.undefined import UndefinedType

ListElement = Union[bool, int, float, str, Enum, Path]
"""
Types that may appear inside a list property. Note that this doesn't include
``UndefinedType``, because there are no nested properties.
"""

Value = Union[
    bool,
    int,
    float,
    str,
    Enum,
    Path,
    dict[str, ListElement],
    list[ListElement],
    UndefinedType,
    None,
]
"""
Union of the types that a property's value may have:

  * ``bool`` is for the strings that are recognized by CMake as boolean value
    representations;
  * ``int`` is for the strings that are convertible to an integer;
  * ``float`` is for the strings that are convertible to a floating-point 
    number (64-bit precision);
  * ``str`` is for single values that may not grow to a list because of the
    property's semantics;
  * ``dict`` is for dictionaries of primitive values - ``CMake`` doesn't support 
    nested lists;
  * ``list`` is for lists of primitive values - ``CMake`` doesn't support nested 
    lists;
  * ``Enum`` is for Python enumerations that capture CMake constants, such as
    target types, for example;
  * ``Path`` for file (and directory) names;
  * ``NoneType`` is for empty strings and the strings of the form 
    ``<var>-NOTFOUND``;
  * ``UndefinedType`` is a special type that a property value may have when that
    property is not in sync with ``CMake``.

  .. seealso TargetType 
"""

FileName = Union[Path, str]
"""
Used for the properties that can, but not necessarily do hold a value that is
a path. This type serves two purposes:
 
  * In the public API, it allows using strings without constructing a path 
    object first;
  * It emphasizes the duality of the property's value, reminding the handling
    code to branch correctly.
  
"""

T = TypeVar("T")

Values = Union[T, list[T]]


class ListOrValue(List):
    def __init__(self, values: Union[T, list[T]]):
        if not isinstance(values, list):
            values = [values]
        super(ListOrValue, self).__init__(values)
