frozendict==2.3.4
pyyaml~=6.0

setuptools==65.4.0

pytest==7.1.3
pytest-cov
pytest-profiling
pytest-cases
pytest-timeout

# sphinx==5.1.1
# sphinx_rtd_theme
# sphinxcontrib-confluencebuilder==1.9.0
# myst_parser==0.17.0
