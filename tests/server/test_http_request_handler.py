import tempfile
from typing import Optional

import pytest

from yaranga.server.http import RequestHandlerBase
from yaranga.server.router import Router
from yaranga.server.signals import Flush, Directory, Signal


class RequestHandlerBaseUnderTest(RequestHandlerBase):
    def __init__(self, router):
        super(RequestHandlerBaseUnderTest, self).__init__()

        self.router = router
        self.path = None
        self._ok = False
        self._notfound = False

    def send(self, signal: Signal):
        self.router.send(signal)

    def poll(self, uid: str) -> Optional[Signal]:
        return self.router.poll_outgoing(uid)

    def uri(self) -> str:
        return self.path

    def ok(self, _: bytes):
        self._ok = True

    def is_ok(self):
        return self._ok

    def not_found(self):
        self._notfound = True

    def is_not_found(self):
        return self._notfound


class TestHttpRequestHandler(object):
    @pytest.fixture
    def working_dir(self):
        with tempfile.TemporaryDirectory() as tmp_dir:
            yield tmp_dir

    @pytest.fixture
    def router(self, working_dir) -> Router:
        return Router()

    @pytest.fixture
    def uid(self):
        return "uid"

    @pytest.fixture
    def payload(self):
        return "payload"

    @pytest.fixture
    def handler(self, router):
        return RequestHandlerBaseUnderTest(router)

    def test_start(self, router, handler, payload, uid):
        bus = router.add_bus(uid)

        handler.path = "/directory"
        handler._handle(uid, payload)
        assert handler.is_ok()

        signal = bus.poll_incoming()
        assert signal.payload() == payload
        assert isinstance(signal, Directory)

    @pytest.mark.timeout(10)
    def test_flush(self, router, handler, payload, uid):
        bus = router.add_bus(uid)

        handler.path = "/flush"
        bus.route(Flush(uid, payload))
        handler._handle(uid, payload)

        assert handler.is_ok()


if __name__ == "__main__":
    pytest.main()
