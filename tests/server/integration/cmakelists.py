from yaranga.server.context import CMakeContext


def run(cmake: CMakeContext):
    var = cmake.globals["CPM_INITIALIZED"]
    print(f"!!! var = {var} !!!")
    assert var == "ON"
