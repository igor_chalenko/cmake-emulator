from typing import Type

import pytest
from pytest_cases import fixture, parametrize

from tests.server.integration.forcible_stop.client import ClientScenario
from tests.server.integration.forcible_stop.server import ServerScenario
from tests.server.integration.integration import IntegrationTest
from tests.server.integration.mixins.file import FileTransportMixin
from tests.server.integration.mixins.http import HttpTransportMixin
from tests.server.integration.observable import ObservableVirtualMachine
from yaranga.server.context import CMakeContext


@pytest.mark.integration_test
class TestForcibleStop(IntegrationTest):
    @fixture
    @pytest.mark.parametrize("context_class", [ServerScenario, CMakeContext])
    def machine(self, config, context_class: Type[CMakeContext] = CMakeContext):
        return ObservableVirtualMachine(config, context_class)

    @fixture
    def scenario(self, sender):
        return ClientScenario(sender)

    @fixture
    def transport(self, machine):
        yield machine.transport
        machine.transport.stop()

    @fixture
    @parametrize("mixin", [HttpTransportMixin, FileTransportMixin])
    def settings(self, mixin, working_dir):
        return mixin.settings(working_dir)

    @staticmethod
    def test(uid: str, scenario_runner, machine: ObservableVirtualMachine):
        scenario_runner.run(uid)

        with pytest.raises(ValueError):
            machine.after_remove_application(lambda: machine.remove_application(uid))


if __name__ == "__main__":
    pytest.main()
