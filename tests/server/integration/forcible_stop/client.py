from yaranga.server.abstractrouter import Sender
from yaranga.server.signals import Stop


class ClientScenario:
    def __init__(self, sender: Sender):
        self._sender = sender

    def run(self, uid: str):
        self._sender.send(Stop(uid))
