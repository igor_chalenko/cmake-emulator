from pathlib import Path
from typing import Type

from yaranga.server.context import CMakeContext
from yaranga.server.router import Router
from yaranga.server.signals import Stop, Done, Signal


class ServerScenario(CMakeContext):
    def __init__(self, uid: str, router: Router):
        super(ServerScenario, self).__init__(uid, router)

    def run(self):
        def receive(signal_type: Type[Signal]):
            signal = self._router.select(self._uid)
            assert isinstance(signal, signal_type)

        def send(signal_type: Type[Signal]):
            self._router.send(signal_type(self._uid))

        receive(Stop)
        send(Done)
