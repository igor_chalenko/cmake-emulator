import abc
import os
import shutil
import tempfile
from pathlib import Path
from typing import Type

from pytest_cases import fixture

from tests.server.integration.mixins.scenario import ScenarioMixin
from yaranga.server.context import CMakeContext
from yaranga.server.signals import (
    SIGNAL_TYPES,
)


class IntegrationTest(ScenarioMixin, abc.ABC):
    @fixture
    def working_dir(self):
        # with tempfile.TemporaryDirectory() as tmp_dir:
        #    yield tmp_dir
        return f"{os.getcwd()}/../build"

    @fixture
    def root_cmakelists(self, working_dir: Path):
        current_dir: Path = Path(os.path.dirname(os.path.abspath(__file__)))

        shutil.copy2(current_dir / "cmakelists.py", working_dir)

    @fixture
    def settings(self, mixin, working_dir, router):
        raise NotImplementedError()

    @fixture
    def machine(self, config, context_class: Type[CMakeContext]):
        raise NotImplementedError()

    @fixture
    def transport(self, machine):
        yield machine.transport
        machine.transport.stop()

    @fixture
    def config(self, settings):
        return settings.config

    @fixture
    def sender(self, settings):
        return settings.sender

    @fixture
    def uid(self, settings):
        return settings.uid

    @fixture(autouse=True)
    def clean_files(self, working_dir: str):
        for key in [signal.mnemonic() for signal in SIGNAL_TYPES]:
            working_dir_path = Path(working_dir)
            lock_file_path = working_dir_path / f"{key}.lock"
            payload_file_path = working_dir_path / f"{key}.payload"
            if payload_file_path.exists():
                os.remove(payload_file_path)
            if lock_file_path.exists():
                os.remove(lock_file_path)
