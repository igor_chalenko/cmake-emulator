import concurrent.futures
import dataclasses
import traceback
from concurrent.futures import Executor, ThreadPoolExecutor
from typing import Any

from yaranga.server.transport import SignalTransport
from yaranga.server.vm import VirtualMachine


@dataclasses.dataclass
class ScenarioRunner(object):
    """
    Runs a given scenario.
    """

    scenario: Any
    transport: SignalTransport
    machine: VirtualMachine
    executor: Executor = ThreadPoolExecutor()

    def run(self, uid):
        # prepare the router
        try:
            context_fut = self.machine.add_application(uid)

            # start transport
            self.executor.submit(self.transport.start)
            self.transport.wait_for_start()

            # run scenario itself
            run = getattr(self.scenario, "run")
            fut = self.executor.submit(run, uid)

            concurrent.futures.wait([fut, context_fut], return_when="ALL_COMPLETED")
        except (ValueError, TypeError) as e:
            traceback.print_exc()
            breakpoint()
            raise e
