from tests.server.integration.mixins.settings import Settings
from tests.server.integration.filesender import FileSender
from yaranga.server.file import FileTransportConfig, FileTransport


class FileTransportMixin(object):
    @staticmethod
    def settings(working_dir):
        config = FileTransportConfig(working_dir)
        return Settings(
            config=config,
            # transport=FileTransport(config, router),
            sender=FileSender(config),
            uid=str(working_dir),
        )
