import uuid

from tests.server.integration.httpsender import HttpSender
from tests.server.integration.mixins.settings import Settings
from yaranga.server.http import HttpTransportConfig


class HttpTransportMixin(object):
    @staticmethod
    def settings(_):
        config = HttpTransportConfig("127.0.0.1", 12345)
        return Settings(
            config=config,
            # transport=HttpTransport(config, router),
            sender=HttpSender(config),
            uid=uuid.uuid1().hex,
        )
