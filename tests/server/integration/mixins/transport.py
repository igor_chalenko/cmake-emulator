import abc

import pytest

NOT_IMPLEMENTED = "Add a mixin that provides this fixture"


class TransportMixin(object, metaclass=abc.ABCMeta):
    @pytest.fixture
    @abc.abstractmethod
    def config(self):
        raise NotImplementedError(NOT_IMPLEMENTED)

    @pytest.fixture
    @abc.abstractmethod
    def transport(self, config, router):
        raise NotImplementedError(NOT_IMPLEMENTED)
