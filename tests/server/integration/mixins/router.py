import abc

import pytest

from yaranga.server.router import Router

NOT_IMPLEMENTED = "Add a mixin that provides this fixture"


class RouterMixin(object, metaclass=abc.ABCMeta):
    @pytest.fixture
    @abc.abstractmethod
    def router(self, router_class, working_dir) -> Router:
        raise NotImplementedError(NOT_IMPLEMENTED)
