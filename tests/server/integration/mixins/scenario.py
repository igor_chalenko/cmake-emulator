import pytest

from tests.server.integration.scenariorunner import ScenarioRunner
from yaranga.server.abstractrouter import Sender


class ScenarioMixin(object):
    @pytest.fixture
    def scenario(self, sender: Sender):
        raise NotImplementedError()

    @pytest.fixture
    def scenario_runner(self, transport, machine, scenario):
        return ScenarioRunner(machine=machine, scenario=scenario, transport=transport)
