import dataclasses

from yaranga.server.abstractrouter import Sender
from yaranga.server.transport import SignalTransportConfig


@dataclasses.dataclass
class Settings:
    config: SignalTransportConfig
    # transport: SignalTransport
    sender: Sender
    uid: str
