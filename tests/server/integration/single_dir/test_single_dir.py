from typing import Type

import pytest
from pytest_cases import parametrize, fixture

from tests.server.integration.integration import IntegrationTest
from tests.server.integration.mixins.file import FileTransportMixin
from tests.server.integration.mixins.http import HttpTransportMixin
from tests.server.integration.observable import ObservableVirtualMachine
from tests.server.integration.scenariorunner import ScenarioRunner
from tests.server.integration.single_dir.client import ClientScenario, FailingScenario
from tests.server.integration.single_dir.server import ServerScenario, FailingServerScenario
from yaranga.server.context import CMakeContext


@pytest.mark.integration_test
class TestSingleDir(IntegrationTest):
    @fixture
    @parametrize("context_class", [ServerScenario, CMakeContext])
    def machine(self, config, context_class: Type[CMakeContext] = CMakeContext):
        return ObservableVirtualMachine(config, context_class)

    @pytest.fixture
    def scenario(self, sender, working_dir):
        return ClientScenario(sender, working_dir)

    @fixture
    def transport(self, machine):
        yield machine.transport
        machine.transport.stop()

    @fixture
    @parametrize("mixin", [FileTransportMixin, HttpTransportMixin])
    def settings(self, mixin, working_dir):
        return mixin.settings(working_dir)

    def test(self, capsys, uid, root_cmakelists, scenario_runner: ScenarioRunner):
        with capsys.disabled():
            scenario_runner.run(uid)


@pytest.mark.integration_test
class TestFailingSingleDir(TestSingleDir):
    @fixture
    @parametrize("context_class", [FailingServerScenario, CMakeContext])
    def machine(self, config, context_class: Type[CMakeContext] = CMakeContext):
        return ObservableVirtualMachine(config, context_class)

    @pytest.fixture
    def scenario(self, sender, working_dir):
        return FailingScenario(sender, working_dir)


if __name__ == "__main__":
    pytest.main()
