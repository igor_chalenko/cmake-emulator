from pathlib import Path

from yaranga.server.abstractrouter import Sender, SenderReceiver
from yaranga.server.signals import (
    DirectoryProcessed,
    FlushResponse,
    Flush,
    Directory,
    Stop,
)


class ClientScenario:
    def __init__(self, sender: Sender, root_dir: Path):
        self.sender = sender
        self.root_dir = root_dir

    def run(self, uid: str):
        self.sender.send(
            Directory(uid, f'extract_directory(r"{self.root_dir}", r"{self.root_dir}")'),
            Flush(uid),
            FlushResponse(uid, '- set_global_property("CPM_INITIALIZED", "ON")'),
            DirectoryProcessed(uid),
            Stop(uid),
        )


class FailingScenario:
    def __init__(self, sender: SenderReceiver, root_dir: Path):
        self.sender = sender
        self.root_dir = root_dir

    def run(self, uid: str):
        self.sender.send(
            Directory(uid, f'extract_directory1(r"{self.root_dir}", r"{self.root_dir}")'))

        try:
            resp = self.sender.receive(DirectoryProcessed(uid))
            assert isinstance(resp, DirectoryProcessed)
            assert resp.payload().startswith("Traceback")

            self.sender.send(
                Stop(uid)
            )
        except Exception as e:
            breakpoint()