import os
from pathlib import Path
from typing import Type

import pytest
from pytest_cases import parametrize, fixture

from tests.server.integration.mixins.file import FileTransportMixin
from tests.server.integration.two_dirs.client import ClientScenario
from tests.server.integration.two_dirs.server import ServerScenario
from tests.server.integration.integration import IntegrationTest
from tests.server.integration.mixins.http import HttpTransportMixin
from tests.server.integration.observable import ObservableVirtualMachine
from tests.server.integration.scenariorunner import ScenarioRunner
from yaranga.server.context import CMakeContext


@pytest.mark.integration_test
class TestTwoDirs(IntegrationTest):
    @fixture
    @parametrize("context_class", [ServerScenario, CMakeContext])
    def machine(self, config, context_class: Type[CMakeContext] = CMakeContext):
        return ObservableVirtualMachine(config, context_class)

    @fixture
    def transport(self, machine):
        yield machine.transport
        machine.transport.stop()

    @pytest.fixture
    def scenario(self, sender, working_dir):
        return ClientScenario(sender, working_dir)

    @fixture
    def nested_dir(self, working_dir):
        path = Path(working_dir) / "x"
        if not os.path.exists(path):
            os.mkdir(path)
        return path

    @fixture
    def nested_cmakelists(self, nested_dir):
        with open(Path(nested_dir) / "cmakelists.py", "wt") as f:
            code = """
def run(cmake):
    var = cmake.globals['CPM_INITIALIZED_2']
    print(f"!!! var = {var} !!!")
    assert var == 'OFF'
"""
            f.write(code)

    @fixture
    @parametrize("mixin", [HttpTransportMixin, FileTransportMixin])
    def settings(self, mixin, working_dir):
        return mixin.settings(working_dir)

    def test(
        self,
        capsys,
        uid,
        root_cmakelists,
        nested_cmakelists,
        scenario_runner: ScenarioRunner,
    ):
        with capsys.disabled():
            scenario_runner.run(uid)


if __name__ == "__main__":
    pytest.main()
