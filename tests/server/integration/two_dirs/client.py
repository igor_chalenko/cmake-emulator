from pathlib import Path

from yaranga.server.abstractrouter import SenderReceiver
from yaranga.server.signals import (
    DirectoryProcessed,
    FlushResponse,
    Flush,
    Directory,
    Stop,
)


class ClientScenario:
    def __init__(self, sender: SenderReceiver, root_dir: Path):
        self.sender = sender
        self.root_dir = root_dir

    def run(self, uid: str):
        send = self.sender.send
        receive = self.sender.receive

        send(Directory(uid, f'extract_directory(r"{self.root_dir}", r"{self.root_dir}")'))
        receive(Flush(uid))
        send(FlushResponse(uid, '- set_global_property("CPM_INITIALIZED", "ON")'))
        receive(DirectoryProcessed(uid))

        send(Directory(uid, f'extract_directory("""x""", """x""")'))
        receive(Flush(uid))
        send(FlushResponse(uid, '- set_global_property("CPM_INITIALIZED_2", "OFF")'))
        receive(DirectoryProcessed(uid))
        send(Stop(uid))
