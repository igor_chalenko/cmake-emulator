import traceback
from pathlib import Path

from yaranga.server.abstractrouter import Sender
from yaranga.server.signals import (
    Directory,
    Flush,
    FlushResponse,
    DirectoryProcessed,
    Stop,
)


class ClientScenario(object):
    def __init__(self, sender: Sender, root_dir: Path):
        self.sender = sender
        self.root_dir = root_dir

    def run(self, uid: str):
        try:
            dir_payload = f'extract_directory(r"{self.root_dir}", r"{self.root_dir}")'
            self.sender.send(
                Directory(uid, dir_payload),
                Flush(uid),
                # emulate the error and restart here
                # Start(uid),
                Directory(uid, dir_payload),
                Flush(uid),
                FlushResponse(uid, '- set_global_property("CPM_INITIALIZED", "ON")'),
                DirectoryProcessed(uid),
                Stop(uid),
            )
        except:
            traceback.print_exc()
            breakpoint()