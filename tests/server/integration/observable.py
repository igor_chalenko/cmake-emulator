from threading import Lock
from typing import Type

from yaranga.server.context import CMakeContext
from yaranga.server.http import HttpTransportConfig
from yaranga.server.vm import VirtualMachine


class ObservableVirtualMachine(VirtualMachine):
    def __init__(
        self,
        config: HttpTransportConfig,
        context_class: Type[CMakeContext] = CMakeContext,
    ):
        super(ObservableVirtualMachine, self).__init__(config, context_class)

        self._remove_route = Lock()
        self._remove_route.acquire()

    def remove_application(self, uid: str):
        super(ObservableVirtualMachine, self).remove_application(uid)

        self._remove_route.release()

    def after_remove_application(self, callback):
        with self._remove_route:
            callback()
