import logging
import urllib.request
from typing import Optional
from urllib.parse import quote_plus

from yaranga.server.abstractrouter import SenderReceiver
from yaranga.server.http import HttpTransportConfig, RequestHandlerBase
from yaranga.server.signals import Signal, ExternalSignal, ApplicationSignal

logger = logging.getLogger(__name__)


class RequestHandlerHelper(RequestHandlerBase):
    def __init__(self, uri: str):
        super(RequestHandlerHelper, self).__init__()

        self._uri = uri

    def poll(self, uid: str) -> Signal:
        pass

    def uri(self) -> str:
        return self._uri

    def ok(self, body: bytes):
        pass

    def not_found(self):
        pass


class HttpSender(SenderReceiver):
    def __init__(self, config: HttpTransportConfig):
        self.config = config

    def send(self, *signals: Signal):
        address = f"http://{self.config.host}:{self.config.port}"

        def get(signal: Signal):
            uri = signal.mnemonic()
            if signal.params:
                params = [f"{k}={v}" for k, v in signal.params.items()]
                uri += "?"
                uri += "&".join(["=".join(params)])
            headers = {"Cookie": signal.uid}
            req = urllib.request.Request(
                f"{address}/{uri}",
                method="GET",
                headers=headers,
            )
            with urllib.request.urlopen(req) as response:
                return response.read()

        def put(signal: Signal):
            uri = signal.mnemonic()
            if signal.params:
                params = [f"{k}={v}" for k, v in signal.params.items()]
                uri += "?"
                uri += "&".join(["=".join(params)])

            uri = quote_plus(uri)
            payload: Optional[str] = signal.payload()

            payload_bytes = payload.encode("utf-8") if payload else bytes()
            headers = {
                "Content-Type": "text/yaml",
                "Content-Length": str(len(payload_bytes)),
                "Cookie": signal.uid,
            }
            req = urllib.request.Request(
                f"{address}/{uri}",
                method="PUT",
                data=payload_bytes,
                headers=headers,
            )
            with urllib.request.urlopen(req) as response:
                return response.read()

        def _send(signal: Signal):
            signal_type = type(signal)
            if issubclass(signal_type, ApplicationSignal):
                if signal.payload():
                    return put(signal)
                else:
                    return get(signal)
            elif issubclass(signal_type, ExternalSignal):
                return put(signal)

        res = None
        for _ in signals:
            res = _send(_)
        return res

    def receive(self, signal: Signal):
        payload = self.send(signal).decode("utf-8")
        return type(signal)(signal.uid, payload, {"location": "unknown"})
