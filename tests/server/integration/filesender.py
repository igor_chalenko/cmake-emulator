import os
import time
from pathlib import Path
from typing import Sequence

from yaranga.server.abstractrouter import SenderReceiver
from yaranga.server.file import FileTransportConfig
from yaranga.server.signals import Signal
from yaranga.types import Values


class FileSender(SenderReceiver):
    def __init__(self, config: FileTransportConfig):
        self.working_dir = Path(config.working_dir)

    def send(self, *signals: Values[Signal]):
        def _send(_signal: Signal):
            lock_file_path = self.working_dir / f"{_signal.mnemonic()}.lock"
            if _signal.payload():
                payload_file_path = self.working_dir / f"{_signal.mnemonic()}.payload"
            else:
                payload_file_path = None
            if payload_file_path:
                with open(payload_file_path, "wt") as f:
                    if isinstance(signal.payload(), Sequence):
                        f.writelines([str(cmd) for cmd in signal.payload()])
                    else:
                        f.write(_signal.payload())
            lock_file_path.touch()

        for signal in signals:
            _send(signal)

    def receive(self, signal: Signal):
        key = signal.mnemonic()
        lock_file_path = self.working_dir / f"{key}.lock"
        payload_file_path = self.working_dir / f"{key}.payload"

        # todo duplicates the server part
        while not lock_file_path.exists():
            time.sleep(1)
        if payload_file_path.exists():
            with open(payload_file_path, "rt") as f:
                payload = f.read()
            os.remove(payload_file_path)
        else:
            payload = None
        os.remove(lock_file_path)
        return type(signal)(signal.uid, payload)
