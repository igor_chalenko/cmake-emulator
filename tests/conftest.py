import os.path
from pathlib import Path

import pytest


@pytest.fixture
def resource_path() -> Path:
    test_dir = Path(os.path.dirname(os.path.abspath(__file__)))
    return test_dir / "resources"
