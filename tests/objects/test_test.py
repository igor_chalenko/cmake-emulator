import pytest

from yaranga.model.exceptions import ValidationError
from yaranga.objects.test import Test


class TestCMakeTest:
    @pytest.fixture
    def test_object(self) -> Test:
        return Test(source_dir="./", name="a", command="echo test")

    def test_update_properties(self, test_object: Test):
        test_object.cost = "33"
        test_object["a"] = "b"
        assert "b" == test_object["a"]
        assert 33 == test_object.cost

    def test_set_environment(self, test_object: Test):
        test_object.environment = "a=b;c=d"
        # some strange checker bug - may need to remove later
        # noinspection PyTypeChecker, PydanticTypeChecker
        assert test_object.environment["a"] == "b"
        # noinspection PyTypeChecker, PydanticTypeChecker
        assert test_object.environment["c"] == "d"

    def test_validation(self, test_object):
        test_object["SKIP_RETURN_CODE"] = 1  # ok

        with pytest.raises(ValidationError):
            test_object["SKIP_RETURN_CODE"] = 1000  # not ok


if __name__ == "__main__":
    pytest.main()
