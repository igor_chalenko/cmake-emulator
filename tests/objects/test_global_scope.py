import pytest
import yaml

from yaranga.objects.global_scope import GlobalScope


class TestGlobalScope:
    def test_create(self):
        scope = GlobalScope(cmake_role="SCRIPT", generator_is_multi_config=False)

        scope["CPM_DOWNLOAD_ALL"] = True
        assert not scope.generator_is_multi_config

    def test_load_from_file(self):
        to_load = """
globals:
    a: b
    c: d        
        """

        obj = yaml.safe_load(to_load)
        global_scope = GlobalScope()

        for k, v in obj["globals"].items():
            global_scope[k] = v
        assert "b" == global_scope["a"]
        global_scope.cmake_role = "PROJECT"


if __name__ == "__main__":
    pytest.main()
