from pathlib import Path

import pytest

from yaranga.model.exceptions import ValidationError, NotWritableAttributeError
from yaranga.objects.directory import Directory
from yaranga.objects.target import Target, ImportedLibrary, Executable
from yaranga.objects.target_type import TargetType, ImportedTargetType


class TestDirectory:
    APPEND_PATH = "foo.cc"

    @pytest.fixture
    def directory(self, resource_path):
        return Directory(source_dir=resource_path / "build", binary_dir=resource_path)

    def test_construct(self, resource_path):
        directory_1 = Directory(source_dir=resource_path, binary_dir=resource_path)
        assert (
            isinstance(directory_1.source_dir, Path)
            and directory_1.source_dir.is_absolute()
        )
        assert (
            isinstance(directory_1.binary_dir, Path)
            and directory_1.binary_dir.is_absolute()
        )
        assert not directory_1.tests

        Executable(
            "target", source_dir=directory_1, sources=[TestDirectory.APPEND_PATH]
        )

        Path(directory_1.source_dir, TestDirectory.APPEND_PATH).resolve(strict=True)
        assert TestDirectory.APPEND_PATH in directory_1.sources
        src = directory_1.get_source_file(TestDirectory.APPEND_PATH)
        assert TestDirectory.APPEND_PATH == src.location

        directory_1["MACROS"] = ["macro1", "macro2"]
        assert ["macro1", "macro2"] == directory_1.macros

    def test_add_tests(self, directory: Directory):
        assert 0 == len(directory.tests)
        # add a test
        directory.add_test(name="test", command="echo Test")
        # find it
        assert 1 == len(directory.tests)
        assert directory.tests[0] == "test"

    def test_read_only_properties(self, directory):
        with pytest.raises(NotWritableAttributeError):
            directory.sources = []
        with pytest.raises(NotWritableAttributeError):
            directory.macros = []
        with pytest.raises(NotWritableAttributeError):
            directory.buildsystem_targets = []
        with pytest.raises(NotWritableAttributeError):
            directory.cache_variables = []
        with pytest.raises(NotWritableAttributeError):
            directory.imported_targets = []
        with pytest.raises(NotWritableAttributeError):
            directory.parent_directory = None
        with pytest.raises(NotWritableAttributeError):
            directory.tests = []
        with pytest.raises(NotWritableAttributeError):
            directory.variables = []

    def test_include_directories(self, resource_path, directory):
        include = "../include"

        p = Path(resource_path, include)
        directory.include_directories = p
        assert directory.include_directories == [p]

        directory.include_directories = include
        assert directory.include_directories == [include]

    def test_link_directories(self, resource_path):
        directory = Directory(source_dir=resource_path, binary_dir=resource_path)

        directory.link_directories = resource_path
        assert [resource_path] == directory.link_directories

        directory.include_directories = resource_path
        assert [resource_path] == directory.include_directories

        subdir = resource_path / "build"
        Directory(source_dir=subdir, binary_dir=subdir, parent=directory)
        assert [subdir] == [x for x in directory.subdirectories]

    def test_parent_directory(self, resource_path):
        root = Directory(source_dir=resource_path, binary_dir=resource_path)
        directory = Directory(
            source_dir=resource_path / "../resources",
            binary_dir=resource_path,
            parent=root,
        )
        assert root.source_dir == directory.parent_directory

    def test_validate_path(self):
        with pytest.raises(ValidationError):
            Directory(source_dir=Path("../model/"), binary_dir=Path("../model/"))

    def test_compile_definitions(self, resource_path):
        root = Directory(source_dir=resource_path, binary_dir=resource_path)
        root.compile_definitions = ["a", "b"]
        root.implicit_depends_include_transform = ["a"]
        directory = Directory(
            source_dir=resource_path / "build",
            binary_dir=resource_path,
            parent=root,
        )
        assert root.compile_definitions == directory.compile_definitions
        assert (
            root.implicit_depends_include_transform
            == directory.implicit_depends_include_transform
        )

    def test_add_imported_target(self, directory):
        # create an imported target
        target = ImportedLibrary(
            name="test_target",
            imported_target_type=ImportedTargetType.STATIC,
            source_dir=directory,
        )

        directory.imported_targets.append(target.name)
        assert 1 == len(directory.imported_targets)

    def test_add_target(self, directory):
        target = Target(
            name="test_target", type_=TargetType.EXECUTABLE, source_dir=directory
        )
        directory.buildsystem_targets.append(target.name)
        assert 1 == len(directory.buildsystem_targets)


if __name__ == "__main__":
    pytest.main()
