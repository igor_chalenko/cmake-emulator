import pytest

from yaranga.model.exceptions import ValidationError
from yaranga.objects.cache_variable import CacheVariable, CacheVariableType


class TestCacheVariable:
    def test_create_update(self):
        # create a cache variable
        var = CacheVariable(
            name="abc", typ=CacheVariableType.PATH, value="def", helpstring="help"
        )

        # and check its properties
        assert var.name == var["NAME"]
        assert var.type == CacheVariableType.PATH

        # update type
        var["TYPE"] = "FILEPATH"
        assert CacheVariableType.FILEPATH == var.type

    def test_empty_name(self):
        # name is `NOTFOUND`
        with pytest.raises(ValidationError):
            CacheVariable(
                name="NOTFOUND",
                typ=CacheVariableType.STRING,
                value="def",
                helpstring="help",
            )
        # name is `None`
        with pytest.raises(ValidationError):
            # noinspection PyTypeChecker,PydanticTypeChecker
            CacheVariable(
                name=None, typ=CacheVariableType.STRING, value="def", helpstring="help"
            )


if __name__ == "__main__":
    pytest.main()
