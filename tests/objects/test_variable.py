import pytest

from yaranga.objects.variables import Variables


class TestVariables:
    def test_variables(self):
        variables = Variables()
        # create a variable
        variables["a"] = "b"

        # read it back
        assert "b" == variables["a"]


if __name__ == "__main__":
    pytest.main()
