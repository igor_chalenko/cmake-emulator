import pytest

from yaranga.model.exceptions import ValidationError
from yaranga.objects.source_file import SourceFile


class TestSourceFile:
    NAME = "foo.cc"

    def test_compare_normal(self):
        source = SourceFile(location="a")
        assert source.location == "a"
        # custom property
        source["a"] = "b"
        assert "b" == source["a"]

    def test_compare_with_gen_expressions(self):
        vs_xaml_type = "xaml"

        source_1 = SourceFile(location="$<1:foo.cc>", abstract=True)
        assert source_1.abstract

        source_1["ABSTRACT"] = "no"
        assert not source_1.abstract

        source_1.vs_xaml_type = vs_xaml_type
        assert vs_xaml_type == source_1.vs_xaml_type

        source_2 = SourceFile(location=f"$<1:../model/{TestSourceFile.NAME}>")
        assert source_1 != source_2

    def test_generated(self, resource_path):
        location_1 = resource_path / TestSourceFile.NAME
        source_1 = SourceFile(location=str(location_1), generated=True)

        with pytest.raises(ValidationError):
            source_1.generated = False


if __name__ == "__main__":
    pytest.main()
