from pathlib import Path

import pytest

from yaranga.model.exceptions import NotWritableAttributeError
from yaranga.objects.directory import Directory
from yaranga.objects.source_file import SourceFile
from yaranga.objects.target import (
    Target,
    Executable,
    ImportedLibrary,
    AliasTarget,
    Library,
)
from yaranga.objects.target_type import TargetType, ImportedTargetType
from yaranga.undefined import Undefined


class BaseTargetTest:
    @pytest.fixture
    def source_directory(self, resource_path):
        return Directory(source_dir=resource_path, binary_dir=resource_path)


class TestTarget(BaseTargetTest):
    SOURCE_FILE_1 = "example.cc"
    SOURCE_FILE_2 = "foo.cc"

    @pytest.fixture
    def source_path(self, source_directory) -> str:
        path = Path(source_directory.source_dir, TestTarget.SOURCE_FILE_1).resolve(
            strict=True
        )
        return str(path)

    @pytest.fixture
    def target(self, source_directory):
        return Target(
            name="test", type_=TargetType.EXECUTABLE, source_dir=source_directory
        )

    def test_target_to_string(self, target):
        target_as_str: str = str(target)

        assert "'imported': Undefined" in target_as_str
        assert "'name': 'test'" in target_as_str

    def test_target_properties(self, target, source_directory):
        assert "test" == target.name
        assert TargetType.EXECUTABLE == target.type
        assert target["bogus"] is Undefined

    def test_set_custom_property(self, target):
        # set a custom property
        target["CUSTOM_PROPERTY"] = TestTarget.SOURCE_FILE_2
        # read it back
        assert TestTarget.SOURCE_FILE_2 == target["CUSTOM_PROPERTY"]

    def test_set_compile_options(self, target, source_directory, source_path):
        target.set_sources(
            sources=[TestTarget.SOURCE_FILE_1], properties={"COMPILE_OPTIONS": "-O1"}
        )
        src: SourceFile = source_directory.get_source_file(TestTarget.SOURCE_FILE_1)
        assert TestTarget.SOURCE_FILE_1 == src.location
        assert TestTarget.SOURCE_FILE_1 in source_directory.sources
        assert ["-O1"] == src.compile_options

    def test_ref_counting(self, target, source_directory, source_path):
        # set target sources
        target.set_sources(sources=TestTarget.SOURCE_FILE_2)
        assert TestTarget.SOURCE_FILE_2 in target.sources

        # create a target with the same sources
        target_2 = Target(
            name="test_2", type_=TargetType.EXECUTABLE, source_dir=source_directory
        )
        target_2.set_sources(sources=TestTarget.SOURCE_FILE_2)

        target.set_sources(sources=[])
        # target_2 still references the file
        assert 1 == len(source_directory._sources)
        assert 0 == len(target.sources)


class TestExecutable(BaseTargetTest):
    def test_create(self, source_directory):
        # create an executable
        target = Executable(
            name="doxygen",
            source_dir=source_directory,
            sources=[TestTarget.SOURCE_FILE_1],
            win32_executable=True,
            exclude_from_all=True,
        )

        # check properties
        assert target.win32_executable
        assert target["EXCLUDE_FROM_ALL"] and target.exclude_from_all
        assert [TestTarget.SOURCE_FILE_1] == target.sources


class TestImportedExecutable(BaseTargetTest):
    def test_create(self, source_directory):
        # create an imported executable
        target = ImportedLibrary(
            name="doxygen",
            imported_target_type=ImportedTargetType.SHARED,
            source_dir=source_directory,
        )

        # check properties
        assert target["IMPORTED"] and target.imported
        assert not target.is_global

        # update global flag and re-check
        target["GLOBAL"] = True
        assert target.is_global and target["GLOBAL"]


class TestAliasExecutable(BaseTargetTest):
    def test_imported_target_alias(self, source_directory):
        # create an imported executable with
        imported_target = ImportedLibrary(
            name="doxygen",
            imported_target_type=ImportedTargetType.SHARED,
            source_dir=source_directory,
            imported_global=True,
        )
        # create an alias target
        target = AliasTarget(
            name="Doxygen::doxygen",
            source_dir=source_directory,
            target=imported_target,
        )

        # verify properties
        assert target.type == TargetType.ALIAS
        assert target.alias_global

    def test_target_alias(self, source_directory):
        # create an executable
        executable = Executable(
            name="doxypress",
            source_dir=source_directory,
            sources=[TestTarget.SOURCE_FILE_1],
        )

        # create an alias target
        target = AliasTarget(
            name="Doxypress::doxypress",
            source_dir=source_directory,
            target=executable,
        )

        # verify properties
        assert target.type == TargetType.ALIAS
        assert not target.alias_global


class TestLibrary(BaseTargetTest):
    def test_create(self, source_directory):
        # create a library
        target = Library(
            name="fmt",
            type_=TargetType.STATIC_LIBRARY,
            source_dir=source_directory,
            sources=TestTarget.SOURCE_FILE_1,
            exclude_from_all=True,
        )

        # check properties
        assert TargetType.STATIC_LIBRARY == target.type
        assert not target.framework
        assert target["EXCLUDE_FROM_ALL"] and target.exclude_from_all
        assert [TestTarget.SOURCE_FILE_1] == target.sources

        # attempt to overwrite type
        with pytest.raises(NotWritableAttributeError):
            target.type = TargetType.EXECUTABLE


class TestImportedLibrary(BaseTargetTest):
    def test_create(self, source_directory):
        target = ImportedLibrary(
            name="fmt",
            imported_target_type=ImportedTargetType.UNKNOWN,
            source_dir=source_directory,
            imported_global=True,
        )

        assert target.type == ImportedTargetType.UNKNOWN
        assert target.imported
        assert not target.sources
        assert target.imported_global


if __name__ == "__main__":
    pytest.main()
