from pathlib import Path

import pytest

from yaranga.objects.directory import Directory
from yaranga.util.path_dict import PathDict


class TestPathDict:
    def test_insert(self, resource_path):
        directory_1 = Directory(
            source_dir=resource_path / "../resources/build", binary_dir=resource_path
        )
        directory_2 = Directory(
            source_dir=resource_path / "build", binary_dir=resource_path
        )

        dct = PathDict(resource_path)
        dct[directory_1.source_dir] = 1
        dct[resource_path] = 2
        assert 1 == dct[directory_2.source_dir]


if __name__ == "__main__":
    pytest.main()
