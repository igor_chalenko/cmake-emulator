import logging.config
import os
from pathlib import Path

from sphinx.util import colorize
from sphinx.util.logging import ColorizeFormatter, COLOR_MAP

from yaranga.util.yaml_dict import yaml_dict


old_factory = logging.getLogRecordFactory()


def spell_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    if record.levelname == "INFO":
        setattr(record, "color", "green")
    elif record.levelname == "ERROR":
        setattr(record, "color", "white")
    elif record.levelname == "WARNING":
        setattr(record, "color", "blue")
    elif record.levelname == "DEBUG":
        setattr(record, "color", "gray")

    return record


class CustomColorFormatter(ColorizeFormatter):
    def format(self, record: logging.LogRecord) -> str:
        if record.msg == "\n":
            return "\n"

        color = getattr(record, "color", None)
        if color is None:
            color = COLOR_MAP.get(record.levelno)

        if color:
            colored_level = colorize(color, record.levelname)
            record.levelname = colored_level
        return logging.Formatter.format(self, record)


logging.setLogRecordFactory(spell_record_factory)
current_path = Path(__file__).parent.resolve()
os.chdir(current_path)
logging.config.dictConfig(yaml_dict(Path(f"{current_path}/resources/logging.yaml")))
