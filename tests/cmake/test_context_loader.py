from importlib.resources import open_text

import pytest

from tests import resources
from tests.cmake.fixtures import TranslatorTest
from yaranga.cmake.context_loader import ContextLoader
from yaranga.cmake.injector import Injector


class TestContextLoader(TranslatorTest):
    def test_load_from_file(self, translator, resource_path):
        def load(filename):
            loader = ContextLoader(translator.sm)
            with open_text(resources, filename) as resource:
                loader.load(resource)

        with Injector(translator):
            # load state from a file
            load("sample.yaml")

            # verify loaded state
            assert "B" == translator.cache["A"]["VALUE"]

            assert 2 == len(translator.directories)

            directory = translator.directories[resource_path / "empty"]
            assert len(directory.buildsystem_targets) == 1
            assert len(directory.sources) == 2

            source = directory.get_source_file("src/file1.cc")
            assert "B" == source["A"]
            assert "D" == source["C"]

            directory = translator.directories[resource_path]
            assert len(directory.buildsystem_targets) == 5
            assert len(directory.imported_targets) == 2


if __name__ == "__main__":
    pytest.main()
