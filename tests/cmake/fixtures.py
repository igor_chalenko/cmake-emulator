import pytest

from yaranga.cmake.translator import Translator


class TranslatorTest:
    @pytest.fixture
    def translator(self, resource_path) -> Translator:
        translator = Translator(resource_path, resource_path)
        translator["CMAKE_SOURCE_DIR"] = str(resource_path)

        return translator

    @pytest.fixture
    def commands(self, translator):
        return translator.command_tray.get_commands()
