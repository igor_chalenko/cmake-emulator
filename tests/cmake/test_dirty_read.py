from tests.cmake.fixtures import TranslatorTest
from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator
from yaranga.commands.get import (
    GetTargetProperty,
    GetCacheVariableProperty,
    GetTestProperty,
    GetDirectoryProperty,
    GetSourceFileProperty,
    GetGlobalProperty,
)
from yaranga.objects.cache_variable import CacheVariableType
from yaranga.objects.target import Executable
from yaranga.undefined import Undefined

import pytest


class TestDirtyRead(TranslatorTest):
    def test_dirty_read_global(self, commands, translator: Translator):
        with Injector(translator):
            translator.get_global_property("IN_TRY_COMPILE")
            _ = translator.sm.globals.in_try_compile
            assert len(commands) == 3
            assert str(commands[1]) == str(GetGlobalProperty("IN_TRY_COMPILE"))
            assert str(commands[2]) == str(commands[1])

    def test_dirty_read_variable(self, commands, translator: Translator):
        with Injector(translator):
            assert translator["non_existent"] is Undefined
            assert len(commands) == 2
            assert str(commands[1]) == "narta_get_variable(non_existent)"

    def test_dirty_read_cache_variable(self, commands, translator: Translator):
        with Injector(translator):
            translator.set_cache_variable("A", "B", CacheVariableType.INTERNAL)
            assert translator.sm.cache_variables["A"]["PROPERTY"] is Undefined
            assert len(commands) == 3
            assert str(commands[-1]) == str(GetCacheVariableProperty("A", "PROPERTY"))

    def test_dirty_read_target(self, commands, translator: Translator):
        with Injector(translator):
            test: Executable = translator.add_executable("test")
            assert test.android_api is Undefined
            assert len(commands) == 4
            assert str(commands[-1]) == str(GetTargetProperty("test", "ANDROID_API"))

    def test_dirty_read_test(self, commands, translator: Translator):
        translator.add_test("test")

        with Injector(translator):
            assert translator.tests["test"]["PROP"] is Undefined
            assert len(commands) == 2
            expected_cmd = GetTestProperty(
                translator.sm.current_dir.source_dir, "test", "PROP"
            )
            assert str(commands[1]) == str(expected_cmd)

    def test_dirty_read_directory(
        self, resource_path, commands, translator: Translator
    ):
        with Injector(translator):
            assert translator.sm.directories[resource_path]["PROP"] is Undefined
            assert len(commands) == 2
            assert str(commands[1]) == str(GetDirectoryProperty(resource_path, "PROP"))

    def test_dirty_read_source(self, commands, translator: Translator):
        translator.sm.current_dir.set_source_property("foo.cc", "PROP", "VALUE")
        with Injector(translator):
            assert translator.sm.current_dir._sources["foo.cc"]["PROP2"] is Undefined
            assert len(commands) == 2
            assert str(commands[1]) == str(
                GetSourceFileProperty(translator.current_source_dir, "foo.cc", "PROP2")
            )

    def test_mark_as_dirty(self, translator):
        translator.set_global_property("AAA", "BBB")
        translator.set_cache_variable("AAA", "BBB")
        translator.add_test("test_1", prop="value")
        translator.add_interface_library("target_1")
        translator.sm.mark_as_dirty()

        assert translator.sm.globals["AAA"] is Undefined
        assert translator.sm.globals["IN_TRY_COMPILE"] is Undefined
        assert translator.sm.cache_variables["AAA"] is Undefined
        assert translator.targets["target_1"]["NAME"] == "target_1"

        assert translator.sm.current_dir.tests == ["test_1"]


if __name__ == "__main__":
    pytest.main()
