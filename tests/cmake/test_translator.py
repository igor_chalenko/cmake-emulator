import pytest

from tests.cmake.fixtures import TranslatorTest
from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator
from yaranga.objects.cache_variable import CacheVariableType, CacheVariable
from yaranga.objects.target import Executable, AliasTarget
from yaranga.objects.target_type import ImportedTargetType


class TestTranslatorAPI(TranslatorTest):
    def test_function(self, translator):
        function = translator.function("fn", 1, 2, 3)
        assert str(function) == str(translator.command_tray.get_commands()[-1])

    def test_global_property(self, translator: Translator):
        translator.set_global_property("a", "b")
        assert "b" == translator.sm.globals["a"]

    def test_set_variable(self, translator: Translator):
        translator["a"] = "b"
        assert "b" == translator["a"]

    def test_set_cache_variable(self, translator: Translator):
        translator.set_cache_variable("a", "b", CacheVariableType.STATIC, "help")
        assert "b" == translator.sm.cache_variables["a"].value
        translator.set_cache_variable_property("a", "HELPSTRING", "new help")
        cache_var: CacheVariable = translator.sm.cache_variables["a"]
        assert "new help" == cache_var.helpstring

    def test_project(self, translator):
        translator.project(
            name="test_project",
            version="1.0",
            homepage_url="http://url",
            description="desc",
            languages=["CXX"],
        )
        assert "test_project" == translator["PROJECT_NAME"]

    def test_add_executable(self, translator, resource_path):
        translator.add_directory(resource_path, resource_path)

        translator["CMAKE_SOURCE_DIR"] = resource_path
        executable: Executable = translator.add_executable(
            "new_target", sources="src/foo.cc"
        )
        assert (
            resource_path == translator.sm.targets["new_target"].source_dir.source_dir
        )
        assert not executable.win32_executable and not executable["WIN32_EXECUTABLE"]

    def test_add_library(self, translator, resource_path):
        translator.add_directory(resource_path, resource_path)
        translator.set_current_directory(resource_path)

        translator["CMAKE_SOURCE_DIR"] = resource_path
        library = translator.add_static_library(
            "new_target", exclude_from_all=False, sources=["src/foo.cc"]
        )
        assert (
            resource_path == translator.sm.targets["new_target"].source_dir.source_dir
        )
        assert library.sources == ["src/foo.cc"]

    def test_add_alias(self, translator, resource_path):
        translator.add_directory(resource_path, resource_path)
        translator["CMAKE_SOURCE_DIR"] = resource_path

        translator.add_static_library(
            "new_target", exclude_from_all=False, sources=["src/foo.cc"]
        )
        with Injector(translator):
            alias: AliasTarget = translator.add_alias_target("alias", "new_target")
            assert alias.aliased_target == "new_target"

    def test_add_imported_library(self, translator, resource_path):
        translator.add_directory(resource_path, resource_path)
        translator["CMAKE_SOURCE_DIR"] = resource_path

        imported = translator.add_imported_library(
            "new_target", ImportedTargetType.STATIC
        )
        assert imported.type == ImportedTargetType.STATIC


if __name__ == "__main__":
    pytest.main()
