import pytest

from yaranga.cmake.version import Version


class TestVersion:
    def test_full_version(self):
        v = Version("v0.1.2.3")
        assert "v0" == v.major
        assert "1" == v.minor
        assert "2" == v.patch
        assert "3" == v.tweak
        assert str(v) == "v0.1.2.3"

        v = Version("3.22.0-rc2")
        assert "3" == v.major
        assert "22" == v.minor
        assert "0-rc2" == v.patch
        assert v.tweak is None

    def test_patch_version(self):
        v = Version("0.1.2")
        assert "0" == v.major
        assert "1" == v.minor
        assert "2" == v.patch
        assert v.tweak is None

        v = Version("boost-1.77.0")
        assert "boost-1" == v.major
        assert "77" == v.minor
        assert "0" == v.patch
        assert v.tweak is None

        assert str(v) == "boost-1.77.0"

    def test_minor_version(self):
        v = Version("0.1")
        assert "0" == v.major
        assert "1" == v.minor
        assert v.patch is None
        assert v.tweak is None

    def test_major_version(self):
        v = Version("version")
        assert "version" == v.major
        assert v.minor is None
        assert v.patch is None
        assert v.tweak is None


if __name__ == "__main__":
    pytest.main()
