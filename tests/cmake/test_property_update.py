import pytest

from tests.cmake.fixtures import TranslatorTest
from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator
from yaranga.commands.setproperty import SetTestProperty, SetDirectoryProperty
from yaranga.objects.cache_variable import CacheVariableType


class TestPropertyUpdate(TranslatorTest):
    def test_update_variable(self, commands, translator: Translator):
        with Injector(translator):
            translator["non_existent_variable"] = "x"
            assert len(commands) == 2
            assert str(commands[1]) == 'set(non_existent_variable "x")'

    def test_update_cache_variable(self, commands, translator: Translator):
        with Injector(translator):
            translator.set_cache_variable(
                "non_existent_variable", "x", CacheVariableType.INTERNAL, "help string"
            )
            assert len(commands) == 2
            assert (
                str(commands[1])
                == 'set(non_existent_variable x CACHE INTERNAL "help string")'
            )

    def test_update_global_property(self, commands, translator: Translator):
        with Injector(translator):
            translator.set_global_property("GLOBAL_PROPERTY", "local value")
            assert len(commands) == 2
            assert (
                str(commands[1])
                == 'set_property(GLOBAL PROPERTY GLOBAL_PROPERTY "local value")'
            )

    def test_update_target_property(self, commands, translator: Translator):
        with Injector(translator):
            translator.add_executable("test")
            translator.sm.targets["test"].android_api = 1
            assert len(commands) == 4
            assert (
                str(commands[-1]) == "set_property(TARGET test PROPERTY ANDROID_API 1)"
            )

            commands.clear()
            translator.add_interface_library("test2")
            translator.sm.targets["test2"].android_api = 1
            assert len(commands) == 2
            assert (
                str(commands[-1]) == "set_property(TARGET test2 PROPERTY ANDROID_API 1)"
            )

    def test_update_test_property(self, commands, translator: Translator):
        with Injector(translator):
            translator.add_test("test")
            translator.tests["test"]["PROP"] = "x"
            assert len(commands) == 3
            assert commands[-1] == SetTestProperty(
                translator.tests["test"], "PROP", "x"
            )

    def test_update_directory_property(self, commands, translator: Translator):
        with Injector(translator):
            translator.sm.current_dir["PROPERTY"] = "x"
            assert len(commands) == 2
            assert commands[1] == SetDirectoryProperty(
                translator.sm.current_dir, "PROPERTY", "x"
            )

    def test_update_source_property(self, commands, translator: Translator):
        with Injector(translator):
            translator.set_source_property("foo.cc", "PROP", "x")
            assert len(commands) == 2
            assert str(commands[-1]) == f'set_property(SOURCE "foo.cc" PROPERTY PROP x)'


if __name__ == "__main__":
    pytest.main()
