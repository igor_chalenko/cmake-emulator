import pytest

from yaranga.cmake.translator import Translator


class TestSetCurrentDirectory:
    @pytest.fixture
    def context(self, resource_path):
        return Translator()

    def test_set_current_directory(self, context, resource_path):
        assert context.sm.current_dir is None

        context.add_directory(resource_path, resource_path)
        assert context.current_source_dir == resource_path
