import pytest as pytest

from yaranga.model.basemodel import BaseModel
from yaranga.model.field import Field
from yaranga.model.model import ModelMetaclass
from yaranga.model.objectmodel import ObjectModel


class TestModelFieldFactory:
    def test_model_factory(self):
        class TestModel:
            a: int = Field(read_only=True)
            b: str = Field(default="abc", const=True, nullable=False)

        class TestModelChild(TestModel):
            c: str = "abc"

        # in the absence of the metaclass, need to do this by hand
        namespace = {**TestModel.__dict__, "__qualname__": "TestModel"}
        model = ObjectModel(namespace, TestModel)

        assert "a" in model

        model_field = model["a"]
        assert model_field.read_only
        assert not model_field.const
        assert model_field.nullable

        model_field = model["b"]
        assert not model_field.read_only
        assert model_field.const
        assert not model_field.nullable

        namespace = {**TestModelChild.__dict__, "__qualname__": "TestModelChild"}
        model = ObjectModel(namespace, TestModelChild)

        # no inherited fields here - collect_fields doesn't handle that
        assert "a" not in model
        assert "b" not in model
        model_field = model["c"]
        assert model_field.default == "abc"

    def test_model_metaclass(self):
        class TestModel(BaseModel, metaclass=ModelMetaclass, fom=ObjectModel):
            a: int = Field(read_only=True)
            b: str = "abc"

            def __init__(self, **properties):
                super(TestModel, self).__init__(**properties)
                for k, v in properties.items():
                    setattr(self, k, v)

        a = TestModel(a=1)
        assert a.a == 1
        assert isinstance(a.__class__.fields, ObjectModel)


if __name__ == "__main__":
    pytest.main()
