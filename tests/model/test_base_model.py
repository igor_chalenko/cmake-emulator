from typing import Optional, List, Union

import pytest

from yaranga.model.reflection import is_union
from yaranga.model.basemodel import BaseModel
from yaranga.model.field import Field
from yaranga.model.model import ObjectModel
from yaranga.model.objectmodel import FieldFactory
from yaranga.objects.cmakeobject import CMakeObject


class TestObjectModel:
    def test_create(self):
        class TestModel:
            f1: int = Field(read_only=True, alias="x")
            f2: str = Field(read_only=False, alias="y")

        # emulate metaclass interface
        namespace = {**TestModel.__dict__, "__qualname__": "TestModel"}
        # create an instance of object model
        model = ObjectModel(namespace, TestModel)

        # check fields
        assert model["f1"].read_only
        assert model.get_by_alias("x").read_only
        assert not model["f2"].read_only
        assert not model.get_by_alias("y").read_only

    def test_field_generator(self):
        class TestModel:
            a: Optional[List[str]] = Field(read_only=True)
            b: Union[str, List[str]]

        namespace = {**TestModel.__dict__, "__qualname__": "x"}
        gen = FieldFactory(namespace)()

        f = next(gen)
        t = f.type
        assert f.name == "a"
        assert t.type is list
        assert t.subtypes[0].type == str

        f = next(gen)
        t = f.type
        assert f.name == "b"
        assert is_union(t.type)
        assert t.subtypes[0].type == str
        assert t.subtypes[1].type == list
        assert t.subtypes[1].subtypes[0].type == str

    def test_validate_and_transform(self):
        class TestModel(BaseModel):
            a: int = Field(default=41)
            b: str = "abc"

        m = TestModel()
        assert (41, 42) == TestModel.fields.convert(42, m, "a")


class TestMergeProperty:
    VALUE = ["value1", "value2"]

    def test_merge_empty(self):
        obj = CMakeObject()
        obj.merge_property("PROP", TestMergeProperty.VALUE)
        assert obj["PROP"] == TestMergeProperty.VALUE

    def test_merge_non_empty(self):
        obj = CMakeObject()
        obj.merge_property("PROP", TestMergeProperty.VALUE)
        obj.merge_property("PROP", "value3")

        assert obj["PROP"] == ["value1", "value2", "value3"]
        obj.merge_property("PROP", ["value4", "value5"])
        assert obj["PROP"] == ["value1", "value2", "value3", "value4", "value5"]


if __name__ == "__main__":
    pytest.main()
