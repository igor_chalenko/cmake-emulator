from enum import Enum

import pytest as pytest

from yaranga.model.converters import convert
from yaranga.model.typeinfo import TypeInfo

TEST_STRING = "a string"


class TestBoolConversion:
    @pytest.fixture
    def typeinfo(self):
        return TypeInfo(bool)

    @pytest.mark.parametrize(
        "test_input", ["42", "ON", "on", "some string", "YES", "y"]
    )
    def test_true_values(self, test_input, typeinfo):
        assert convert(test_input, typeinfo)

    @pytest.mark.parametrize(
        "test_input", ["OFF", "off", "ignore", "0", "no", "false", "n"]
    )
    def test_false_values(self, test_input, typeinfo):
        assert not convert(test_input, typeinfo)

    @pytest.mark.parametrize("test_input", ["NOTFOUND", "name_NOTFOUND"])
    def test_none_values(self, test_input, typeinfo):
        assert not convert(test_input, typeinfo)

    def test_non_bool(self, typeinfo):
        class TestBool:
            def __bool__(self):
                return True

        val = TestBool()
        assert convert(val, typeinfo)


class Positive(int):
    def __new__(cls, value, *args, **kwargs):
        return super(cls, cls).__new__(cls, value)

    def __add__(self, other):
        res = super(Positive, self).__add__(other)
        return self.__class__(max(res, 0))

    def __sub__(self, other):
        res = super(Positive, self).__sub__(other)
        return self.__class__(max(res, 0))

    def __mul__(self, other):
        res = super(Positive, self).__mul__(other)
        return self.__class__(max(res, 0))

    def __floordiv__(self, other):
        res = super(Positive, self).__floordiv__(other)
        return self.__class__(max(res, 0))

    def __str__(self):
        return f"{int(self)}"

    def __repr__(self):
        return f"Positive({int(self)})"


class TestIntConversion:
    @pytest.fixture
    def typeinfo(self):
        return TypeInfo(int)

    @pytest.mark.parametrize(
        "test_input, expected",
        [(Positive(42), Positive(42)), ("-42", -42), ("NOTFOUND", None)],
    )
    def test_cast(self, typeinfo, test_input, expected):
        assert expected == convert(test_input, typeinfo)

    def test_invalid_values(self, typeinfo):
        with pytest.raises(ValueError):
            convert(TEST_STRING, typeinfo)


class TestFloatConversion:
    @pytest.fixture
    def typeinfo(self):
        return TypeInfo(float)

    @pytest.mark.parametrize(
        "test_input, expected", [("-42.0", -42.0), ("NOTFOUND", None)]
    )
    def test_cast(self, typeinfo, test_input, expected):
        assert pytest.approx(expected, 0.001) == convert(test_input, typeinfo)


class TestStrConversion:
    @pytest.fixture
    def typeinfo(self):
        return TypeInfo(str)

    @pytest.mark.parametrize(
        "test_input, expected",
        [
            ("42", "42"),
            (["#1", "#2"], "['#1', '#2']"),
            ("name_NOTFOUND", None),
            ("NOTFOUND", None),
        ],
    )
    def test_cast(self, typeinfo, test_input, expected):
        assert expected == convert(test_input, typeinfo)


class TestListConversion:
    @pytest.fixture
    def typeinfo(self):
        return TypeInfo(list[int])

    @pytest.mark.parametrize(
        "test_input, expected",
        [("", []), ("1;2;3", [1, 2, 3]), ("1;NOTFOUND", [1, None])],
    )
    def test_cast(self, typeinfo, test_input, expected):
        res = convert(test_input, typeinfo)
        assert isinstance(res, list)
        assert expected == res

    def test_none(self, typeinfo):
        assert convert(None, typeinfo) == []

    def test_nested_list(self, typeinfo):
        with pytest.raises(ValueError):
            convert([1, 2, "[1, 2]"], typeinfo)
        with pytest.raises(TypeError):
            ti = TypeInfo(list[list[int]])

    def test_invalid_int(self, typeinfo):
        with pytest.raises(ValueError):
            convert(["1;2;3"], typeinfo)


class TestDictConversion:
    @pytest.fixture
    def typeinfo(self):
        return TypeInfo(dict[str, int])

    @pytest.mark.parametrize(
        "test_input, expected",
        [
            ("", {}),
            ("two_jobs=2; ten_jobs=10", {"two_jobs": 2, "ten_jobs": 10}),
            ("two_jobs=NOTFOUND", {"two_jobs": None}),
        ],
    )
    def test_cast(self, typeinfo, test_input, expected):
        res = convert(test_input, typeinfo)
        assert isinstance(res, dict)
        assert expected == res


class TestEnumConversion:
    def test_cast(self):
        class TestEnum(Enum):
            A = (1,)
            B = 2

        a = "A"
        b = "B"
        typeinfo = TypeInfo(TestEnum)

        res = convert(a, typeinfo)
        assert isinstance(res, TestEnum) and res == TestEnum.A
        res = convert(b, typeinfo)
        assert isinstance(res, TestEnum) and res == TestEnum.B
        c = "C"
        with pytest.raises(KeyError):
            convert(c, typeinfo)

        assert convert(None, typeinfo) is None


if __name__ == "__main__":
    pytest.main()
