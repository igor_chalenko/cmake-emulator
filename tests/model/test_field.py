import pytest as pytest

from yaranga.model.exceptions import NotWritableAttributeError, ValidationError
from yaranga.model.field import Field
from yaranga.undefined import Undefined
from yaranga.objects.cmakeobject import CMakeObject


class TestField:
    def test_alias(self):
        class TestModel(CMakeObject):
            a: int = Field(alias="B")

        model = TestModel()
        assert model.a is Undefined
        model["B"] = 42
        assert model.a == 42

    def test_read_only(self):
        class TestModel(CMakeObject):
            a: int = Field(read_only=True)

        model = TestModel()
        assert model.a is Undefined
        model.a = 42
        try:
            model.a = 41
            assert False
        except NotWritableAttributeError as e:
            assert str(e) == f"TestModel.a is read-only, will not update"

    def test_custom_converter(self):
        def convert_int(value, _):
            return int(value) + 1

        class TestModel(CMakeObject):
            a: int = Field(converter=convert_int)

        model = TestModel()
        model.a = 41
        assert model.a == 42

    def test_custom_validator(self):
        def validate_int(_, value):
            if value > 42:
                raise ValidationError(
                    f"{value} is too large; 42 is the maximum allowed"
                )

        class TestModel(CMakeObject):
            a: int = Field(validators=[validate_int])

        model = TestModel()
        model.a = 42
        with pytest.raises(ValidationError):
            model.a = 43

    def test_default_value(self):
        class TestModel(CMakeObject):
            a: int = Field(default=42)
            b: list[str] = []

        model = TestModel()
        assert 42 == model.a
        model2 = TestModel(a=43)
        assert 43 == model2.a
        model3 = TestModel()
        assert 42 == model3.a

    def test_invalid_configuration(self):
        with pytest.raises(AttributeError):

            class TestModel(CMakeObject):
                a: int = Field(const=True)

            TestModel()

        with pytest.raises(AttributeError):

            class TestModel(CMakeObject):
                a: int = Field(nullable=False, default=None)

            TestModel()


if __name__ == "__main__":
    pytest.main()
