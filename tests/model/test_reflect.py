from pathlib import Path
from typing import Union, Optional

import pytest as pytest

from yaranga.model.reflection import is_union, is_optional


class TestIntrospection:
    def test_generic_type_detection(self):
        assert is_union(Union[str, Path])
        assert is_optional(Optional[str])
        # this holds in 3.9, but I don't know about later versions
        assert is_optional(Union[str, None])


if __name__ == "__main__":
    pytest.main()
