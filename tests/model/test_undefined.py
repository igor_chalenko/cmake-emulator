import pytest

from yaranga.undefined import Undefined


class TestUndefined:
    def test_comparison(self):
        self.x = Undefined
        assert self.x == Undefined
        assert self.x is Undefined


if __name__ == "__main__":
    pytest.main()
