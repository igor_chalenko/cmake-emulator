from pathlib import Path

import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator
from yaranga.commands.get import (
    GetCacheVariableProperty,
    GetTestProperty,
    GetSourceFileProperty,
    GetGlobalProperty,
)
from yaranga.commands.setproperty import SetGlobalProperty


class SingleUpdateTranslator(Translator):
    def flush(self, last=True):
        self.set_global_property("test_1", "value")
        self.set_global_property("test_2", None)


class TestGetGlobalProperty(CommandTestCase):
    @pytest.fixture
    def translator(self, resource_path):
        translator = SingleUpdateTranslator()
        translator.add_directory(resource_path, resource_path)
        return translator

    @pytest.fixture
    def prop_name(self):
        return "test_1"

    @pytest.fixture
    def unknown_prop_name(self):
        # should not be the same as `prop_name`
        return "test_2"

    def test_str(self):
        cmd = GetGlobalProperty("IN_TRY_COMPILE")
        assert str(cmd) == "narta_get_global_property(IN_TRY_COMPILE)"

    def test(self, translator, commands, prop_name, unknown_prop_name):
        with Injector(translator):
            value = translator.globals[prop_name]
            assert value == "value"
            assert len(commands) == 3, (
                "There must be exactly 3 commands in the tray:\n"
                "narta_get_global_property(test_1)\n"
                "set_property(GLOBAL PROPERTY test_1 value)\n"
                "set_property(GLOBAL PROPERTY test_2)"
            )
            assert commands[0] == GetGlobalProperty(prop_name)
            assert commands[1] == SetGlobalProperty(
                translator.globals, prop_name, "value"
            )
            assert commands[2] == SetGlobalProperty(
                translator.globals, unknown_prop_name, None
            )

    def test_get_non_existing_property(self, translator, unknown_prop_name: str):
        with Injector(translator):
            assert translator.globals[unknown_prop_name] is None


class TestGetCacheVariableProperty(object):
    def test_str(self):
        cmd = GetCacheVariableProperty("A", "PROP")
        assert str(cmd) == "narta_get_cache_variable_property(A PROP)"


class TestGetTestProperty(object):
    def test_str(self, resource_path):
        cmd = GetTestProperty(resource_path, "A", "PROP")
        cur_dir_str = str(resource_path)
        path = cur_dir_str if cur_dir_str.isalnum() else '"' + cur_dir_str + '"'
        assert str(cmd) == f"narta_get_test_property({Path(path).as_posix()} A PROP)"


class TestGetSourceFileProperty(CommandTestCase):
    def test_str(self, translator):
        cmd = GetSourceFileProperty(translator.current_source_dir, "foo.cc", "PROP2")
        cur_dir_str = translator.current_source_dir.as_posix()
        assert (
            str(cmd)
            == f'narta_get_source_file_property("{cur_dir_str}" "foo.cc" PROP2)'
        )


if __name__ == "__main__":
    pytest.main()
