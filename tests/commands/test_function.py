import pytest

from yaranga.commands.function import Function


class TestFunction:
    def test_function(self):
        function = Function("fn", 1, "2", 3)
        assert str(function) == "fn(1 2 3)"


if __name__ == "__main__":
    pytest.main()
