import abc

import pytest

from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator, silent
from yaranga.commands.get import GetTestProperty, GetTest


class GetTestBase(metaclass=abc.ABCMeta):
    @pytest.fixture
    def test_name(self):
        return "test_1"

    @pytest.fixture
    def unknown_test_name(self):
        # should not be the same as `test_name`
        return "test_2"

    @pytest.fixture
    def prop_name(self):
        return "PROP"

    @pytest.fixture
    def prop_value(self):
        return "value"

    @abc.abstractmethod
    def flush_emulator(self, resource_path, test_name, prop_name, prop_value):
        raise NotImplementedError()

    @pytest.fixture
    def context(self, resource_path, test_name, prop_name, prop_value):
        context = self.flush_emulator(resource_path, test_name, prop_name, prop_value)
        context.add_directory(resource_path, resource_path)
        return context

    @pytest.fixture
    def commands(self, context: Translator):
        return context.command_tray.get_commands()


class TestGetTest(GetTestBase):
    def flush_emulator(self, resource_path, test_name, prop_name, prop_value):
        class SequentialFlushEmulator(Translator):
            def __init__(self):
                super(SequentialFlushEmulator, self).__init__()
                self.added_test = False

            def flush(self, last=True):
                if self.added_test:
                    self.set_test_property(test_name, prop_name, prop_value)
                else:
                    with (silent(self)):
                        self.add_test(test_name)
                        self.added_test = True

        context = SequentialFlushEmulator()
        context.add_directory(resource_path, resource_path)
        return context

    def test_get_test(
        self, context, commands, test_name: str, prop_name: str, prop_value
    ):
        with Injector(context):
            value_1 = context.get_test_property(test_name, prop_name)
            value_2 = context.tests[test_name][prop_name]
            assert value_1 == value_2 == prop_value
            assert commands[0] == GetTest(context.current_source_dir, test_name)
            assert commands[1] == GetTestProperty(
                context.current_source_dir, test_name, prop_name
            )

    def test_get_non_existing_test(
        self, context, unknown_test_name: str, prop_name: str
    ):
        with Injector(context):
            with pytest.raises(ValueError):
                context.get_test_property(unknown_test_name, prop_name)


class TestGetTestProperty(GetTestBase):
    def flush_emulator(self, resource_path, test_name, prop_name, prop_value):
        class CumulativeFlushEmulator(Translator):
            def flush(self, last=True):
                with (silent(self)):
                    self.add_test(test_name)
                    self.set_test_property(test_name, prop_name, prop_value)

        return CumulativeFlushEmulator()

    def test(self, context, commands, test_name, prop_name, prop_value):
        with Injector(context):
            value = context.get_test_property(test_name, prop_name)
            assert value == prop_value
            assert commands[0] == GetTest(context.current_source_dir, test_name)
