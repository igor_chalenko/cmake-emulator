import pytest

from yaranga.commands.targets_delta import TargetsDelta, TargetsDeltaBefore


class TestTargetsDelta:
    def test_targets_delta_before(self, resource_path):
        cmd = TargetsDeltaBefore(resource_path)
        current_dir = resource_path.as_posix()
        assert str(cmd) == f'narta_targets("{current_dir}" PREV)'

    def test_targets_delta(self, resource_path):
        cmd = TargetsDelta(resource_path)
        current_dir = resource_path.as_posix()
        assert (
            str(cmd)
            == f'narta_targets_delta("{current_dir}" PREV CURRENT directory_callback '
            f"directory_property_callback)"
        )


if __name__ == "__main__":
    pytest.main()
