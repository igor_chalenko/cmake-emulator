import pytest
from yaranga.cmake.injector import Injector

from tests.commands.command_test_case import CommandTestCase


class TestAddNewLibrary(CommandTestCase):
    def test_exec(self, translator):
        name = "name"
        with Injector(translator):
            translator.add_static_library(
                name=name, exclude_from_all=False, sources=["src/foo.cc"]
            )
            cmd = translator.command_tray.commands[3]
            assert str(cmd) == f'add_library({name} STATIC "src/foo.cc")'


if __name__ == "__main__":
    pytest.main()
