import os

import pytest
from yaranga.undefined import Undefined

from yaranga.commands.set import Set, SetCacheVariable
from yaranga.objects.cache_variable import CacheVariableType
from .command_test_case import CommandTestCase


class TestSetVariable(CommandTestCase):
    def test_set(self, translator):
        set_command = Set(translator.sm, "var", "value")
        assert str(set_command) == 'set(var "value")'
        translator.set(set_command.name, set_command.value)
        assert "value" == translator["var"]

        remove_command = Set(translator.sm, "var", Undefined)
        translator.set(remove_command.name, remove_command.value)
        assert "var" not in translator

        # repeat to make sure the state is correct
        translator.set(set_command.name, set_command.value)
        assert "var" in translator
        del translator["var"]
        assert "var" not in translator

    def test_set_env(self, translator):
        set_command = Set(translator.sm, "$ENV{var}", "value")
        assert str(set_command) == 'set($ENV{var} "value")'
        translator.set(set_command.name, set_command.value)
        assert "value" == os.environ["var"]

    def test_set_cache(self, translator):
        set_command = Set(translator.sm, "$CACHE{var}", "value")
        assert str(set_command) == 'set($CACHE{var} "value")'
        translator.set(set_command.name, set_command.value)
        assert translator.sm.cache_variables["var"].value == "value"


class TestSetCacheVariable(CommandTestCase):
    @pytest.fixture
    def command(self, translator):
        translator.set_cache_variable(
            "var1",
            "value1",
            typ=CacheVariableType.STRING,
            helpstring="docs",
            force=True,
        )
        cmd = SetCacheVariable(
            "var1",
            "value1",
            typ=CacheVariableType.STRING,
            helpstring="docs",
            force=True,
        )

        return cmd

    def test_str(self, command):
        assert str(command) == f"set(var1 value1 CACHE STRING docs FORCE)"

    def test_remove(self, command, translator):
        translator.set_cache_variable(command.name, None)
        assert command.name not in translator.sm.cache_variables

    def test_force(self, command, translator):
        translator.set_cache_variable(command.name, command.value)
        translator.set_cache_variable(command.name, command.value)
        # no force - no update
        assert "value1" == translator.sm.cache_variables["var1"]["VALUE"]
        # force the update
        command.value = "another value"
        translator.set_cache_variable(command.name, command.value, force=True)
        assert "another value" == translator.sm.cache_variables["var1"]["VALUE"]


if __name__ == "__main__":
    pytest.main()
