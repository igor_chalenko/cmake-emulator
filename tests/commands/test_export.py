from pathlib import Path

import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.commands.export import ExportCommand

SOURCE_FILES = "../resources/foo.cc"
LABEL = "cool label"


class TestExport(CommandTestCase):
    def test_export(self, translator):
        export_path = Path(translator["CMAKE_SOURCE_DIR"]).as_posix()
        export_command = ExportCommand(export_path)
        assert f'export("{export_path}")' == str(export_command)


if __name__ == "__main__":
    pytest.main()
