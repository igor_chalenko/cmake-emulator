import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.injector import Injector
from yaranga.objects.target_type import TargetType
from yaranga.undefined import Undefined


class TestAddInterfaceLibrary(CommandTestCase):
    @pytest.mark.parametrize(
        "test_method,option_name",
        [
            ("add_interface_library", "INTERFACE"),
            ("add_module_library", "MODULE"),
            ("add_shared_library", "SHARED"),
            ("add_static_library", "STATIC"),
            ("add_object_library", "OBJECT"),
        ],
    )
    def test_str(self, translator, test_method: str, option_name: str):
        name = "name"

        with Injector(translator):
            getattr(translator, test_method)(name=name, sources=["src/foo.cc"])
            cmd = translator.command_tray.commands[3]
            assert str(cmd) == f'add_library({name} {option_name} "src/foo.cc")'

    @pytest.mark.parametrize(
        "test_method",
        [
            "add_interface_library",
            "add_module_library",
            "add_shared_library",
            "add_static_library",
            "add_object_library",
        ],
    )
    def test_exec(self, translator, test_method):
        name = "name"
        target = getattr(translator, test_method)(
            name=name, exclude_from_all=False, sources=["src/foo.cc"]
        )
        assert name in translator.sm.targets

        with Injector(translator):
            target.compile_definitions = Undefined
            assert target.compile_definitions == ["-O1"]

    def test_mark_as_dirty(self, translator):
        library = translator.add_library(
            name="name",
            target_type=TargetType.STATIC_LIBRARY,
            exclude_from_all=False,
            sources=["src/foo.cc"],
        )
        library.mark_as_dirty()
        assert library.name == "name"
        assert library.exclude_from_all is Undefined


if __name__ == "__main__":
    pytest.main()
