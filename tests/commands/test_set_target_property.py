import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.translator import Translator
from yaranga.commands.setproperty import SetTargetProperty
from yaranga.objects.updatetype import UpdateType
from yaranga.undefined import Undefined


class TestTranslator(Translator):
    __test__ = False

    def flush(self, last=True):
        self.add_executable("non_existing_target", sources=["foo.cc"])


class TestSetTargetProperty(CommandTestCase):
    @pytest.fixture
    def translator(self, resource_path):
        context = TestTranslator()
        context.add_directory(resource_path, resource_path)
        return context

    @pytest.fixture
    def target(self, translator):
        return translator.add_executable("test", sources=["foo.cc"])

    def test_set_value(self, translator, target, commands):
        translator.set_target_property("test", "VAR", "value")
        cmd = commands[-1]
        assert str(cmd) == f"set_property(TARGET {target.name} PROPERTY VAR value)"

    def test_append(self, translator, target):
        translator.set_target_property("test", "VAR", "value")
        translator.set_target_property("test", "VAR", "value2", UpdateType.APPEND)
        assert translator.sm.targets["test"]["VAR"] == ["value", "value2"]

        with pytest.raises(ValueError):
            translator.set_target_property(
                "test", "VAR", "more", UpdateType.APPEND_STRING
            )

        with pytest.raises(ValueError):
            translator.set_target_property("test", "VAR", Undefined, UpdateType.APPEND)

        with pytest.raises(ValueError):
            translator.set_target_property(
                "test", "VAR", Undefined, UpdateType.APPEND_STRING
            )

    def test_append_to_empty(self, translator, target):
        translator.set_target_property("test", "VAR", "value", UpdateType.APPEND)
        assert translator.sm.targets["test"]["VAR"] == "value"

    def test_append_string(self, translator, target):
        translator.set_target_property("test", "VAR", "auto")
        translator.set_target_property("test", "VAR", "cross", UpdateType.APPEND_STRING)
        assert translator.sm.targets["test"]["VAR"] == "autocross"

    def test_prepend(self, translator, target):
        translator.set_target_property("test", "VAR", "value")
        translator.set_target_property("test", "VAR", "value2", UpdateType.PREPEND)
        assert translator.sm.targets["test"]["VAR"] == ["value2", "value"]

    def test_remove_value(self, translator, target, commands):
        translator.set_target_property("test", "VAR", "value")
        translator.set_target_property("test", "VAR")
        cmd = commands[-1]
        assert str(cmd) == f"set_property(TARGET test PROPERTY VAR)"

    def test_set_none(self, translator, target, commands):
        translator.set_target_property("test", "VAR", "value")
        translator.set_target_property("test", "VAR", None)
        cmd = commands[-1]
        assert str(cmd) == f"set_property(TARGET test PROPERTY VAR)"

    def test_add_missing(self, translator, target, commands):
        translator.set_target_property("non_existing_target", "VAR", "value")
        cmd = commands[-1]
        assert cmd == SetTargetProperty(cmd.obj, "VAR", "value")
