import pytest

from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator
from yaranga.commands.get import Get
from yaranga.commands.set import Set


class TestTranslator(Translator):
    __test__ = False

    def flush(self, last=False):
        self["test_1"] = "value"
        self["test_2"] = None


class TestGet:
    @pytest.fixture
    def context(self, resource_path):
        context = TestTranslator()
        context.add_directory(resource_path, resource_path)
        return context

    @pytest.fixture
    def commands(self, context: Translator):
        return context.command_tray.get_commands()

    @pytest.fixture
    def var_name(self):
        return "test_1"

    @pytest.fixture
    def unknown_var_name(self):
        return "test_2"

    def test_get(self):
        cmd = Get("x")
        assert str(cmd) == "narta_get_variable(x)"

    def test_known(self, context, commands, var_name):
        with Injector(context):
            value = context[var_name]
            assert value == "value"
            assert len(commands) == 3, "There must be exactly 2 commands in the tray"
            assert commands[0] == Get(var_name)
            assert commands[1] == Set(context.sm, var_name, "value")

    def test_unknown(self, context, commands, unknown_var_name):
        with Injector(context):
            value = context[unknown_var_name]
            assert value is None


if __name__ == "__main__":
    pytest.main()
