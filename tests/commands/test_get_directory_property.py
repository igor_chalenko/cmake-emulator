import abc
from pathlib import Path

import pytest

from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator, silent
from yaranga.commands.get import GetDirectory, GetDirectoryProperty


class GetDirectoryBase(metaclass=abc.ABCMeta):
    @pytest.fixture
    def prop_name(self):
        return "PROP"

    @pytest.fixture
    def prop_value(self):
        return "value"

    @abc.abstractmethod
    def flush_emulator(self, resource_path, subdir, prop_name, prop_value):
        raise NotImplementedError()

    @pytest.fixture
    def context(self, resource_path, subdir, prop_name, prop_value):
        context = self.flush_emulator(resource_path, subdir, prop_name, prop_value)
        context.add_directory(resource_path, resource_path)
        return context

    @pytest.fixture
    def subdir(self, resource_path: Path):
        return resource_path / "build"

    @pytest.fixture
    def commands(self, context: Translator):
        return context.command_tray.get_commands()


class TestGetDirectory(GetDirectoryBase):
    def flush_emulator(self, resource_path, subdir, prop_name, prop_value):
        class SequentialFlushEmulator(Translator):
            def __init__(self):
                super(SequentialFlushEmulator, self).__init__()
                self.added_dir = False

            def flush(self, last=True):
                if self.added_dir:
                    d = self.sm.directories[self.current_source_dir]
                    self.set_directory_property(str(d), prop_name, prop_value)
                else:
                    with silent(self):
                        self.add_directory(subdir, self.current_source_dir)
                        self.added_dir = True

        return SequentialFlushEmulator()

    def test(self, context, commands, subdir, prop_name, prop_value):
        with Injector(context):
            value = context.directories[subdir][prop_name]
            assert value == prop_value
            assert len(commands) == 3, "There must be exactly 3 commands in the tray"
            assert commands[0] == GetDirectory(subdir)
            assert commands[1] == GetDirectoryProperty(subdir, prop_name)


class TestGetDirectoryProperty(GetDirectoryBase):
    def flush_emulator(self, resource_path, target_name, prop_name, prop_value):
        class CumulativeFlushEmulator(Translator):
            def flush(self, last=True):
                d = self.add_directory(
                    self.current_source_dir / "build", self.current_source_dir
                )
                self.set_directory_property(str(d), prop_name, prop_value)

        return CumulativeFlushEmulator()

    def test_get_directory_property(
        self, context, commands, subdir, prop_name, prop_value
    ):
        with Injector(context):
            value = context.directories[subdir][prop_name]
            assert value == prop_value
            assert commands[0] == GetDirectory(subdir)
