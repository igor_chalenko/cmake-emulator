import abc

import pytest

from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator, silent
from yaranga.commands.get import GetTargetProperty, GetTarget


class GetTargetBase(metaclass=abc.ABCMeta):
    @pytest.fixture
    def target_name(self):
        return "test"

    @pytest.fixture
    def prop_name(self):
        return "PROP"

    @pytest.fixture
    def prop_value(self):
        return "value"

    @pytest.fixture
    def sources(self):
        return ["foo.cc"]

    @abc.abstractmethod
    def flush_emulator(
        self, resource_path, target_name, prop_name, prop_value, sources
    ):
        raise NotImplementedError()

    @pytest.fixture
    def context(self, resource_path, target_name, prop_name, prop_value, sources):
        context = self.flush_emulator(
            resource_path, target_name, prop_name, prop_value, sources
        )
        context.add_directory(resource_path, resource_path)
        return context

    @pytest.fixture
    def commands(self, context: Translator):
        return context.command_tray.get_commands()


class TestGetTarget(GetTargetBase):
    def flush_emulator(
        self, resource_path, target_name, prop_name, prop_value, sources
    ):
        class SequentialFlushEmulator(Translator):
            def __init__(self):
                super(SequentialFlushEmulator, self).__init__()
                self.added_target = False

            def flush(self, last=True):
                if self.added_target:
                    self.set_target_property(target_name, prop_name, prop_value)
                else:
                    with silent(self):
                        self.add_executable(target_name, sources=sources)
                        self.added_target = True

        return SequentialFlushEmulator()

    def test(self, context, commands, target_name, prop_name, prop_value):
        with Injector(context):
            value = context.targets[target_name][prop_name]
            assert value == prop_value
            assert commands[0] == GetTarget(context.current_source_dir, target_name)
            assert commands[1] == GetTargetProperty(target_name, prop_name)


class TestGetTargetProperty(GetTargetBase):
    def flush_emulator(
        self, resource_path, target_name, prop_name, prop_value, sources
    ):
        class CumulativeFlushEmulator(Translator):
            def flush(self, last=True):
                self.add_executable(target_name, sources=sources)
                self.set_target_property(target_name, prop_name, prop_value)

        return CumulativeFlushEmulator()

    def test_str(self, context, commands, target_name: str, sources: list[str]):
        prop_name = "ANDROID_API"

        target = context.add_executable(target_name, sources=sources)
        cmd = GetTargetProperty(target.name, prop_name)

        assert cmd == GetTargetProperty(target.name, prop_name)

    def test_get_target_property(
        self, context, commands, target_name, prop_name, prop_value
    ):
        with Injector(context):
            value = context.get_target_property(target_name, prop_name)
            assert value == prop_value
            assert commands[0] == GetTarget(context.current_source_dir, target_name)
