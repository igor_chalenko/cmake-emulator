import abc

import pytest

from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator, silent
from yaranga.commands.get import GetCacheVariable, GetCacheVariableProperty
from yaranga.objects.cache_variable import CacheVariableType
from yaranga.undefined import Undefined


class GetCacheVariableBase(metaclass=abc.ABCMeta):
    @pytest.fixture
    def var_name(self):
        return "test_1"

    @pytest.fixture
    def var_value(self):
        return "value_1"

    @pytest.fixture
    def unknown_var_name(self):
        # should not be the same as `var_name`
        return "test_2"

    @pytest.fixture
    def prop_name(self):
        return "PROP"

    @pytest.fixture
    def prop_value(self):
        return "value"

    @abc.abstractmethod
    def flush_emulator(self, resource_path, var_name, var_value, prop_name, prop_value):
        raise NotImplementedError()

    @pytest.fixture
    def context(self, resource_path, var_name, var_value, prop_name, prop_value):
        context = self.flush_emulator(
            resource_path, var_name, var_value, prop_name, prop_value
        )
        context.add_directory(resource_path, resource_path)
        return context

    @pytest.fixture
    def commands(self, context: Translator):
        return context.command_tray.get_commands()


class TestGetCacheVariable(GetCacheVariableBase):
    def flush_emulator(self, resource_path, var_name, var_value, prop_name, prop_value):
        class SequentialFlushEmulator(Translator):
            def __init__(self):
                super(SequentialFlushEmulator, self).__init__()
                self.added_var = False

            def flush(self, last=True):
                if self.added_var:
                    self.set_cache_variable_property(var_name, prop_name, prop_value)
                else:
                    with silent(self):
                        self.set_cache_variable(
                            var_name, var_value, CacheVariableType.INTERNAL
                        )
                        self.added_var = True

        return SequentialFlushEmulator()

    def test(self, context: Translator, commands, var_name, prop_name, prop_value):
        value = context.get_cache_variable_property(var_name, prop_name)
        assert value is Undefined, f"{var_name} is not in the initial context"
        with Injector(context):
            value = context.cache[var_name][prop_name]
            assert (
                value == prop_value
            ), f"{var_name} is loaded into the context during the flush"
            assert len(commands) == 3, "There must be exactly 3 commands in the tray"
            assert commands[0] == GetCacheVariable(var_name)
            assert commands[1] == GetCacheVariableProperty(var_name, prop_name)

    def test_get_non_existing_variable(self, context, unknown_var_name: str, prop_name):
        with Injector(context):
            assert (
                context.get_cache_variable_property(unknown_var_name, prop_name) is None
            )


class TestGetCacheVariableProperty(GetCacheVariableBase):
    def flush_emulator(self, resource_path, var_name, var_value, prop_name, prop_value):
        class CumulativeFlushEmulator(Translator):
            def flush(self, last=True):
                self.set_cache_variable(var_name, var_value, CacheVariableType.INTERNAL)
                self.set_cache_variable_property(var_name, prop_name, prop_value)

        return CumulativeFlushEmulator()

    def test(self, context, commands, var_name, prop_name, prop_value):
        with Injector(context):
            value = context.cache[var_name][prop_name]
            assert value == prop_value
            assert commands[0] == GetCacheVariable(var_name)
