import pytest

from yaranga.commands.globals_delta import GlobalsDelta, GlobalsDeltaBefore


class TestGlobalsDelta:
    def test_globals_delta_before(self):
        cmd = GlobalsDeltaBefore()
        assert str(cmd) == "narta_globals(PREV)"

    def test_globals_delta(self):
        cmd = GlobalsDelta()
        assert str(cmd) == "narta_globals_delta(PREV CURRENT globals_callback)"


if __name__ == "__main__":
    pytest.main()
