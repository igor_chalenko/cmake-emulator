import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.injector import Injector

from yaranga.cmake.translator import Translator
from yaranga.commands.setproperty import SetTestProperty
from yaranga.objects.updatetype import UpdateType
from yaranga.undefined import Undefined


class TestSetTestProperty(CommandTestCase):
    @pytest.fixture
    def test(self, translator):
        return translator.add_test("test", command="date")

    def test_set_value(self, translator, commands, test):
        translator.set_test_property("test", "VAR", "value")
        cmd = commands[-1]
        assert str(cmd) == f"set_property(TEST {test.name} PROPERTY VAR value)"

    def test_append(self, translator, test):
        translator.set_test_property("test", "VAR", "value")
        translator.set_test_property("test", "VAR", "value2", UpdateType.APPEND)
        assert translator.sm.current_dir.tests_dict["test"]["VAR"] == [
            "value",
            "value2",
        ]

        with pytest.raises(ValueError):
            translator.set_test_property(
                "test", "VAR", "more", UpdateType.APPEND_STRING
            )

        with pytest.raises(ValueError):
            translator.set_test_property("test", "VAR", Undefined, UpdateType.APPEND)

        with pytest.raises(ValueError):
            translator.set_test_property(
                "test", "VAR", Undefined, UpdateType.APPEND_STRING
            )

    def test_append_to_empty(self, translator, test):
        translator.set_test_property("test", "VAR", "value", UpdateType.APPEND)
        assert translator.tests["test"]["VAR"] == "value"

    def test_append_string(self, translator, test):
        translator.set_test_property("test", "VAR", "auto")
        translator.set_test_property("test", "VAR", "cross", UpdateType.APPEND_STRING)
        assert translator.tests["test"]["VAR"] == "autocross"

    def test_prepend(self, translator, test):
        translator.set_test_property("test", "VAR", "value")
        translator.set_test_property("test", "VAR", "value2", UpdateType.PREPEND)
        assert translator.tests["test"]["VAR"] == ["value2", "value"]

    def test_remove_value(self, translator, commands, test):
        translator.set_test_property("test", "VAR", "value")
        translator.set_test_property("test", "VAR")
        cmd = commands[-1]
        assert str(cmd) == f"set_property(TEST test PROPERTY VAR)"

    def test_set_none(self, translator, commands, test):
        translator.set_test_property("test", "VAR", "value")
        translator.set_test_property("test", "VAR", None)
        cmd = commands[-1]
        assert str(cmd) == f"set_property(TEST test PROPERTY VAR)"


class TestTranslator(Translator):
    __test__ = False

    def flush(self, last=True):
        self.add_test("non_existing_test")


class TestSetPropertyOnNonExistingTest(CommandTestCase):
    @pytest.fixture
    def translator(self, resource_path):
        translator = TestTranslator()
        translator.add_directory(resource_path, resource_path)
        return translator

    def test_add_missing(self, translator):
        with Injector(translator):
            translator.set_test_property("non_existing_test", "VAR", "value")
            cmd = translator.command_tray.commands[-1]
            assert cmd == SetTestProperty(cmd.obj, "VAR", "value")

    def test_error(self, translator, commands):
        with pytest.raises(ValueError):
            with Injector(translator):
                translator.set_test_property("non_existing_test_2", "VAR", "value")

        with pytest.raises(ValueError):
            # this will configure one test
            with Injector(translator):
                translator.set_test_property(
                    ["non_existing_test", "non_existing_test_2"], "VAR", "value"
                )

        assert len(translator.command_tray.commands) == 5
        test = translator.tests["non_existing_test"]
        assert commands[2] == SetTestProperty(test, "VAR", "value")


if __name__ == "__main__":
    pytest.main()
