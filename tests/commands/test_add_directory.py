from pathlib import Path

import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator


class TestAddDirectory(CommandTestCase):
    @pytest.fixture
    def commands(self, translator):
        return translator.command_tray.get_commands()

    @pytest.fixture
    def subdir(self, translator: Translator):
        return translator.current_source_dir / "build"

    def test(self, translator, commands, subdir):
        with Injector(translator):
            translator.sm.current_dir.set_source_property("foo.cc", "PROP", "value")
            subdirectory = translator.add_directory(
                source_dir=subdir,
                binary_dir=subdir,
                parent_dir=translator.current_source_dir,
            )
            # the existing source was copied into the new directory
            assert subdirectory.get_source_file("foo.cc")["LOCATION"] == "foo.cc"


if __name__ == "__main__":
    pytest.main()
