import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.commands.add_imported_library import AddImportedLibrary
from yaranga.objects.target_type import ImportedTargetType


class TestAddImportedLibrary(CommandTestCase):
    def test_str(self, translator):
        name = "name"
        command = AddImportedLibrary(
            name="name",
            imported_target_type=ImportedTargetType.STATIC,
            imported_global=True,
        )

        assert str(command) == f"add_library({name} STATIC IMPORTED GLOBAL)"

    def test_exec(self, translator):
        name = "name"
        translator.add_imported_library(
            name=name,
            type_=ImportedTargetType.STATIC,
            imported_global=False,
        )
        assert name in translator.sm.targets


if __name__ == "__main__":
    pytest.main()
