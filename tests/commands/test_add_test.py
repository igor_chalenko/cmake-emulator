import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.commands.add_test import AddTest


class TestAddTest(CommandTestCase):
    def test_str(self, translator):
        translator.add_test("test", command="date")
        cmd = AddTest(name="test", command="date")
        assert str(cmd) == f"add_test(test COMMAND date)"


if __name__ == "__main__":
    pytest.main()
