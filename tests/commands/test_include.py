import pytest

from yaranga.commands.include import IncludeCommand

SOURCE_FILES = "../resources/foo.cc"
LABEL = "cool label"


class TestInclude(object):
    def test_include(self):
        include_command = IncludeCommand("cpm.cmake")
        assert 'include("cpm.cmake")' == str(include_command)


if __name__ == "__main__":
    pytest.main()
