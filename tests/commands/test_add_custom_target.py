import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.injector import Injector
from yaranga.commands.add_custom_target import AddCustomTarget


class TestAddCustomTarget(CommandTestCase):
    def test_str(self, translator, commands):
        name = "name"
        command = AddCustomTarget(name=name, command="date", all=True)
        assert str(command) == f"add_custom_target({name} ALL date)"

        with Injector(translator):
            translator.add_custom_target(command.name, command.command)
            assert 5 == len(commands)
            assert str(commands[-2]) == f"add_custom_target({name} date)"


if __name__ == "__main__":
    pytest.main()
