import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.injector import Injector
from yaranga.commands.add_executable import AddExecutable


class TestAddExecutable(CommandTestCase):
    def test_str(self, translator, commands):
        name = "name"
        command = AddExecutable(
            name=name, source_dir=translator.current_source_dir, sources=["src/foo.cc"]
        )
        assert str(command) == f'add_executable({name} "src/foo.cc")'

        with Injector(translator):
            translator.add_executable(name=command.name, sources=command.sources)
            assert 6 == len(commands)


if __name__ == "__main__":
    pytest.main()
