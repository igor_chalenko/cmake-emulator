from pathlib import Path

import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.translator import Translator
from yaranga.objects.updatetype import UpdateType
from yaranga.undefined import Undefined

FOO = "foo.cc"


class TestSetSourceFileProperty(CommandTestCase):
    def test_set_value(self, translator, commands):
        translator.set_source_property(FOO, "VAR", "value")
        cmd = commands[-1]
        assert str(cmd) == f'set_property(SOURCE "foo.cc" PROPERTY VAR value)'

    def test_append(self, translator: Translator):
        translator.set_source_property(FOO, "VAR", "value")
        translator.set_source_property(FOO, "VAR", "value2", UpdateType.APPEND)
        sourcefile = translator.sm.current_dir.get_source_file(FOO)
        assert sourcefile["VAR"] == ["value", "value2"]

        with pytest.raises(ValueError):
            translator.set_source_property(FOO, "VAR", "more", UpdateType.APPEND_STRING)

        with pytest.raises(ValueError):
            translator.set_source_property(FOO, "VAR", Undefined, UpdateType.APPEND)

        with pytest.raises(ValueError):
            translator.set_source_property(
                FOO, "VAR", Undefined, UpdateType.APPEND_STRING
            )

    def test_append_to_empty(self, translator):
        translator.set_source_property(FOO, "VAR", "value", UpdateType.APPEND)
        sourcefile = translator.sm.current_dir.get_source_file(FOO)
        assert sourcefile["VAR"] == "value"

    def test_append_string(self, translator):
        translator.set_source_property(FOO, "VAR", "auto")
        translator.set_source_property(FOO, "VAR", "cross", UpdateType.APPEND_STRING)
        sourcefile = translator.sm.current_dir.get_source_file(FOO)
        assert sourcefile["VAR"] == "autocross"

    def test_prepend(self, translator):
        translator.set_source_property(FOO, "VAR", "value")
        translator.set_source_property(FOO, "VAR", "value2", UpdateType.PREPEND)
        sourcefile = translator.sm.current_dir.get_source_file(FOO)
        assert sourcefile["VAR"] == ["value2", "value"]

    def test_remove_value(self, translator, commands):
        translator.set_source_property(FOO, "VAR", "value")
        translator.set_source_property(FOO, "VAR")
        sourcefile = translator.sm.current_dir.get_source_file(FOO)
        cmd = commands[-1]
        assert str(cmd) == f'set_property(SOURCE "{sourcefile.location}" PROPERTY VAR)'

    def test_set_none(self, translator, commands):
        translator.set_source_property(FOO, "VAR", "value")
        translator.set_source_property(FOO, "VAR", None)
        sourcefile = translator.sm.current_dir.get_source_file(FOO)
        cmd = commands[-1]
        assert str(cmd) == f'set_property(SOURCE "{sourcefile.location}" PROPERTY VAR)'

    def test_directories(self, resource_path, translator):
        translator.add_directory(resource_path, resource_path)

        translator.set_source_property(FOO, "VAR", "value", directories=resource_path)
        sourcefile = translator.sm.current_dir.get_source_file(FOO)
        assert sourcefile["VAR"] == "value"

    def test_target_directories(self, resource_path, translator):
        translator.add_directory(resource_path, resource_path)
        translator.add_executable("test")

        translator.set_current_directory(resource_path)
        translator.set_source_property(FOO, "VAR", "value", target_directories="test")
        sourcefile = translator.sm.directory(resource_path).get_source_file(FOO)
        assert sourcefile["VAR"] == "value"
