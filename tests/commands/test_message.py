import logging

import pytest

from yaranga.commands.message import MessageCommand


class TestMessage:
    def test_message(self):
        message = MessageCommand("message", logging.INFO)
        assert str(message) == f"message(STATUS message)"


if __name__ == "__main__":
    pytest.main()
