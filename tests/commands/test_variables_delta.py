import pytest

from yaranga.commands.variables_delta import VariablesDeltaBefore, VariablesDelta


class TestVariablesDelta:
    def test_variables_delta_before(self):
        cmd = VariablesDeltaBefore()
        assert str(cmd) == "narta_variables(PREV)"

    def test_globals_delta(self):
        cmd = VariablesDelta()
        assert str(cmd) == "narta_variables_delta(PREV CURRENT variables_callback)"


if __name__ == "__main__":
    pytest.main()
