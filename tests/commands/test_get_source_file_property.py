import abc
from pathlib import Path

import pytest

from yaranga.cmake.injector import Injector
from yaranga.cmake.translator import Translator, silent
from yaranga.commands.get import GetSourceFile, GetDirectory, GetSourceFileProperty
from yaranga.commands.setproperty import SetSourceFileProperty
from yaranga.objects.updatetype import UpdateType


class GetSourceFileBase(metaclass=abc.ABCMeta):
    @pytest.fixture
    def context(self, resource_path, subdir, sourcefile, prop_name, prop_value):
        context = self.flush_emulator(
            resource_path, subdir, sourcefile, prop_name, prop_value
        )
        context.add_directory(resource_path, resource_path)
        return context

    @pytest.fixture
    def commands(self, context: Translator):
        return context.command_tray.get_commands()

    @pytest.fixture
    def prop_name(self):
        return "PROP"

    @pytest.fixture
    def prop_value(self):
        return "value"

    @abc.abstractmethod
    def flush_emulator(self, resource_path, subdir, sourcefile, prop_name, prop_value):
        raise NotImplementedError()

    @pytest.fixture
    def sourcefile(self):
        return "foo.cc"

    @pytest.fixture
    def subdir(self, resource_path: Path):
        return resource_path / "build"

    @pytest.fixture
    def non_existing_sourcefile(self):
        return "foo_1.cc"


class TestGetSource(GetSourceFileBase):
    def flush_emulator(self, resource_path, subdir, sourcefile, prop_name, prop_value):
        class SequentialFLushEmulator(Translator):
            def __init__(self):
                super(SequentialFLushEmulator, self).__init__()
                self.added_dir = False

            def flush(self, last=True):
                if self.added_dir:
                    self.set_source_property(sourcefile, prop_name, prop_value)
                else:
                    with silent(self):
                        self.add_directory(subdir, subdir)
                    self.added_dir = True

        return SequentialFLushEmulator()

    def test(self, context, commands, subdir, sourcefile, prop_name, prop_value):
        with Injector(context):
            value = context.get_source_file_property(
                sourcefile, prop_name, directory=subdir
            )
            assert value == prop_value
            assert commands[0] == GetDirectory(subdir)
            assert commands[1] == GetSourceFile(subdir, sourcefile)


class TestGetSourceProperty(GetSourceFileBase):
    def flush_emulator(self, resource_path, subdir, sourcefile, prop_name, prop_value):
        class CumulativeTranslator(Translator):
            def flush(self, last=True):
                self.set_source_property(sourcefile, prop_name, prop_value)

        return CumulativeTranslator()

    def test(self, context, commands, sourcefile, prop_name, prop_value):
        with Injector(context):
            value = context.get_source_file_property(
                sourcefile, prop_name, directory=context.current_source_dir
            )
            assert value == prop_value
            assert commands[0] == GetSourceFile(context.current_source_dir, sourcefile)
            assert commands[1] == SetSourceFileProperty(
                sourcefile,
                prop_name,
                prop_value,
                UpdateType.SET,
                context.sm.current_dir,
            )

    def test_get_non_existing_source(
        self, context, commands, non_existing_sourcefile, prop_name
    ):
        with Injector(context):
            value = context.get_source_file_property(non_existing_sourcefile, prop_name)
            assert value is None


class TestGetSourcePropertyInThreeSteps(GetSourceFileBase):
    def flush_emulator(self, resource_path, subdir, sourcefile, prop_name, prop_value):
        class SequentialFlushEmulator(Translator):
            def __init__(self, working_dir):
                super(SequentialFlushEmulator, self).__init__()
                self.added_dir = False
                self.added_get_source = False

            def flush(self, last=True):
                if not self.added_dir:
                    with silent(self):
                        self.add_directory(subdir, subdir)
                    self.added_dir = True
                elif not self.added_get_source:
                    self.set_source_property(sourcefile, "LOCATION", sourcefile)
                    self.added_get_source = True
                else:
                    self.set_source_property(sourcefile, prop_name, prop_value)

        return SequentialFlushEmulator(resource_path)

    def test(self, context, commands, subdir, sourcefile, prop_name, prop_value):
        with Injector(context):
            value = context.get_source_file_property(
                sourcefile, prop_name, directory=subdir
            )
            assert value == prop_value
            assert commands[0] == GetDirectory(subdir)
            assert commands[1] == GetSourceFile(context.current_source_dir, sourcefile)
            assert commands[3] == GetSourceFileProperty(
                context.current_source_dir, sourcefile, prop_name
            )
            assert commands[4] == SetSourceFileProperty(
                sourcefile,
                prop_name,
                prop_value,
                UpdateType.SET,
                context.sm.current_dir,
            )
