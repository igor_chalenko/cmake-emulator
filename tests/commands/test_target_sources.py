from pathlib import Path

import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.translator import Translator
from yaranga.commands.target_sources import TargetSources
from yaranga.objects.target import Executable


class TestTargetSources(CommandTestCase):
    @pytest.fixture
    def executable(self, translator):
        return translator.add_executable(name="name", sources=["src/foo.cc"])

    @pytest.fixture
    def subdir(self, resource_path: Path):
        return resource_path / "build"

    @pytest.fixture
    def sources(self):
        return ["foo_1.cc", "foo_2.cc"]

    @pytest.fixture
    def cmake_sources(self) -> str:
        return '"foo_1.cc" "foo_2.cc"'

    def test_public(self, executable, sources, cmake_sources):
        command = TargetSources(executable.name, public=sources)
        assert str(command) == f"target_sources(name PUBLIC {cmake_sources})"

    def test_cmp0076(
        self, translator: Translator, executable, subdir, sources, cmake_sources
    ):
        translator.add_directory(subdir, subdir)
        translator.target_sources(executable.name, private="format.cc")
        assert executable.sources == [
            "src/foo.cc",
            Path(translator.current_source_dir, "format.cc"),
        ]

    def test_alias_target(self, translator, executable):
        alias_target = translator.add_alias_target("Foo::foo", executable)
        with pytest.raises(ValueError):
            translator.target_sources(alias_target.name, private="format.cc")

    def test_interface(self, executable):
        command = TargetSources(
            executable.name, public="pthread.cc", interface="catch2.cc"
        )
        assert (
            str(command)
            == 'target_sources(name PUBLIC "pthread.cc" INTERFACE "catch2.cc")'
        )

    def test_mixed_visibility(self, translator, executable: Executable):
        translator.target_sources(
            executable.name,
            public="catch2.cc",
            interface="pthread.cc",
            private=["catch.cc", "gtest.cc"],
        )
        translator.target_sources(executable.name, private="format.cc")
        command = translator.command_tray.get_commands()[-2]
        assert (
            str(command) == 'target_sources(name PUBLIC "catch2.cc" '
            'PRIVATE "catch.cc" "gtest.cc" INTERFACE "pthread.cc")'
        )
        assert executable.sources == [
            "src/foo.cc",
            "catch2.cc",
            "catch.cc",
            "gtest.cc",
            "format.cc",
        ]

    def test_str(self, translator, executable):
        translator.target_sources("name", "catch2.cc")
        cmd = translator.command_tray.get_commands()[3]
        assert str(cmd) == 'target_sources(name PUBLIC "catch2.cc")'


if __name__ == "__main__":
    pytest.main()
