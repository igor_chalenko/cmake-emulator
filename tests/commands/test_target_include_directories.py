import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.commands.target_include_directories import TargetIncludeDirectories
from yaranga.objects.target import Executable


class TestTargetIncludeDirectories(CommandTestCase):
    @pytest.fixture
    def executable(self, translator):
        return translator.add_executable(name="name", sources=["src/foo.cc"])

    def test_public(self, translator, executable):
        command = TargetIncludeDirectories(
            translator.current_source_dir,
            executable.name,
            public=["include", "include2"],
        )
        assert (
            str(command) == "target_include_directories(name PUBLIC include include2)"
        )

    def test_interface(self, translator, executable):
        command = TargetIncludeDirectories(
            translator.current_source_dir,
            executable.name,
            public="include",
            interface="include2",
        )
        assert (
            str(command)
            == "target_include_directories(name PUBLIC include INTERFACE include2)"
        )

    def test_mixed_visibility(self, translator, executable: Executable):
        translator.target_include_directories(
            executable.name,
            public="catch2",
            interface="pthread",
            private=["catch", "gtest"],
        )
        translator.target_include_directories(executable.name, private="format")
        command = translator.command_tray.get_commands()[-2]
        assert (
            str(command)
            == f"target_include_directories({executable.name} PUBLIC catch2 "
            "PRIVATE catch gtest INTERFACE pthread)"
        )

    def test_alias_target(self, translator, executable):
        alias_target = translator.add_alias_target("Foo::foo", executable)
        with pytest.raises(ValueError):
            translator.target_include_directories(alias_target.name, private="include")


if __name__ == "__main__":
    pytest.main()
