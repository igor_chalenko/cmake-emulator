import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.commands.target_link_libraries import TargetLinkLibraries
from yaranga.objects.target import Executable


class TestTargetLinkLibraries(CommandTestCase):
    @pytest.fixture
    def executable(self, translator):
        return translator.add_executable(name="name", sources=["src/foo.cc"])

    def test_public(self, executable):
        command = TargetLinkLibraries(executable.name, public=["pthread", "catch2"])
        assert str(command) == "target_link_libraries(name PUBLIC pthread catch2)"

    def test_interface(self, executable):
        command = TargetLinkLibraries(
            executable.name, public="pthread", interface="catch2"
        )
        assert (
            str(command)
            == "target_link_libraries(name PUBLIC pthread INTERFACE catch2)"
        )

    def test_mixed_visibility(self, translator, executable: Executable):
        # command = TargetLinkLibraries(executable)
        translator.target_link_libraries(
            executable.name,
            public="catch2",
            interface="pthread",
            private=["catch", "gtest"],
        )
        translator.target_link_libraries(executable.name, private="format")
        command = translator.command_tray.get_commands()[-2]
        assert (
            str(command) == "target_link_libraries(name PUBLIC catch2 "
            "PRIVATE catch gtest INTERFACE pthread)"
        )
        assert executable.link_libraries == ["catch2", "catch", "gtest", "format"]

    def test_str(self, translator, executable):
        translator.target_link_libraries("name", "catch2")
        cmd = translator.command_tray.get_commands()[3]
        assert str(cmd) == "target_link_libraries(name PUBLIC catch2)"


if __name__ == "__main__":
    pytest.main(["--tb=line"])
