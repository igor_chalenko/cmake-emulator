from pathlib import Path

import pytest

from yaranga.cmake.translator import Translator
from yaranga.objects.updatetype import UpdateType
from yaranga.undefined import Undefined

BUILD_DIR = "build"


class TestSetDirectoryProperty:
    @pytest.fixture
    def fullpath(self, resource_path):
        return Path(resource_path, BUILD_DIR).as_posix()

    @pytest.fixture
    def translator(self, fullpath):
        context = Translator(fullpath, fullpath)
        return context

    @pytest.fixture
    def commands(self, translator):
        return translator.command_tray.commands

    def test_set_value(self, translator, fullpath, commands):
        translator.set_directory_property("./", "VAR", "value")
        cmd = commands[-1]
        assert f'set_property(DIRECTORY "{fullpath}" PROPERTY VAR value)' == str(cmd)

    def test_append(self, translator):
        translator.set_directory_property("./", "VAR", "value")
        translator.set_directory_property("./", "VAR", "value2", UpdateType.APPEND)
        assert translator.sm.directory("./")["VAR"] == ["value", "value2"]

        with pytest.raises(ValueError):
            translator.set_directory_property(
                "./", "VAR", "more", UpdateType.APPEND_STRING
            )

        with pytest.raises(ValueError):
            translator.set_directory_property("./", "VAR", Undefined, UpdateType.APPEND)

        with pytest.raises(ValueError):
            translator.set_directory_property(
                "./", "VAR", Undefined, UpdateType.APPEND_STRING
            )

    def test_append_to_empty(self, translator):
        translator.set_directory_property("./", "VAR", "value", UpdateType.APPEND)
        assert translator.sm.directory("./")["VAR"] == "value"

    def test_append_string(self, translator):
        translator.set_directory_property("./", "VAR", "auto")
        translator.set_directory_property(
            "./", "VAR", "cross", UpdateType.APPEND_STRING
        )
        assert translator.sm.directory("./")["VAR"] == "autocross"

    def test_prepend(self, translator):
        translator.set_directory_property("./", "VAR", "value")
        translator.set_directory_property("./", "VAR", "value2", UpdateType.PREPEND)
        assert translator.sm.directory("./")["VAR"] == ["value2", "value"]

    def test_remove_value(self, translator, fullpath, commands):
        translator.set_directory_property("./", "VAR", "value")
        translator.set_directory_property("./", "VAR")
        cmd = commands[-1]
        assert str(cmd) == f'set_property(DIRECTORY "{fullpath}" PROPERTY VAR)'

    def test_set_none(self, translator, fullpath, commands):
        translator.set_directory_property("./", "VAR", "value")
        translator.set_directory_property("./", "VAR", None)
        cmd = commands[-1]
        assert str(cmd) == f'set_property(DIRECTORY "{fullpath}" PROPERTY VAR)'
