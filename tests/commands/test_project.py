import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.commands.project import Project


class TestProject(CommandTestCase):
    @pytest.fixture
    def project_name(self):
        return "name"

    @pytest.fixture
    def homepage(self):
        return "url"

    @pytest.fixture
    def description(self):
        return "description"

    def test_exec(self, translator, project_name, homepage, description):
        ver = "0.1"
        command = Project(
            translator.sm,
            translator.sm.current_dir,
            True,
            name=project_name,
            version=ver,
            homepage_url=homepage,
            description=description,
            languages=["CXX"],
        )
        assert (
            str(command) == f'project(name VERSION "{ver}" '
            f"DESCRIPTION {description} HOMEPAGE_URL {homepage} LANGUAGES CXX)"
        )

        command.is_top_level = True

        translator.project(project_name, ver, homepage, description)
        assert translator["PROJECT_NAME"] == project_name
        assert translator["PROJECT_VERSION"] == ver

        command = Project(
            translator.sm,
            translator.sm.current_dir,
            False,
            name=project_name,
            homepage_url=homepage,
            description=description,
            languages=["CXX"],
        )
        command.is_top_level = False
        translator.project(project_name, None, homepage, description)
        assert translator["PROJECT_NAME"] == project_name
        assert translator["PROJECT_VERSION"] is None

    def test_no_current_dir(self, translator, project_name, homepage):
        translator.sm.current_dir = None
        with pytest.raises(AssertionError):
            translator.project(project_name)


if __name__ == "__main__":
    pytest.main()
