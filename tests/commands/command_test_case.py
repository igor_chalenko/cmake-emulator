import pytest

from yaranga.cmake.translator import Translator
from yaranga.objects.cmakeobject import CMakeObject


class TestTranslator(Translator):
    def on_dirty_read(self, obj: CMakeObject, name: str):
        if name == "COMPILE_DEFINITIONS":
            field_name = obj.attr_from_property(name)
            obj.__setattr__(field_name, "-O1")

    def flush(self, last=True):
        pass


class CommandTestCase:
    @pytest.fixture
    def translator(self, resource_path) -> Translator:
        translator = TestTranslator(resource_path, resource_path / "build")
        translator.set_global_property("IN_TRY_COMPILE", "0")
        translator["CMAKE_SOURCE_DIR"] = str(resource_path)
        translator["CMAKE_CURRENT_BINARY_DIR"] = str(resource_path)

        return translator

    @pytest.fixture
    def commands(self, translator):
        return translator.command_tray.get_commands()
