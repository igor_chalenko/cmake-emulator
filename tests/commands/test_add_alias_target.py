import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.commands.add_alias_target import AddAliasTarget


class TestAddAliasTarget(CommandTestCase):
    def test_str(self, translator):
        target = translator.add_executable("exe", sources="src/foo.cc")
        name = "name"
        command = AddAliasTarget(name, target)
        assert str(command) == f"add_library({name} ALIAS exe)"

    def test_exec(self, translator):
        target = translator.add_executable("exe", sources="src/foo.cc")
        name = "name"
        translator.add_alias_target(name, target)
        assert name in translator.sm.targets


if __name__ == "__main__":
    pytest.main()
