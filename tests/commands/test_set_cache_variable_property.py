import pytest

from tests.commands.command_test_case import CommandTestCase
from yaranga.cmake.translator import Translator
from yaranga.objects.cache_variable import CacheVariableType
from yaranga.objects.updatetype import UpdateType
from yaranga.undefined import Undefined


class TestSetCacheVariableProperty(CommandTestCase):
    @pytest.fixture
    def cache_var(self, translator):
        return translator.set_cache_variable("A", "B", CacheVariableType.STATIC, "help")

    def test_set_value(self, translator: Translator, cache_var, commands):
        translator.set_cache_variable_property("A", "VAR", "value")
        cmd = commands[-1]
        assert str(cmd) == f"set_property(CACHE A PROPERTY VAR value)"

    def test_append(self, translator):
        translator.set_global_property("VAR", "value")
        translator.set_global_property("VAR", "value2", UpdateType.APPEND)
        assert translator.sm.globals["VAR"] == ["value", "value2"]

        with pytest.raises(ValueError):
            translator.set_global_property("VAR", "more", UpdateType.APPEND_STRING)

        with pytest.raises(ValueError):
            translator.set_global_property("VAR", Undefined, UpdateType.APPEND)

        with pytest.raises(ValueError):
            translator.set_global_property("VAR", Undefined, UpdateType.APPEND_STRING)

    def test_append_to_empty(self, translator):
        translator.set_global_property("VAR", "value", UpdateType.APPEND)
        assert translator.sm.globals["VAR"] == "value"

    def test_append_string(self, translator):
        translator.set_global_property("VAR", "auto")
        translator.set_global_property("VAR", "cross", UpdateType.APPEND_STRING)
        assert translator.sm.globals["VAR"] == "autocross"

    def test_prepend(self, translator):
        translator.set_global_property("VAR", "value")
        translator.set_global_property("VAR", "value2", UpdateType.PREPEND)
        assert translator.sm.globals["VAR"] == ["value2", "value"]

    def test_remove_value(self, translator, commands):
        translator.set_global_property("VAR", "value")
        translator.set_global_property("VAR")
        cmd = commands[-1]
        assert f"set_property(GLOBAL PROPERTY VAR)" == str(cmd)

    def test_set_none(self, translator, commands):
        translator.set_global_property("VAR", "value")
        translator.set_global_property("VAR", None)
        cmd = commands[-1]
        assert str(cmd) == f"set_property(GLOBAL PROPERTY VAR)"
