# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open("README.md") as f:
    readme_content = f.read()

with open("LICENSE.md") as f:
    license_content = f.read()

setup(
    name="yaranga",
    version="0.1.0",
    description="Yaranga - CMake emulator",
    long_description=readme_content,
    author="Igor Chalenko",
    author_email="igor.chalenko@gmail.com",
    url="https://bitbucket.org/igor_chalenko/cmake-emulator",
    license=license_content,
    package_dir={"": "src"},
    packages=find_packages(where="src"),
)
