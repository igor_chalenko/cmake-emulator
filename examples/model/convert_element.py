from enum import Enum

from yaranga.model.converters import convert
from yaranga.model.typeinfo import TypeInfo


class TestEnum(Enum):
    A = (1,)
    B = 2


if __name__ == "__main__":
    ti = TypeInfo(bool)

    # prints: True
    print(
        convert("42", ti)
        and convert("ON", ti)
        and convert("on", ti)
        and convert("some string", ti)
        and convert("YES", ti)
        and convert("y", ti)
    )

    # prints: None
    print(convert("NOTFOUND", ti))
    # prints: None
    print(convert("name_NOTFOUND", ti))

    a = "A"
    b = "B"
    typeinfo = TypeInfo(TestEnum)

    res = convert(a, typeinfo)
    # prints: TestEnum.A
    print(res)
    # prints: TestEnum.B
    res = convert(b, typeinfo)
    print(res)

    typeinfo = TypeInfo(int)
    # prints: -42
    print(convert("-42", typeinfo))
