from pathlib import Path

from yaranga.objects.directory import Directory
from yaranga.objects.target import Executable


def example():
    directory = Directory(
        source_dir=Path(".").absolute(), binary_dir=Path(".").absolute()
    )
    target = Executable(
        name="example",
        sources=[Path("example.cc")],
        exclude_from_all=True,
        source_dir=directory,
    )
    # both accessors point to the same data, there's no duplication
    assert target.exclude_from_all and target["EXCLUDE_FROM_ALL"]
    target.exclude_from_all = False
    assert not target.exclude_from_all and not target["EXCLUDE_FROM_ALL"]


if __name__ == "__main__":
    example()
