from yaranga.model.converters import convert
from yaranga.model.typeinfo import TypeInfo

if __name__ == "__main__":
    ti = TypeInfo(list[int])
    # prints: []
    print(convert("", ti))
    # prints: [1, 2, 3]
    print(convert("1;2;3", ti))
    # prints: [1, 2, 3]
    print(convert(["1", "2", "3"], ti))
    # prints: [1, None]
    print(convert("1;NOTFOUND", ti))

    try:
        # raises ValueError
        print(convert(["1;2;3"], ti))
    except ValueError:
        pass
